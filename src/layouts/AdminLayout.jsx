import { Outlet } from "react-router-dom";
import Footer from "../components/Footer/Footer";
import Navbar from "../components/Navbar/NavAdministrador/Navbar";

export const AdminLayout = () => {
  return (
    <>
      <Navbar />
      <Outlet/>
      <Footer/>
    </>
  );
};
