import { Outlet } from "react-router-dom";
import Footer from "../components/Footer/Footer";
import Navbar from "../components/Navbar/NavCoordinador/Navbar";

export const CoordinadorLayout = () => {
  return (
    <>
      <Navbar />
        <Outlet/>
      <Footer/>
    </>
  );
};
