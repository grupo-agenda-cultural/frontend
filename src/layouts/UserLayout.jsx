import Footer from "../components/Footer/Footer";
import Navbar from "../components/Navbar/NavUserFinal/Navbar";
import { Outlet } from "react-router-dom";
export const UserLayout = () => {
  return (
    <>
      <Navbar />
        <Outlet/>
      <Footer/>
    </>
  );
};
