import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from 'react-redux';
import { getCulturalSpaces } from '../../../actions/culturalSpaceAction';
import Card from '../../../components/CulturalSpace/Card/Card';
import './CulturalSpaces.css';
import  img1 from '../../../assets/images/covers/espacios-culturales.png';
import Pagination from '../../../components/Pagination/pagination';

const CulturalSpaces = () => {

  const dispatch = useDispatch();
  const cultural_spaces = useSelector(state => state.cultural_spaces.cultural_spaces);

  useEffect(() => {
      dispatch(getCulturalSpaces())
      // eslint-disable-next-line 
  }, []);  
  
  const culturalSpacesPerPage = 10;
  const [pageNumber, setPageNumber] = useState(0);
  const pageCount = Math.ceil(cultural_spaces.length / culturalSpacesPerPage);
  const pagesVisited = pageNumber * culturalSpacesPerPage;

  const changePage = ({ selected }) => {
    setPageNumber(selected);
  };

  const displayCulturalSpaces = cultural_spaces
    .slice(pagesVisited, pagesVisited + culturalSpacesPerPage)
    .map((item,idx) => {
      return (
          <Card key={idx} item={item}/>
    )
     });


  return (
    <>
      <div className="img-cs">
      <img src={img1} className="img-principal-cultural-spaces" alt="cultural-spaces-img"/>
      </div>
      <div className="container-cultural-spaces">
      
      <p className="information-title">Conocé los espacios culturales que forman parte de Agenda y Comunicación Cultural y Artística.<br/> </p>
      </div>
     
      
    <div className="espacios-culturales">
          {displayCulturalSpaces}
            </div>
          <Pagination 
            pageCount={pageCount} 
            changePage={changePage}
          />
    </>
  );
};

export default CulturalSpaces;