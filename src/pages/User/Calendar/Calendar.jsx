import { React, useState, useEffect} from 'react';
import { useDispatch, useSelector } from 'react-redux';
import ListCards from '../../../components/Production/Card/ListCards';
import { getProductionsCalendar } from '../../../actions/productionAction';
import img1 from '../../../assets/images/covers/agenda.png';
import './Calendar.css';
import FilterCalendar from '../../../components/FilterPanel/Calendar/FilterCalendar';
import Offcanvas from 'react-bootstrap/Offcanvas';
import EmptyView from '../../../components/EmptyView/EmptyView';
import Loading from '../../../components/Spinner/Spinner';
import SearchBar from '../../../components/SearchBar/Search';
const Calendar = () => {

  const dispatch = useDispatch();

  const [dateRange, setDateRange] = useState([null, null]);
  const [startDate, endDate] = dateRange;

  const [selectedPartido, setSelectedPartido] = useState([]);
  const [selectedLanguage, setSelectedLanguage] = useState([]);
  const [selectedCategory, setSelectedCategory] = useState([]);
  const [selectedModality, setSelectedModality] = useState([]);
  const [selectedCost, setSelectedCost] = useState([]);

  const [resultsFound, setResultsFound] = useState(true);
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const [searchInput, setSearchInput] = useState('');


  const productions = useSelector(state => state.productions.productions);
  const [filterProductions, setFilterProductions] = useState(productions)
  
  const [pending, setPending] = useState(true);
  
  useEffect(() => {
    dispatch(getProductionsCalendar())
    // eslint-disable-next-line  react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    applyFilters();
    // eslint-disable-next-line  react-hooks/exhaustive-deps
  }, [ productions, searchInput, dateRange, selectedPartido, selectedLanguage, selectedCategory, selectedModality, selectedCost]);

    useEffect(() => {
        const timeout = setTimeout(() => {
           setPending(false);
        }, 1800);
        return () => clearTimeout(timeout);
      }, []);
    
  const handleDelete = (e) =>{
    e.preventDefault();
    setDateRange('');
    setSelectedPartido('');
    setSelectedLanguage('');
    setSelectedCategory('');
    setSelectedCost('');
    setSelectedModality('');
  }

  const handleSetDate = (date) => {
       setDateRange(date);
  };

  const handleSelectPartido = (value) =>
    setSelectedPartido(value);

  const handleSelectLanguage = (value) =>
    setSelectedLanguage(value);

  const handleSelectCategory = (value) =>
    setSelectedCategory(value);

  const handleSelectModality = (value) =>
    setSelectedModality(value);


  const handleSelectCost = (value) =>
    setSelectedCost(value);

  //Add your search logic here.
  let filteredProductions = productions;

  const applyFilters = () => {
     // Dates Filter
     if (startDate != null && endDate != null) {  
      filteredProductions = filteredProductions.filter((elem) => {
        var date = new Date(endDate);
        date.setDate(endDate.getDate() + 1);
       return (new Date(elem.production.date_start) >= new Date(startDate)
       && new Date(elem.production.date_start) <= date.getTime())
       || new Date(elem.production.date_end) <= date.getTime();
     })
    }
    // Partido Filter
    if (selectedPartido.length >= 1) {
      filteredProductions = filteredProductions.filter((elem) => {
        return selectedPartido.some(partido=> [elem.cultural_space.partido].flat().includes(partido.value))
      })
    }
     // Language Filter
     if (selectedLanguage.length >= 1) {
      filteredProductions = filteredProductions.filter((elem) => {
        return selectedLanguage.some(language => [elem.production.language].flat().includes(language.value))
      })
    }

     // Category Filter
     if (selectedCategory.length >= 1) {
      filteredProductions = filteredProductions.filter((elem) => {
        return selectedCategory.some(category=> [elem.production.category].flat().includes(category.value))
      })
    }

    // Cost Filter
    if (selectedCost.length >= 1) {
      filteredProductions = filteredProductions.filter((elem) => {
        return selectedCost.some(cost => [elem.production.cost].flat().includes(cost.value))
      })
    }

    // Modality Filter
    if (selectedModality.length >= 1) {
       filteredProductions = filteredProductions.filter((elem) => {
       return selectedModality.some(modality => [elem.production.modality].flat().includes(modality.value))
    })
   }
   // Search Filter
    if (searchInput) {
      filteredProductions = filteredProductions.filter(
        (item) =>
          item.production.name.toLowerCase().search(searchInput.toLowerCase().trim()) !==
          -1
      );
    }
   setFilterProductions(filteredProductions)

    // else{
    //   setFilterProductions([...filteredProductions])
    // }
    !filteredProductions.length ? setResultsFound(false) : setResultsFound(true);
  }



  return (
    <>
      <div className="cover">
        <div className="img-agenda">
          <img src={img1} className="img-principal-agenda" alt="img-cover" />
        </div>
        {/* <div className="contenedor">
          <h1 className="agenda-cover">Agenda<span>&#160;</span></h1>
        </div> */}
      </div>
      <h1 className="agenda">Agenda</h1>
      <div className="productions-circle">
        <div className="circulo">
          <h2>{filterProductions.length} </h2>
        </div>
        <h3 className="productions-events">Producciones y eventos</h3>
       
      </div>

      <SearchBar
        value={searchInput}
        changeInput={(e) => setSearchInput(e.target.value)} placeholder={'Buscar producciones'}
      />
      <div className="button-filter">
        <button onClick={handleShow} className="filter btn btn-light border-dark rounded-0"><i className="bi bi-funnel-fill"></i> Filtrar</button>
      </div>
      <div className="container-filters-cards">  
      <div className="filters">
      <div className="filters-panel">
      <FilterCalendar 
            startDate = {startDate}
            endDate = {endDate}
            setDateRange = {handleSetDate}
            selectedPartido={selectedPartido}
            selectPartido={handleSelectPartido}
            selectedLanguage={selectedLanguage}
            selectLanguage={handleSelectLanguage}
            selectedCategory={selectedCategory}
            selectCategory={handleSelectCategory}
            selectedModality={selectedModality}
            selectModality={handleSelectModality}
            selectedCost={selectedCost}
            selectCost={handleSelectCost}
            results={filterProductions}
          /> 
          </div>
          <div className="filter-btn">
                <button className="btn btn-secondary" onClick={handleDelete} style={{ width:"100%"}}>Borrar filtros</button>
              </div>
</div>
          {pending ? 
      <Loading/>
     :
       resultsFound ? <ListCards cards={filterProductions} /> : <EmptyView/>}   
    
        {/* <ListCards cards={
          filterProductions.length === 0
            ? productions
            : filterProductions} /> */}
      </div>
      <Offcanvas show={show} onHide={handleClose} placement={'end'} scroll={true}>
        <Offcanvas.Header closeButton>
          <Offcanvas.Title>Filtros</Offcanvas.Title>
        </Offcanvas.Header>
        <Offcanvas.Body>
        <div>
          <FilterCalendar
            startDate = {startDate}
            endDate = {endDate}
            // dateRange = {dateRange}
            setDateRange = {handleSetDate}
            selectedPartido={selectedPartido}
            selectPartido={handleSelectPartido}
            selectedLanguage={selectedLanguage}
            selectLanguage={handleSelectLanguage}
            selectedCategory={selectedCategory}
            selectCategory={handleSelectCategory}
            selectedModality={selectedModality}
            selectModality={handleSelectModality}
            selectedCost={selectedCost}
            selectCost={handleSelectCost}
            results={filterProductions}
          />
          </div>
           {/* <div className="filter-button">
                <button className="btn btn-primary" onClick={() => setFilterProductions(filteredProductions)}>Aplicar filtros</button>
              </div> */}
                <div className="filter-button">
                <button className="btn btn-secondary" onClick={handleDelete} >Borrar filtros</button>
              </div>  
              
        </Offcanvas.Body>
       
      </Offcanvas>
    </>
  );
}
export default Calendar;
