import React from 'react';
import { useDispatch} from 'react-redux';
import { Formik, ErrorMessage } from "formik";
import * as Yup from 'yup'
import { addSubscriptionCalendar } from '../../../../../actions/subscriptionAction';
import { Form , InputGroup, Button } from 'react-bootstrap';

const Subscription = () => {
    const dispatch = useDispatch();

  // Validaciones
  const ErrorMessagesSchema = Yup.object().shape({
    email: Yup.string().email('Email inválido').required('Debe ingresar su email para suscribirse al espacio cultural')
  });

  const handleSubmit = (values) => {
    dispatch(addSubscriptionCalendar(values))
  }
    return (
        <>
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320"><path fill=" #edf1f3" fillOpacity="1" d="M0,96L80,106.7C160,117,320,139,480,170.7C640,203,800,245,960,245.3C1120,245,1280,203,1360,181.3L1440,160L1440,320L1360,320C1280,320,1120,320,960,320C800,320,640,320,480,320C320,320,160,320,80,320L0,320Z"></path></svg>
 <div className="container-fluid text-center p-4 mt-0" style={{backgroundColor:"#edf1f3" }}>
      
            <div className="row justify-content-center">
                <div className="col-md-12">
                <h1 className="text-primary p-0" style={{fontSize:"9vmin"}}>Suscribite a la agenda</h1>
                <p className="text">Enterate de todas las novedades que tenemos para contarte. ¡Sumate!</p>

                </div>
                <div className="row align-items-center justify-content-center">
            <div className="col-md-12">
               <div className="input-group justify-content-center my-4">
               <Formik
            initialValues={{
              email:'',
            }}
            onSubmit={(values,{resetForm}) => {
              handleSubmit(values);
              resetForm();
            }}
            validationSchema={ErrorMessagesSchema}
          >
            {({ errors, touched, handleSubmit,handleChange, handleBlur, values, isValid, setFieldValue }) => (
              <Form >
              
              <Form.Group >
                    <InputGroup>
                    <Form.Control
                      type="email"
                      name="email"
                      value={values.email}
                      placeholder="Ingresá tu correo electrónico"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      isInvalid={touched.email && errors.email}
                    />
 <Button type="submit" onClick={handleSubmit} variant="primary" id="button-addon2">
      Suscribirme
    </Button> 
    </InputGroup>
    {errors.email && touched.email ? <div style={{ witch:"100%", fontSize: ".875em", color: "#dc3545"}}><ErrorMessage name="email"/></div> : null}

    </Form.Group> 
    





              </Form>

              )}
        </Formik>
                </div> 
                </div>

            
            </div></div>
        </div>
        </>
    );
}

export default Subscription;
