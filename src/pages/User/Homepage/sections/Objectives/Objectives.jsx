import React from "react";
import './Objectives.css';
import { motion } from 'framer-motion/dist/framer-motion';
import {useScroll} from "../../sections/useScroll"
import {
  topContainerAnimation,
} from "../../../../../utils/animation";

const Objectives= () => {
  const [element, controls] = useScroll();
  return (
      <>
<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320"><path fill="#f7f7f7" fillpacity="1" d="M0,224L48,197.3C96,171,192,117,288,133.3C384,149,480,235,576,224C672,213,768,107,864,101.3C960,96,1056,192,1152,192C1248,192,1344,96,1392,48L1440,0L1440,320L1392,320C1344,320,1248,320,1152,320C1056,320,960,320,864,320C768,320,672,320,576,320C480,320,384,320,288,320C192,320,96,320,48,320L0,320Z"></path></svg>    <div className="objectives-container" ref={element}>
      <div className="container-objectives">
          <h1 className="title-objectives">Nuestros objetivos</h1>
    {/* <div className="content"> */}
         {/*   <motion.div
             className="top"
             initial="hidden"
          animate="show"
          animate={controls}
          variants={reveal}
          transition={{ delay: 0.1, stiffness: 300 }}
        >
            <div className="video">
              {/* <GoPlay /> */}
            {/* </div> */}
          {/* </motion.div> */}
          <motion.div
            className="reasons"
            variants={topContainerAnimation}
            animate={controls}
            transition={{ stiffness: 5000 }}
          >
            <ul className="list-objectives">
              <li><div className="objetive">Objective</div></li>
              <li ><div className="objetive">
                Objective</div>
              </li>
              <li> <div className="objetive">-</div></li>
              <li> <div className="objetive">-</div></li>
              <li> <div className="objetive">-</div></li> 
            </ul>
          </motion.div>
        {/* </div> */}
      </div>
    </div>
    </>
  );
}

export default Objectives;