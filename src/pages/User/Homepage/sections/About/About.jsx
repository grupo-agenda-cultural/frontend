import React from 'react';
import './About.css'
import { motion} from 'framer-motion/dist/framer-motion';
import Card from '../../../../../components/Card/Card';
import { reveal } from "../../../../../utils/animation";
import { useScroll } from "../useScroll"
const About = () => {


  const [element, controls] = useScroll();
    return(
    <div className="about-container"  ref={element}>
      <div className="container-about">
        <motion.div
          className="details"
          initial="hidden"
          // animate="show"
          animate={controls}
          variants={reveal}
          transition={{ delay: 0.1, stiffness: 300 }}
        >
          <h1 className="about-acca">Acerca de A.C.C.A.</h1>
          <p className="details">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
            sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
             Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris 
             nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit 
             in voluptate velit esse cillum dolore eu fugiat nulla pariatur. 
             
          </p>
          <p className="details">
            Excepteur sint occaecat cupidatat non proident, 
            sunt in culpa qui officia deserunt mollit anim id est laborum.
          </p>
          {/* <Button content="Why cryo?" /> */}
        </motion.div>
         <div className="cards" ref={element}>
          <Card
              image={<i className="bi bi-music-note-beamed"></i>}
              title="-"
              animateCustom={controls}

          />
          <Card
            title="-"
            animateCustom={controls}
          />
          <Card
            title="-"
            animateCustom={controls}
          />
          <Card
            title="-"
            animateCustom={controls}
          />
        </div> 
      </div>
    </div>
  );
}

export default About;