import React,{ useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import './SliderCards.css';
import Card from '../../../../../components/Production/Card/Card';
import { getProductionsCalendar } from '../../../../../actions/productionAction';

const responsive = {
  superLargeDesktop: {
    // the naming can be any, depends on you.
    breakpoint: { max: 3000, min: 1250},
    items: 4
  },
  desktop: {
    breakpoint: { max: 1249, min: 1090},
    items: 3
  },
  tablet: {
    breakpoint: { max: 1090, min: 621},
    items: 2
  },
  mobile: {
    breakpoint: { max: 620, min: 0 },
    items: 1
  }
};
const SliderCards = () => { 
    const dispatch = useDispatch();


    const productions = useSelector(state => state.productions.productions);
      

    
    useEffect(() => {
      dispatch(getProductionsCalendar())
      // eslint-disable-next-line
    }, []);
    return (
         <div className="container-calendar-week">
        <h1 className="title-calendar-week">Novedades de la agenda</h1>
        <div className="all-productions">
            <a href="/agenda"style={{textDecoration:'none'}}>Ver todas las producciones de la agenda</a>
      </div>
          {productions.length ?

   <Carousel 
      responsive={responsive}
      autoPlay
      autoPlaySpeed={2000}
      infinite={true}
      pauseOnHover
      itemClass="item-card"
      className="items"
>                     

           {productions.map((elem,i) => {
            return(
                   <Card key={i} card={elem}/>
            )
          })

            } 

        </Carousel> 
        :
        <div className="row">
        <div className="col-md-12">           
         <p className="text-center">No hay producciones esta semana</p>  
         </div>  
</div>}
        </div>
    );
}

export default SliderCards;
