import React from "react";
import './Description.css';
import { motion } from 'framer-motion/dist/framer-motion';
import {useScroll} from "../useScroll"
import {
  topContainerAnimation,
} from "../../../../../utils/animation";

const Description = () => {
  const [element, controls] = useScroll();
  return (

    <>
    <div className="why-container" ref={element}>
      <div className="container-why">
      <motion.div
          className="top"
          variants={topContainerAnimation}
          animate={controls}
          transition={{
            duration: 1,
          }}
        >
          <h1 className="title-culturalspace">¿Qué son los espacios culturales?</h1>
          <div className="subTitle">
            <p>
            Aquellos espacios en los que se desarrolle una actividad de producción y/o formación artística. Cuando decimos <b>actividad artística</b> nos referimos a la práctica o estudio de los lenguajes artísticos, a saber: Artes del Movimiento, Artes Visuales, Teatro, Música, 
            Letras o la combinación de dos o más de estas disciplinas artísticas.<br />
            Los espacios relevados se clasificaron en 4 categorías según su actividad:
            </p>
<ul>
<li>Espacios de formación artística (dictado de cursos, seminarios y/o talleres)</li>
<li>Espacios de presentación, producción y/o exhibición de obras artísticas.</li>
<li>Espacios de carácter mixto que complementan ambas actividades.</li>
<li>Espacios alternativos (Bares y espacios gastronómicos con actividades artísticas).</li>

Sumada a esta definición técnica, también entendemos a los espacios culturales como actores sociales, culturales y políticos. Donde se construyen sentidos y valores, necesarios para garantizar el derecho a la cultura que tiene todo ciudadano.
         </ul>
          </div>
          <div className="container-button">
          <p className="text-coordinador-agenda">¿Sos coordinador/a de un espacio cultural?</p>
          <a href="/login" target="_blank" rel="noreferrer" style={{textDecoration:'none'}}><button className="register-culturalspace">Regístrate y sumate a la agenda</button></a>
      </div>
       </motion.div>
        {/* <div className="content">
          <motion.div
            variants={videoAnimation}
            animate={controls}
            transition={{ type: "tween", duration: 0.5 }}
          >
            <div className="video">
              <GoPlay />
            </div>
          </motion.div>
          <motion.div
            className="reasons"
            variants={reasonsAnimation}
            animate={controls}
            transition={{ type: "tween", duration: 0.5 }}
          >
            <ul>
              <li>Over 10+ years of industry wide expierence</li>
              <li>
                Provide solutions to our multiple global clients or website
                traffic generation and workflow
              </li>
              <li>Strong team of 150+ creative people</li>
              <li>99% adhere to service level contract</li>
              <li>Ready to recieve service request 24/7</li>
            </ul>
          </motion.div> */}
        {/* </div> */}
      </div>
    </div>
    </>
  );
}

export default Description;