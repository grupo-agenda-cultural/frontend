import React from 'react';
import Slider from '../../../components/Carousel/Slider';
import Description from './sections/Description/Description';
import About from './sections/About/About'
import Objectives from './sections/Objectives/Objectives';
import SliderCards from './sections/SliderCards/SliderCards';
import Subscription from './sections/Subscription/Subscription';
const Homepage = () => {
    return (
        <>
            <Slider/>
            <About/>
            <Description/>
            <Objectives/>
            <SliderCards/>
            <Subscription/>
        </>
    );
}

export default Homepage;