import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import img1 from '../../../assets/images/covers/formacion.png';
import { getFormationbyCulturalSpace } from '../../../actions/courseAction';
import ListCards from '../../../components/Course/Card/ListCards';
import './Formation.css';
import FilterFormation from '../../../components/FilterPanel/Formation/FilterFormation';
import Offcanvas from 'react-bootstrap/Offcanvas';
import EmptyView from '../../../components/EmptyView/EmptyView';
import { useParams, Link } from "react-router-dom";
import Loading from '../../../components/Spinner/Spinner';
import SearchBar from '../../../components/SearchBar/Search';

const Formation = () => {

  const dispatch = useDispatch();
  const { culturalSpaceId } = useParams();

  const [selectedLanguage, setSelectedLanguage] = useState([]);
  const [selectedCategory, setSelectedCategory] = useState([]);
  const [selectedModality, setSelectedModality] = useState([]);
  const [selectedCost, setSelectedCost] = useState([]);

  const [resultsFound, setResultsFound] = useState(false);
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const { courses } = useSelector(state => state.courses);
  const [filterCourses, setFilterCourses] = useState(courses)
  const [searchInput, setSearchInput] = useState('');

  const[pending, setPending] = useState(true);
  useEffect(() => {
    dispatch(getFormationbyCulturalSpace(culturalSpaceId))
    // eslint-disable-next-line
  }, [courses])

  useEffect(() => {
    applyFilters();
    // eslint-disable-next-line
  }, [setFilterCourses, courses, setFilterCourses, selectedLanguage, selectedCategory, selectedModality, selectedCost]);

  useEffect(() => {
    window.scrollTo({ top: 545, behavior: 'smooth' })
  }, [])

  useEffect(() => {
    const timeout = setTimeout(() => {
      setPending(false);
    }, 1800);
    return () => clearTimeout(timeout);
  }, []);

  const handleDelete = (e) => {
    e.preventDefault();
    setSelectedLanguage('');
    setSelectedCategory('');
    setSelectedCost('');
    setSelectedModality('');
  }


  const handleSelectLanguage = (value) =>
    setSelectedLanguage(value);

  const handleSelectCategory = (value) =>
    setSelectedCategory(value);

  const handleSelectModality = (value) =>
    setSelectedModality(value);


  const handleSelectCost = (value) =>
    setSelectedCost(value);


  const applyFilters = () => {
    let filteredCourses = courses;

    // Language Filter
    if (selectedLanguage.length >= 1) {
      filteredCourses = filteredCourses.filter((elem) => {
        return selectedLanguage.some(language => [elem.course.language].flat().includes(language.value))
      })
    }

    // Category Filter
    if (selectedCategory.length >= 1) {
      filteredCourses = filteredCourses.filter((elem) => {
        return selectedCategory.some(category => [elem.course.category].flat().includes(category.value))
      })
    }

    // Cost Filter
    if (selectedCost.length >= 1) {
      filteredCourses = filteredCourses.filter((elem) => {
        return selectedCost.some(cost => [elem.course.cost].flat().includes(cost.value))
      })
    }

    // Modality Filter
    if (selectedModality.length >= 1) {
      filteredCourses = filteredCourses.filter((elem) => {
        return selectedModality.some(modality => [elem.course.modality].flat().includes(modality.value))
      })
    }
    setFilterCourses(filteredCourses)

    // else{
    //   setFilterCourses([...filteredCourses])
    // }
    !filteredCourses.length ? setResultsFound(false) : setResultsFound(true);
  }

  return (
    <>
      <div className="img-formacion">
        <img src={img1} className="img-principal-formacion" alt="formation-img" />
      </div>
      <Link to="/formacion" className="link-primary"><p className="d-flex align-items-end flex-column p-3" style={{textDecoration:"none"}}>Ver todos los talleres y cursos</p></Link>
      <h1 className="formation">Formación del espacio cultural</h1>
      <div className="courses-circle">
        <div className="circulo-courses">
          <h2>{filterCourses.length}</h2>
          {/* <h2>{isLoading ? <Loading/> : filterCourses.length} </h2> */}
        </div>
        <h3 className="courses-title">Talleres y cursos</h3>
      </div>
       <SearchBar
        value={searchInput}
        changeInput={(e) => setSearchInput(e.target.value)} placeholder={'Buscar talleres'}
      />
      <div className="button-filter">
        <button onClick={handleShow} className="filter btn btn-light border-dark rounded-0"><i className="bi bi-funnel-fill"></i> Filtrar</button>
      </div>
      <div className="container-filters-cards">
        <div className="filters">
          <div className="filters-panel">
            <FilterFormation
              selectedLanguage={selectedLanguage}
              selectLanguage={handleSelectLanguage}
              selectedCategory={selectedCategory}
              selectCategory={handleSelectCategory}
              selectedModality={selectedModality}
              selectModality={handleSelectModality}
              selectedCost={selectedCost}
              selectCost={handleSelectCost}
              results={filterCourses}
            />
          </div>
          <div className="filter-btn">
            <button className="btn btn-secondary" onClick={handleDelete} style={{ width: "100%" }}>Borrar filtros</button>
          </div>
        </div>
        {pending ? 
      <Loading/>
     :
        resultsFound ? <ListCards cards={filterCourses} /> : <EmptyView/>
    }

        {/* <ListCards cards={
          filterCourses.length === 0
            ? courses
            : filterCourses} /> */}
      </div>
      <Offcanvas show={show} onHide={handleClose} placement={'end'} scroll={true}>
        <Offcanvas.Header closeButton>
          <Offcanvas.Title>Filtros</Offcanvas.Title>
        </Offcanvas.Header>
        <Offcanvas.Body>
          <div>
            <FilterFormation
              selectedLanguage={selectedLanguage}
              selectLanguage={handleSelectLanguage}
              selectedCategory={selectedCategory}
              selectCategory={handleSelectCategory}
              selectedModality={selectedModality}
              selectModality={handleSelectModality}
              selectedCost={selectedCost}
              selectCost={handleSelectCost}
              results={filterCourses}
            />
          </div>
          {/* <div className="filter-button">
                <button className="btn btn-primary" onClick={() => setFilterCourses(filteredCourses)}>Aplicar filtros</button>
              </div> */}
          <div className="filter-button">
            <button className="btn btn-secondary" onClick={handleDelete} >Borrar filtros</button>
          </div>

        </Offcanvas.Body>

      </Offcanvas>
    </>
  );
}

export default Formation;
