import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import img1 from '../../../assets/images/covers/formacion.png';
import {  getCoursesFormation } from '../../../actions/courseAction';
import ListCards from '../../../components/Course/Card/ListCards';
import './Formation.css';
import FilterFormation from '../../../components/FilterPanel/Formation/FilterFormation';
import Offcanvas from 'react-bootstrap/Offcanvas';
import EmptyView from '../../../components/EmptyView/EmptyView';
import Loading from '../../../components/Spinner/Spinner';
import SearchBar from '../../../components/SearchBar/Search';

const Formation = () => {

    const dispatch = useDispatch();
    const [selectedLanguage, setSelectedLanguage] = useState([]);
    const [selectedCategory, setSelectedCategory] = useState([]);
    const [selectedModality, setSelectedModality] = useState([]);
    const [selectedCost, setSelectedCost] = useState([]);
  
    const [resultsFound, setResultsFound] = useState(true);
    const [show, setShow] = useState(false);
  
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    
    const courses = useSelector(state => state.courses.courses);
    const [filterCourses, setFilterCourses] = useState(courses)
  
     const [pending, setPending] = useState(true);
    const [searchInput, setSearchInput] = useState('');

    useEffect(() => {
        dispatch(getCoursesFormation())
        // eslint-disable-next-line react-hooks/exhaustive-deps
     }, []);
  
    useEffect(() => {
      applyFilters();
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [setFilterCourses, courses, searchInput, selectedLanguage, selectedCategory, selectedModality, selectedCost]);
  
      useEffect(() => {
          const timeout = setTimeout(() => {
             setPending(false);
          }, 1800);
          return () => clearTimeout(timeout);
        }, []);
      
    const handleDelete = (e) =>{
      e.preventDefault();
      setSelectedLanguage('');
      setSelectedCategory('');
      setSelectedCost('');
      setSelectedModality('');
    }
  

 const handleSelectLanguage = (value) =>
   setSelectedLanguage(value);

 const handleSelectCategory = (value) =>
   setSelectedCategory(value);

 const handleSelectModality = (value) =>
   setSelectedModality(value);


 const handleSelectCost = (value) =>
   setSelectedCost(value);

 //Add your search logic here.
 let filteredCourses = courses;

 const applyFilters = () => {
    
    // Language Filter
    if (selectedLanguage.length >= 1) {
     filteredCourses = filteredCourses.filter((elem) => {
       return selectedLanguage.some(language => [elem.course.language].flat().includes(language.value))
     })
   }

    // Category Filter
    if (selectedCategory.length >= 1) {
     filteredCourses = filteredCourses.filter((elem) => {
       return selectedCategory.some(category=> [elem.course.category].flat().includes(category.value))
     })
   }

   // Cost Filter
   if (selectedCost.length >= 1) {
     filteredCourses = filteredCourses.filter((elem) => {
       return selectedCost.some(cost => [elem.course.cost].flat().includes(cost.value))
     })
   }

   // Modality Filter
   if (selectedModality.length >= 1) {
      filteredCourses = filteredCourses.filter((elem) => {
      return selectedModality.some(modality => [elem.course.modality].flat().includes(modality.value))
   })
  }
  // Search Filter
  if (searchInput) {
    filteredCourses = filteredCourses.filter(
      (item) =>
        item.course.name.toLowerCase().search(searchInput.toLowerCase().trim()) !==
        -1
    );
  }
  setFilterCourses(filteredCourses)

   !filteredCourses.length ? setResultsFound(false) : setResultsFound(true);
 }

    return (
        <>
        <div className="img-formacion">
         <img src={img1} className="img-principal-formacion" alt="formation-img"/>
      </div>
      <h1 className="formation">Formación</h1>
      <div className="courses-circle">
        <div className="circulo-courses">
          <h2>{courses.length} </h2>
        </div>
        <h3 className="courses-title">Talleres y cursos</h3>
      </div>
      <SearchBar
        value={searchInput}
        changeInput={(e) => setSearchInput(e.target.value)} placeholder={'Buscar talleres'}
      />
      <div className="button-filter">
        <button onClick={handleShow} className="filter btn btn-light border-dark rounded-0"><i className="bi bi-funnel-fill"></i> Filtrar</button>
      </div>
      <div className="container-filters-cards">  
      <div className="filters">
      <div className="filters-panel">
      <FilterFormation
            selectedLanguage={selectedLanguage}
            selectLanguage={handleSelectLanguage}
            selectedCategory={selectedCategory}
            selectCategory={handleSelectCategory}
            selectedModality={selectedModality}
            selectModality={handleSelectModality}
            selectedCost={selectedCost}
            selectCost={handleSelectCost}
            results={filterCourses}
          /> 
          </div>
          <div className="filter-btn">
                <button className="btn btn-secondary" onClick={handleDelete} style={{ width:"100%"}}>Borrar filtros</button>
              </div>
</div>
          {pending ? 
          <Loading/>
    
     :
       resultsFound ? <ListCards cards={filterCourses} /> : <EmptyView/>}   
    
        {/* <ListCards cards={
          filterCourses.length === 0
            ? courses
            : filterCourses} /> */}
      </div>
      <Offcanvas show={show} onHide={handleClose} placement={'end'} scroll={true}>
        <Offcanvas.Header closeButton>
          <Offcanvas.Title>Filtros</Offcanvas.Title>
        </Offcanvas.Header>
        <Offcanvas.Body>
        <div>
          <FilterFormation
            selectedLanguage={selectedLanguage}
            selectLanguage={handleSelectLanguage}
            selectedCategory={selectedCategory}
            selectCategory={handleSelectCategory}
            selectedModality={selectedModality}
            selectModality={handleSelectModality}
            selectedCost={selectedCost}
            selectCost={handleSelectCost}
            results={filterCourses}
          />
          </div>
           {/* <div className="filter-button">
                <button className="btn btn-primary" onClick={() => setFilterCourses(filteredCourses)}>Aplicar filtros</button>
              </div> */}
                <div className="filter-button">
                <button className="btn btn-secondary" onClick={handleDelete} >Borrar filtros</button>
              </div>  
              
        </Offcanvas.Body>
       
      </Offcanvas>
    </>
    );
}

export default Formation;
