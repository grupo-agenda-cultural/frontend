import React from 'react';
import Map from '../../../components/Map/Map';

const MapCulturalSpaces = () => {
    return (
        <div className="container-map">
            <Map/>
        </div>
    );
}

export default MapCulturalSpaces;