import React from 'react';
import TableCoordinadores from '../../../components/Account/Tables/TableCoordinadores';

const Coordinadores = () => {

  return (
    <>
      <div className="container-fluid bg-light p-2 min-vh-100">
        <h4 className="p-3 font-weight-bold text-secondary">Coordinadores</h4>
     <TableCoordinadores /> 
     </div>
    </>
  );
};

export default Coordinadores;