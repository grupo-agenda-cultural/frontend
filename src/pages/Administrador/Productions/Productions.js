import { React } from 'react';
import TableProductionsAdministrador from '../../../components/Production/Tables/TableProductionsAdministrador';

const ProductionsAdministrador = () => {

  return (
    <>
      <div className="container-fluid bg-light p-2 min-vh-100">
        <h4 className="p-3 font-weight-bold text-secondary">Producciones</h4>
        <TableProductionsAdministrador />
      </div>
    </>
  );
}

export default ProductionsAdministrador;