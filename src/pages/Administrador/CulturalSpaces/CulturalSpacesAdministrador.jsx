import React from "react";
import TableCulturalSpaces from "../../../components/CulturalSpace/Tables/TableCulturaleSpaces";

const CulturalSpacesAdministrador = () => {

  return (
    <>
      <div className="container-fluid bg-light p-2 min-vh-100">
        <h4 className="p-3 font-weight-bold text-secondary">Espacios culturales</h4>
        <TableCulturalSpaces/>
      </div>  
    </>
  );
};

export default CulturalSpacesAdministrador;