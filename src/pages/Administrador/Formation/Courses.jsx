import React from 'react';
import TableCoursesAdministrador from '../../../components/Course/Tables/TableCoursesAdministrador';

const CoursesAdministrador = () => {

  return (
    <>
      <div className="container-fluid bg-light p-2 min-vh-100">
        <h4 className="p-3 font-weight-bold text-secondary">Formación</h4>
        <TableCoursesAdministrador />
      </div>
    </>
  );
}

export default CoursesAdministrador;