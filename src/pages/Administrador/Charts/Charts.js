import React, { useEffect } from 'react';
import CostChart from "../../../components/Chart/Production/CostChart"
import CostCourseChart from "../../../components/Chart/Course/CostChart";

import CategoryChart from '../../../components/Chart/Production/CategoryChart';
import LanguageChart from '../../../components/Chart/Production/LanguageChart';
import CulturalSpacePartidoChart from '../../../components/Chart/CulturalSpace/CulturalSpacePartidoChart';
import CantCulturalSpacesChart from '../../../components/Chart/CulturalSpace/CantCulturalSpacesChart';
import CantCulturalSpacesRelevamientoChart from '../../../components/Chart/CulturalSpace/CantCulturalSpacesRelevamientoChart';
import { useDispatch, useSelector } from 'react-redux';
import { getProductions } from '../../../actions/productionAction';
import { getCulturalSpaces } from '../../../actions/culturalSpaceAction';
import './Charts.css';
import { getCoordinadores } from '../../../actions/usersAction';
import { getCourses } from '../../../actions/courseAction';
import CategoryCourseChart from '../../../components/Chart/Course/CategoryChart';
import LanguageCourseChart from '../../../components/Chart/Course/LanguageChart';

const ChartsAdministrador = () => {

    const dispatch = useDispatch();
    const productions = useSelector(state => state.productions.productions);
    const cultural_spaces = useSelector(state => state.cultural_spaces.cultural_spaces);
    const coordinadores = useSelector(state => state.users.coordinadores)
    const courses = useSelector(state => state.courses.courses)

    useEffect(() => {
        dispatch(getProductions())
        dispatch(getCulturalSpaces())
        dispatch(getCoordinadores())
        dispatch(getCourses())
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <>
            <div className="container-fluid bg-light p-2">
                <h4 className="p-3 font-weight-bold text-secondary">Reportes</h4>
                    <div className="container-fluid container-content justify-content-center section-cards">
                    <div className="row">
                    <div className="col-xl-3 col-lg-6 col-12">
                            <div className="card-top shadow">
                                <div className="desc">
                                    <span className="number-card">{coordinadores.length}
                                    </span>
                                    <p className="texto-card">Coordinadores</p>
                                </div>
                                <i className="bi bi-people-fill icono"></i>
                            </div>
                        </div>
                        <div className="col-xl-3 col-lg-6 col-12">
                            <div className="card-top shadow">
                                <div className="desc">
                                    <span className="number-card">{cultural_spaces.length}
                                    </span>
                                    <p className="texto-card">Espacios Culturales</p>
                                </div>
                                <i className="bi bi-easel-fill icono"></i>
                            </div>
                        </div>
                        <div className="col-xl-3 col-lg-6 col-12">
                            <div className="card-top shadow">
                                <div className="desc">
                                    <span className="number-card">{productions.length}
                                    </span>
                                    <p className="texto-card">Producciones</p>
                                </div>
                                <i className="bi bi-play-circle-fill icono"></i>
                            </div>
                        </div>
                        <div className="col-xl-3 col-lg-6 col-12">
                            <div className="card-top shadow">
                                <div className="desc">
                                    <span className="number-card">{courses.length}
                                    </span>
                                    <p className="texto-card">Cursos y talleres</p>
                                </div>
                                <i className="bi bi-palette-fill icono"></i>
                            </div>
                        </div>
                        
                    </div>
                    </div>
                    <div className="container-fluid m-2">
                    {/* <div className="row justify-content-center "> */}
                    {/* <div className="card shadow mb-6" style={{borderLeft:0}}> */}
    <div className="card-header py-3 bg-secondary">
      <h5 className="font-weight-bold text-white text-center align-items-center">Producciones</h5>
    </div>
                    <div className="row justify-content-center m-2">

                        <div className="col-md-4">
                            <CostChart productions={productions} />
                        </div>
                        <div className="col-md-7">
                            <CategoryChart productions={productions} />
                        </div>
</div>
                    <div className="row justify-content-center m-2">

                        <div className="col-md-10">
                            <LanguageChart productions={productions} />
                        {/* </div> */}
                        </div></div>
</div>
 <div className="container-fluid m-2">
                    {/* <div className="row justify-content-center "> */}
                    {/* <div className="card shadow mb-6"> */}
    <div className="card-header py-3 bg-secondary">
      <h5 className="font-weight-bold text-white text-center align-items-center">Cursos y talleres</h5>
    </div>
    <div className="row justify-content-center m-2">
                <div className="col-md-4">
                            <CostCourseChart courses={courses} />
                            </div>                <div className="col-md-6">

                            <CategoryCourseChart courses={courses}/>
                            </div>
                            <div className="row justify-content-center m-2">

                            <div className="col-md-10">

<LanguageCourseChart courses={courses}/>
</div>
</div>
                        
        </div>
    {/* </div> */}
 <div className="container-fluid m-2">
                    {/* <div className="row justify-content-center "> */}
                    <div className="card shadow mb-6">
    <div className="card-header py-3 bg-secondary">
      <h5 className="font-weight-bold text-white text-center align-items-center">Espacios culturales</h5>
    </div>
    <div className="row justify-content-center m-2">

                        <div className="col-md-4">
                            <CantCulturalSpacesChart />
                        </div>
                        <div className="col-md-4">
                            <CulturalSpacePartidoChart />
                        </div>
                        <div className="col-md-4">
                            <CantCulturalSpacesRelevamientoChart />
                        </div>
                    </div>
                </div>
                </div></div>
                </div>
            </>
            );
}

export default ChartsAdministrador;