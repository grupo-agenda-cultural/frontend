import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { useParams } from "react-router-dom";
import { resetPasswordConfirm } from '../../../actions/authentication';
import logo from '../../../assets/images/logo/logo.png';
import { Formik, Form } from "formik";
import * as Yup from 'yup'
import { FormControl, FormGroup, InputGroup } from 'react-bootstrap';
import "./ResetPassword.css";

const ResetPassword = () => {
  const dispatch = useDispatch();
  const { uid, token } = useParams();
  const [shown, setShown] = useState(false);
  const [shownConfirm, setShownConfirm] = useState(false);

  const switchShown = () => setShown(!shown);
  const switchShownConfirm = () => setShownConfirm(!shownConfirm);
  const handleSubmit = (values) => {
    dispatch(resetPasswordConfirm (uid, token, values.new_password, values.re_new_password));
  };

  const ErrorMessagesSchema = Yup.object().shape({
        new_password: Yup.string().required('Campo requerido').min(8,'La contraseña debe tener al menos 8 caracteres'),
        re_new_password: Yup.string().required('Campo requerido').oneOf([Yup.ref('new_password'),null],'Las contraseñas no coinciden')

      });
    return (
        <div className="main-password">
<div className="container-pass">
<div className="form-container-password pt-5">

<div className="logo-password">
          <img src={logo} alt="logo" />
        </div>
        <Formik
          initialValues={{ new_password:'', re_new_password:''}}
          validationSchema={ErrorMessagesSchema}
          onSubmit={(values, { resetForm }) => {
            handleSubmit(values);
            resetForm();
          }}>
          {({ errors, touched, handleChange, handleBlur, values }) => (
            <Form className="formulario-password">
              <div className="heading-password">
                <h2>Resetear contraseña</h2>
              </div>
              <div className="row">
                <div className="col-md-12 form-group mt-2 p-2">
                  <label htmlFor="exampleInputEmail1">Contraseña</label>
                  <FormGroup >       
          <InputGroup>
                    <FormControl
                  type={shown ? 'text' : 'password'}
                  name="new_password"
                  className="form-control" 
                  placeholder="Ingresá tu contraseña" 
                  value={values.new_password}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  isInvalid={touched.new_password && errors.new_password}
/>
<span className="input-group-text" onClick={switchShown} id="new_password"> {shown ? <i className="bi bi-eye-slash-fill"></i> : <i className="bi bi-eye-fill"></i>}</span>

              <FormControl.Feedback type="invalid">
                      {errors.new_password}
                    </FormControl.Feedback>
                    </InputGroup>
                    
              </FormGroup>  
              </div>
              </div>
              <div className="row">
                <div className="col-md-12 form-group mt-2 p-2">
                  <label htmlFor="exampleInputConfirmPassword">Confirmación de contraseña</label>
                  <FormGroup >       
          <InputGroup>
                    <FormControl
                      type={shownConfirm ? 'text' : 'password'}
                      name="re_new_password"
                      className="form-control"
                      placeholder="Confirmá tu nueva contraseña"
                      aria-label="re_new_password"
                      aria-describedby="re_new_password" 
                      value={values.re_new_password}
                  onChange={handleChange}
                  onBlur={handleBlur}
                      isInvalid={touched.re_new_password && errors.re_new_password}
                      // valid={touched.password && !errors.password}
                      />
<span className="input-group-text" onClick={switchShownConfirm} id="new_password"> {shown ? <i className="bi bi-eye-slash-fill"></i> : <i className="bi bi-eye-fill"></i>}</span>

              <FormControl.Feedback type="invalid">
                      {errors.re_new_password}
                    </FormControl.Feedback>
                    </InputGroup>
                    
              </FormGroup>  

</div>
</div>              <div className="button-confirm">
              <input type="submit" value="Enviar" className="password-btn" />
</div>
              <div className="text-acca">
                <p className="copyright">&copy;2022 Agenda y Comunicación Cultural y Artística</p>
              </div>
    </Form>
    )}
    </Formik>
</div>
</div>
</div>
    );
}

export default ResetPassword;