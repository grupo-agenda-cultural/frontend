import React from 'react';
import { useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import { resetPassword } from '../../../actions/authentication';
import logo from '../../../assets/images/logo/logo.png';
import { Formik, Form } from "formik";
import * as Yup from 'yup'
import "./ResetPassword.css";
import { FormGroup, FormControl } from 'react-bootstrap';

const ResetPassword = () => {

  const dispatch = useDispatch();

  const handleSubmit = (values) => {
    dispatch(resetPassword(values.email));
  }

    const ErrorMessagesSchema = Yup.object().shape({
        email: Yup.string().email('Email inválido').required('Debe ingresar su email para restablecer su contraseña'),
      });
    return (
        <div className="main-password">
<div className="container-pass">
<div className="form-container-password ">
<div className="return-form">
  <Link to="/login"> Atrás</Link>
</div>
<div className="logo-password">
          <img src={logo} alt="logo" />
        </div>
        <Formik
          initialValues={{ email: ''}}
          validationSchema={ErrorMessagesSchema}
          onSubmit={(values, { resetForm }) => {
            handleSubmit(values);
            resetForm();
          }}>
          {({ errors, touched,values, handleSubmit, handleChange, handleBlur }) => (
            <Form onSubmit={handleSubmit} className="formulario-password">
              <div className="heading-password">
                <h2>¿Olvidaste tu contraseña?</h2>
                <h6>Ingresá tu email y recibí las instrucciones para que puedas reestablecer tu contraseña.</h6>
              </div>
              <div className="row">
                <div className="col-md-12 form-group mt-4 mb-3 p-2">
                  <label htmlFor="exampleInputEmail1">Email</label>
                  <FormGroup>
                  <FormControl
                    type="email"
                    name="email"
                    value={values.email}
                    className="form-control"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    isInvalid={touched.email && errors.email}
              />
                  <FormControl.Feedback type="invalid">
                      {errors.email}
                    </FormControl.Feedback>
                    </FormGroup>

              <input type="submit" value="Enviar" className="password-btn" />

              <div className="text-acca">
                <p className="copyright">&copy;2022 Agenda y Comunicación Cultural y Artística</p>
              </div>
</div>
</div>
    </Form>
    )}
    </Formik>
</div>
</div>
</div>
    );
}

export default ResetPassword;