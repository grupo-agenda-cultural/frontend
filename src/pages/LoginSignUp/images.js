import img1 from '../../assets/images/login/multiespacio.jpg';
import img2 from '../../assets/images/login/obra2.jpg';


const images = [
    {
        "id": 0,
        "image": img1
    },
    {
        "id": 1,
        "image": img2
    },
    // {
    //     "id": 2,
    //     "image": img3
    // },
    // {
    //     "id": 3,
    //     "image": img4
    // }
]

const random = Math.floor((Math.random() * (images.length)));

export default images[random].image;