import React, { useState } from 'react';
import LoginForm from '../../components/Login/LoginForm';
import '../../components/Login/Login.css';
import images from './images';
import SignUpForm from '../../components/SignUp/SignUpForm';

const LoginSignUp = () => {

    const [slide, setSlide] = useState("");

    return (
        <div className="main">
            <div className={`container-main ${slide}`} id="container-main">
                <SignUpForm slide={setSlide} />
                <LoginForm slide={setSlide} />
                <div className="overlay-container">
                    <div className="overlay">
                        <div className="overlay-panel overlay-left">
                            <img src={images} className="image show" alt="" />
                        </div>
                        <div className="overlay-panel overlay-right">
                            <div className="container-imagen">
                                <img src={images} className="image show" alt="" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default LoginSignUp;