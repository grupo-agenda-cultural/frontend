import React from 'react';
import CulturalSpace from '../../../components/CulturalSpace/CulturalSpace';

const CulturalSpaceCoordinador = () => {

  return (
    <>
      <div className="container-fluid bg-light">
      <h4 className="p-3 font-weight-bold text-secondary">Mi espacio cultural</h4>
        <CulturalSpace />
      </div>
    </>
  );
};

export default CulturalSpaceCoordinador;