import React from 'react';
import imagen from '../../../assets/images/manual/user-manual.jpg';
import { connect } from 'react-redux';

const Coordinador = ({usuario}) => {
    return (
        <>
            <div className="container-fluid bg-light p-2">
                <h4 className="p-3 font-weight-bold text-secondary">Inicio</h4>
                <div className="row">
                    <div className="col-md-12 col-lg-12">
                        <div className="card p-2 m-2">
                            <div className=" card-body">
                                <h5 className="card-title">
                                    <i className="bi bi-house-door-fill" style={{ color: "#01a579", fontSize: "40px" }}></i></h5>
                                
                                <h3 className="card-text font-weight">¡Hola {usuario.name} {usuario.last_name}!</h3>
                                <p>Estas son algunas de las acciones que puede realizar con su rol de coordinador.</p>

                            </div>
                        </div>

                    </div>
                </div>
                <div className="row m-2 justify-content-center">
                    <div className="col-md-4">
                        <div className="card mb-4 p-2" style={{ height: "15em", borderColor: "#FA5100" }}>
                            <div className="card-body">
                                <h5 className="card-title">                            <i className="bi bi-pencil-square" style={{ color: "#FA5100", fontSize: "40px" }}></i>
                                </h5>
                                <h3 className="card-text font-weight ">Crear espacio cultural</h3>
                                <p>Crear su propio espacio cultural detallando el nombre, la ubicación y los datos de contacto</p>



                            </div>
                        </div>
                    </div>
                    <div className="col-md-4">
                        <div className="card border-primary mb-4 p-2" style={{ height: "15em" }}>
                            <div className="card-body ">
                                <h5 className="card-title"><i className="bi bi-palette-fill" style={{ color: "#487EF7", fontSize: "40px" }}></i>
                                </h5>
                                <h3 className="card-text font-weight">Agregar las actividades</h3>
                                <p>Agregar las producciones y talleres de su espacio cultural</p>



                            </div>
                        </div>
                    </div>
                    <div className="col-md-4">
                        <div className="card p-2" style={{ height: "15em", borderColor: "#9120FA" }}>                            <div className="card-body">
                            <h5 className="card-title"><i className="bi bi-pie-chart-fill" style={{ color: "#9120FA", fontSize: "40px" }}></i>
                            </h5>
                            <h3 className="card-text font-weight ">Generar reportes</h3>
                            <p>Generar reportes estadísticos de las actividades que se realizan en su espacio cultural</p>
                        </div>
                        </div>
                    </div>
                </div>
                <div className="row justify-content-center">
                    <div className="col-xs-12 col-sm-12 col-md-12 col-lg-9">
                        <div className="card p-2">
                            <div className="card-body">
                                <h2 className="card-title pb-4 text-center"><i className="bi bi-journals" style={{ color: "#FA2D44", fontSize: "40px" }}></i>   Manual de uso del coordinador</h2>
                                <div className="row justify-content-center">
                                    <div className="col-md-10 col-md-offset-10 col-xs-12" >
                                        <div className="btn-group d-flex" role="group">

                                            <button type="button" className="btn btn-primary">Ver manual de uso</button>

                                            <button type="button" className="btn btn-outline-secondary">Descargar manual de uso</button>
                                        </div>
                                    </div>

                                </div>
                                <img src={imagen} style={{ width: "100%" }} alt="user-manual" />

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </>
    );
}

// Get Data from redux store
const mapStateToProps = (state) => ({
    // Check Authentictaion
    usuario: state.authentication.user,
  });

export default connect(mapStateToProps)(Coordinador) ;