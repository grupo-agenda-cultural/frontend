import  React,{ useState } from 'react';
import ModalAdd from '../../../components/Production/Modals/ModalAdd';
import { Button } from '../../../components/Button/Button';
import { connect } from 'react-redux';
import Error from '../../../components/Error/Error';
import TableProductionsCoordinador from '../../../components/Production/Tables/TableProductionsCoordinador';

const ProductionsCoordinador = ({ coordinador}) => {


  const [showModalAdd, setShowModalAdd]= useState(false);
  return (
    <>
      <div className="container-fluid bg-light p-2 min-vh-100">
        {coordinador.cultural_space !== '' ?
        <>
        <h4 className="p-3 font-weight-bold text-secondary">Producciones</h4>
        <Button type="button" onClick={()=> setShowModalAdd(true)} className="btn btn-success m-3" icon="bi bi-plus"> Agregar producción </Button>
        <TableProductionsCoordinador />  
        </>
        :
          <Error path='/coordinador/espacio-cultural'/>
        }
      </div>
     <ModalAdd show={showModalAdd} onHide={()=> setShowModalAdd(false)} setOnHide={setShowModalAdd} />
    </>
  );
}
// Get Data from redux store
const mapStateToProps = (state) => ({
  // Check Authentictaion
  isAuthenticated: state.authentication.isAuthenticated,
  coordinador: state.authentication.coordinador,
});

export default connect(mapStateToProps)(ProductionsCoordinador) ;