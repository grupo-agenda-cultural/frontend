import  React from 'react';
import { connect } from 'react-redux';
import Error from '../../../components/Error/Error';
import Charts from './Charts';

const ChartsCoordinador = ({ coordinador}) => {

  return (
    <>
        {coordinador.cultural_space !== '' ?
        <>
        <Charts/>
        </>
        :
          <Error path='/coordinador/espacio-cultural'/>
        }
    </>
  );
}
// Get Data from redux store
const mapStateToProps = (state) => ({
  // Check Authentictaion
  isAuthenticated: state.authentication.isAuthenticated,
  coordinador: state.authentication.coordinador,
});

export default connect(mapStateToProps)(ChartsCoordinador) ;