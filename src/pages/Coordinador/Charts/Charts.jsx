import React, { useEffect } from 'react';
import CostChart from "../../../components/Chart/Production/CostChart"
import CategoryChart from '../../../components/Chart/Production/CategoryChart';
import LanguageChart from '../../../components/Chart/Production/LanguageChart';
import CategoryCourseChart from '../../../components/Chart/Course/CategoryChart';
import LanguageCourseChart from '../../../components/Chart/Course/LanguageChart';
import CostCourseChart from '../../../components/Chart/Course/CostChart';
import { useDispatch, useSelector } from 'react-redux';
import { getProductionsByMyCulturalSpace } from '../../../actions/productionAction';
import { getCoursesByMyCulturalSpace } from '../../../actions/courseAction';

import './Charts.css';

const Charts = () => {

    const dispatch = useDispatch();
    const productions = useSelector(state => state.productions.productions);
    const courses = useSelector(state => state.courses.courses);

    useEffect(() => {
        dispatch(getProductionsByMyCulturalSpace ())
        dispatch(getCoursesByMyCulturalSpace ())
        // eslint-disable-next-line
    }, []);

    return (
        <>
            <div className="container-fluid bg-light p-2">
                <h4 className="p-3 font-weight-bold text-secondary">Reportes</h4>
                    <div className="container-fluid container-content justify-content-center section-cards">
                    <div className="row">
                        <div className="col-xl-4 col-lg-6 col-12">
                            <div className="card-top shadow">
                                <div className="desc">
                                    <span className="number-card">{productions.length}
                                    </span>
                                    <p className="texto-card">Producciones</p>
                                </div>
                                <i className="bi bi-play-circle-fill icono"></i>
                            </div>
                        </div>
                        <div className="col-xl-4 col-lg-6 col-12">
                            <div className="card-top shadow">
                                <div className="desc">
                                    <span className="number-card">{courses.length}
                                    </span>
                                    <p className="texto-card">Cursos y talleres</p>
                                </div>
                                <i className="bi bi-palette-fill icono"></i>
                            </div>
                        </div>
                        
                    </div>
                    </div>
                    <div className="container-fluid m-2">
                    <div className="card shadow mb-6">
    <div className="card-header py-3 bg-secondary">
      <h5 className="font-weight-bold text-white text-center align-items-center">Producciones</h5>
    </div>
                    <div className="row justify-content-center m-2">

                        <div className="col-md-4">
                            <CostChart productions={productions} />
                        </div>
                        <div className="col-xl-6 col-md-6">
                            <CategoryChart productions={productions} />
                        </div>

                        <div className="col-md-12 col-xl-9 ">
                            <LanguageChart productions={productions} />
                        {/* </div> */}
                        </div></div>
</div>
 <div className="container-fluid m-2">
                    {/* <div className="row justify-content-center "> */}
                    <div className="card shadow mb-6">
    <div className="card-header py-3 bg-secondary">
      <h5 className="font-weight-bold text-white text-center align-items-center">Cursos y talleres</h5>
    </div>
    <div className="row justify-content-center m-2">
        <div className="col-md-4">
                            <CostCourseChart courses={courses} />
                        </div>
                        <div className=" col-md-6">
                            <CategoryCourseChart courses={courses} />
                        </div>

                        <div className="col-md-10 ">
                            <LanguageCourseChart courses={courses} />
                        </div>
        </div>
        </div>
    </div>
 </div>
                </div>
            </>
            );
}

export default Charts;