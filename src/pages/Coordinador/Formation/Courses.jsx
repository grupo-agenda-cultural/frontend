import React ,{ useState } from 'react';
import { connect } from 'react-redux';
import ModalAdd from '../../../components/Course/Modals/ModalAdd';
import { Button } from '../../../components/Button/Button';
import TableCoursesCoordinador from '../../../components/Course/Tables/TableCoursesCoordinador';
import Error from '../../../components/Error/Error';
const CoursesCoordinador = ({coordinador}) => {

  const [showModalAdd, setShowModalAdd]= useState(false);


  return (
    <>
      <div className="container-fluid bg-light p-2">
      {coordinador.cultural_space !== '' ?
        <>
      <h4 className="p-3 font-weight-bold text-secondary">Talleres y cursos</h4>
      <Button type="button" className="btn btn-success m-3"  onClick={()=> setShowModalAdd(true)}  icon="bi bi-plus"> Agregar taller </Button>
      <TableCoursesCoordinador/>
      </>
        :
          <Error path='/coordinador/espacio-cultural'/>
        }
      </div>
      <ModalAdd show={showModalAdd} onHide={()=> setShowModalAdd(false)} setOnHide={setShowModalAdd} />
    </>
  );
}

// Get Data from redux store
const mapStateToProps = (state) => ({
  // Check Authentictaion
  isAuthenticated: state.authentication.isAuthenticated,
  coordinador: state.authentication.coordinador,
});

export default connect(mapStateToProps)(CoursesCoordinador) ;