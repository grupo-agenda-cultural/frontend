import React from 'react';
import {connect} from 'react-redux';
import { Route, Navigate } from 'react-router-dom';

const PrivateRoute = ({user, isAuthenticated, component: Component, ...rest}) => {
    
    return(
    <Route {...rest} component={(props) => (
        isAuthenticated ? (
            <div>
             <Navigate to="/" />           
            </div>                   
        ): (
            <Component {...props} />
        )
    )} 
    />
)}

const mapStateToProps = (state) => ({
    isAuthenticated: state.authentication.isAuthenticated,
    user: state.authentication.user
  });

export default connect(mapStateToProps)(PrivateRoute);