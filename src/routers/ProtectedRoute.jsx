import React from 'react';
import { connect } from 'react-redux';
import { Navigate, Outlet } from 'react-router-dom';

    const ProtectedRoute = ({
        user,
        isAuthenticated,
        redirectPath = '/login',
        isAllowed,
        children
      }) => {

        if (!isAuthenticated || user === null) {
          return <Navigate to={redirectPath} replace />;
        }
        if (isAllowed !== user.groups[0].id ) {      
           
          return <Navigate to={redirectPath} replace />;
        }
      
      
        return children ? children : <Outlet />;
      };

const mapStateToProps = (state) => ({
    isAuthenticated: state.authentication.isAuthenticated,
    user: state.authentication.user
});

export default connect(mapStateToProps)(ProtectedRoute);