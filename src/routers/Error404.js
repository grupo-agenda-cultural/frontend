import React from 'react';
import error from '../assets/images/error/error-404.jpg';
import './Error404.css';

const Error404 = () => {


    const clickBack = () => {
        window.history.back();
    }
    return (

        <div className="error-404">
        {/* <div className="logo-error">
                <img src={logo} className="img-logo-404" alt="logo"/>
            </div>  */}
            <div className="img-error">
                <img src={error} className="img-error-404" alt="img-404"/>
            </div>
            <div className="error-text-404">
            <div className="texto-error">
                <h1>Está página no está disponible</h1>
                
                 <p className="parrado-error text-center">Es posible que el enlace que seguiste no existe o que haya sido eliminado.</p>
               </div>
                <div className="button-back">
                <button className="btn btn-primary" onClick={clickBack}>Volver atrás</button>
                </div>
            </div>
        </div>


    );
}

export default Error404;