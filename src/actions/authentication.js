import { types } from '../types/types';
import { notificationError, notificationTimer, notificationRegister, notification,  notificationConfirmPassword } from '../utils/alert';
import axiosInstance from '../services/axiosApi'

// Register user
export const register = (values) => async dispatch => {
  try {
      const response = await axiosInstance.post('/auth/register/', {
        name: values.name,
        last_name: values.lastname,
        email: values.email,
        phone: values.phone,
        password: values.password,
        is_active: false
      });
          if (response.status === 201 || response.status === 200) {
            dispatch({
              type: types.AUTHENTICATED_SUCCESS,
              payload: response.data
           });
            notificationRegister("Registro exitoso", "Se registró la cuenta con éxito", 'success');
        }  
   } catch (error) {
      dispatch({
            type: types.AUTHENTICATED_FAILURE
    })
      notificationError('Error', 'Error al registrarse','error' );
   }
};


// Login user
export const login = (email, password) => async dispatch => {
    try {
        const response = await axiosInstance.post('/auth/login/', {
            email: email,
            password: password
          }
        );
          const body = await response.data;
          if ( response.status === 200 ||  response.status === 201 ){
            localStorage.setItem('access_token', body.access_token);
            localStorage.setItem('refresh_token', body.refresh_token);
            axiosInstance.defaults.headers['Authorization'] = 
                    'JWT ' + localStorage.getItem('access_token')
            dispatch({
                type: types.LOGIN_SUCCESS,
                payload: response.data
             });
             notificationTimer( 'Inicio de sesión exitoso' );
             dispatch(loadUser());
            }
                  
     } catch (error) {
        notificationError('Error', 'Hubo un problema al iniciar sesión','error' );

        dispatch({
                type: types.LOGIN_FAILURE
             })
     }
};

// CURRENT USER
export const loadUser = () => async (dispatch) => {
    if (localStorage.getItem('access_token')) {
      try {
        const response = await axiosInstance.get("/auth/login/me/", {
          headers: {
            "Content-Type": "application/json",
            Authorization: `JWT ${localStorage.getItem('access_token')}`,
            Accept: "application/json",
          },
        });
        dispatch({
          type: types.LOAD_USER_SUCCESS,
          payload: response.data,
          // payload: response.data.user,

        });
    
      } catch (err) {
        dispatch({
          type: types.LOAD_USER_FAILURE,
        });
      }
    } else {
      dispatch({
        type: types.LOAD_USER_FAILURE,
      });
    }
  };

// LOGOUT
export const logout = () => async (dispatch) => {
  try{  dispatch({
    type: types.LOGOUT_SUCCESS,
  });
}catch(error)
{
  dispatch({
    type: types.LOGOUT_FAILURE,
  });
}
};


export const resetPassword = (email) => async dispatch => {
  const config = {
      headers: {
          'Content-Type': 'application/json'
      }
  };

  const body = JSON.stringify({ email });

  try {
      await axiosInstance.post(`/auth/users/reset_password/`, body, config);

      dispatch({
          type: types.PASSWORD_RESET_SUCCESS
      });
      notification("Restablecimiento de contraseña",'Se envío el email con las instrucciones con éxito.', 'success')
  } catch (err) {
      dispatch({
          type: types.PASSWORD_RESET_FAILURE
      });
      notificationError("Restablecimiento de contraseña",'Ocurrió un error al enviar el email.  Intente nuevamente.', 'error')

  }
};

export const resetPasswordConfirm = (uid, token, new_password, re_new_password) => async dispatch => {
  const config = {
      headers: {
          'Content-Type': 'application/json'
      }
  };

  const body = JSON.stringify({ uid, token, new_password, re_new_password });
  try {
      await axiosInstance.post(`/auth/users/reset_password_confirm/`, body, config);

      dispatch({
          type: types.PASSWORD_RESET_CONFIRM_SUCCESS
      });
      notificationConfirmPassword("Restablecimiento de contraseña",'Se modificó la contraseña con éxito.', 'success')
  } catch (err) {
      dispatch({
          type: types.PASSWORD_RESET_CONFIRM_FAILURE
      });
      notificationError("Restablecimiento de contraseña",'Ocurrió un error al querer modificar la contraseña. Intente nuevamente.', 'error')

  }
};