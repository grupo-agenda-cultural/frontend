import axiosInstance from '../services/axiosApi'
import { notification } from "../utils/alert"
import { types } from '../types/types';

export const addProduction = (coordinador, values, date_start, date_end) => async (dispatch) => {
  try {
    const formData = new FormData()
    formData.append("image", values.image);
    formData.append("name", values.name);
    formData.append("organizer_person", values.organizer_person);
    formData.append("description", values.description);
    formData.append("date_start", date_start);
    formData.append("date_end", date_end);
    formData.append("language", values.language);
    formData.append("modality", values.modality);
    formData.append("category", values.category);
    formData.append("cost", values.cost);
    formData.append("price", values.price);
    formData.append("cultural_space", coordinador.cultural_space.id);
    await axiosInstance.post(`/productions/add/`, formData, {
      headers: {
        "Content-Type": "application/json",
        Authorization: `JWT ${localStorage.getItem('access_token')}`,
        Accept: "application/json"
      },
    }).then(res => {
      dispatch({
        type: types.ADD_PRODUCTION_SUCCESS,
        payload: res.data
      })
      if (res.status === 200 || res.status === 201) {
        notification("Producción agregada", "Se agregó el registro con éxito", 'success');
        dispatch(getProductionsByMyCulturalSpace())
      }
    })
  } catch (error) {
    dispatch({
      type: types.ADD_PRODUCTION_FAILURE,
    });
    notification("Error", "Ha ocurrido un error. Intente nuevamente", 'error');
  }
}

export const updateProduction = (values, date_start, date_end) => async (dispatch) => {
  try {
    const formData = new FormData()
    if (typeof values.image === 'object')
      formData.append("image", values.image);
    formData.append("name", values.name);
    formData.append("organizer_person", values.organizer_person);
    formData.append("description", values.description);
    formData.append("date_start", date_start);
    formData.append("date_end", date_end);
    formData.append("language", values.language);
    formData.append("modality", values.modality);
    formData.append("category", values.category);
    formData.append("cost", values.cost);
    formData.append("price", values.price);
    
    await axiosInstance.patch(`/productions/update/${values.id}/`, formData, {
      headers: {
        "Content-Type": "application/json",
        Authorization: `JWT ${localStorage.getItem('access_token')}`,
        Accept: "application/json"
      },
    })
      .then(res => {
        dispatch({
          type: types.UPDATE_PRODUCTION_SUCCESS,
          payload: res.data
        })
        if (res.status === 200 || res.status === 201) {
          notification("Producción editada", "Se editó el registro con éxito", 'success');
          dispatch(getProductionsByMyCulturalSpace())
        }
      })
  } catch (error) {
    dispatch({
      type: types.UPDATE_PRODUCTION_FAILURE
    });
    notification("Error", "Ha ocurrido un error. Intente nuevamente", 'error');
  }
}

export const deleteProduction = ({ production }) => async (dispatch) => {
  try {
    const { id } = production;
    await axiosInstance.delete(`/productions/delete/${id}/`, {
      headers: {
        "Content-Type": "application/json",
        Authorization: `JWT ${localStorage.getItem('access_token')}`,
        Accept: "application/json",
      }
    })
      .then(res => {
        dispatch({
          type: types.DELETE_PRODUCTION_SUCCESS,
          payload: res.data.id,
        })
        if (res.status === 200) {
          notification("Producción eliminada", "Se eliminó el registro con éxito", 'success');
          dispatch(getProductionsByMyCulturalSpace())
        }
      })

  } catch (error) {
    dispatch({
      type: types.DELETE_PRODUCTION_FAILURE,
    })
    notification("Error", "Ha ocurrido un error. Intente nuevamente", 'error');
  }
}

export const getProductions = () => async (dispatch) => {
  try {
    await axiosInstance.get('/productions/')
      .then(res => {
        dispatch({
          type: types.GET_PRODUCTIONS_SUCCESS,
          payload: res.data,
        })
      })
  } catch (error) {
    dispatch({
      type: types.GET_PRODUCTIONS_FAILURE
    });
  }
}

export const getProductionsByMyCulturalSpace = () => async (dispatch) => {
  try {
    await axiosInstance.get('/productions/my-cultural-space/', {
      headers: {
        "Content-Type": "application/json",
        Authorization: `JWT ${localStorage.getItem('access_token')}`,
        Accept: "application/json",
      }
    })
      .then(res => {
        dispatch({
          type: types.GET_PRODUCTIONS_BY_MY_CULTURAL_SPACE_SUCCESS,
          payload: res.data,
        })
      })
  } catch (error) {
    dispatch({
      type: types.GET_PRODUCTIONS_BY_MY_CULTURAL_SPACE_FAILURE
    });
  }
}

export const getProductionsCalendar = () => async (dispatch) => {
  try {
    await axiosInstance.get(`/productions/calendar/`)
      .then(res => {
        dispatch({
          type: types.GET_PRODUCTIONS_CALENDAR_SUCCESS,
          payload: res.data,
        })
      }
      )
  } catch (error) {
    dispatch({
      type: types.GET_PRODUCTIONS_CALENDAR_FAILURE,
    })
  }
}

export const getProductionCalendar = (id) => async (dispatch) => {
  try {
    await axiosInstance.get(`/productions/calendar/${id}/`)
      .then(res => {
        dispatch({
          type: types.GET_PRODUCTION_CALENDAR_SUCCESS,
          payload: res.data,
        })
      }
      )
  } catch (error) {
    dispatch({
      type: types.GET_PRODUCTION_CALENDAR_FAILURE,
    });
  }
}

export const getProductionCalendarbyCulturalSpace = (id) => async (dispatch) => {
  try {
    await axiosInstance.get(`/productions/calendar/culturalspaces/${id}/`)
      .then(res => {
        dispatch({
          type: types.GET_PRODUCTIONS_BY_CULTURAL_SPACE_SUCCESS,
          payload: res.data,
        })
      }
      )
  } catch (error) {
    dispatch({
      type: types.GET_PRODUCTIONS_BY_CULTURAL_SPACE_FAILURE
    })
  }
}



