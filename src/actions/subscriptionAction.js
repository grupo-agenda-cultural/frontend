import axiosInstance from '../services/axiosApi';
import { notification, notificationSubscription } from "../utils/alert";
import { types } from '../types/types';

export const addSubscription = (values) => async (dispatch) => {
  try {
    await axiosInstance.post(`/subscriptions/cultural_space/add/`,{
        email: values.email,
        cultural_space: values.id,
    }).then(res => {
          dispatch({
              type: types.ADD_SUBSCRIPTION_SUCCESS,
              payload: res.data
          })
          if (res.status === 200 || res.status === 201 ){
            notification("Suscripción exitosa","Te vas a enterar de todas las producciones y eventos",'success');
          }else{
            notification("Error", "Ha ocurrido un error. Intente nuevamente",'error');
          }
    })
  } catch (err) {
    dispatch({
      type: types.ADD_SUBSCRIPTION_FAILURE
    });

  }}



export const addSubscriptionCalendar = (values) => async (dispatch) => {
  try {
    await axiosInstance.post(`/subscriptions/calendar/add/`,{
        email: values.email,
    }).then(res => {
          dispatch({
              type: types.ADD_SUBSCRIPTION_CALENDAR_SUCCESS,
              payload: res.data
          })
          if (res.status === 200 || res.status === 201 ){
            notificationSubscription("Suscripción exitosa",'success');
          }else{
            notificationSubscription("Error", "Ha ocurrido un error. Intente nuevamente",'error');
          }
    })
  } catch (err) {
    dispatch({
      type: types.ADD_SUBSCRIPTION_CALENDAR_FAILURE
    });

  }}