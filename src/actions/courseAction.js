import axiosInstance from '../services/axiosApi'
import { notification } from "../utils/alert"
import { types } from '../types/types';

export const addCourse = (coordinador, values, inscription_start, inscription_end) => async (dispatch) => {

  try {
    const formData = new FormData()
    formData.append("image", values.image);
    formData.append("name", values.name);
    formData.append("teacher", values.teacher);
    formData.append("description", values.description);
    formData.append("date", values.date);
    formData.append("hour", values.hour);
    formData.append("content", values.content);
    formData.append("language", values.language);
    formData.append("formation", values.formation);
    formData.append("category", values.category);
    formData.append("duration", values.duration);
    formData.append("modality", values.modality);
    formData.append("requirements", values.requirements);
    formData.append("inscription_start", inscription_start);
    formData.append("inscription_end", inscription_end);
    formData.append("cost", values.cost);
    formData.append("price", values.price);
    formData.append("observations", values.observations);
    formData.append("cultural_space", coordinador.cultural_space.id);
    await axiosInstance.post(`/courses/add/`, formData, {
      headers: {
        "Content-Type": "application/json",
        Authorization: `JWT ${localStorage.getItem('access_token')}`,
        Accept: "application/json"
      },
    }).then(res => {
      dispatch({
        type: types.ADD_COURSE_SUCCESS,
        payload: res.data
      })
      if (res.status === 200 || res.status === 201) {
        notification("Taller agregado", "Se agregó el registro con éxito", 'success');
      }
      dispatch(getCoursesByMyCulturalSpace());
    })
  } catch (error) {
    dispatch({
      type: types.ADD_COURSE_FAILURE,
    })
    notification("Error", "Ha ocurrido un error. Intente nuevamente", 'error');

  }
}
export const updateCourse = (values, inscription_start, inscription_end) => async (dispatch) => {
  try {
    const formData = new FormData()
    if (typeof values.image === 'object')
      formData.append("image", values.image);
    formData.append("name", values.name);
    formData.append("teacher", values.teacher);
    formData.append("description", values.description);
    formData.append("date", values.date);
    formData.append("hour", values.hour);
    formData.append("content", values.content);
    formData.append("language", values.language);
    formData.append("formation", values.formation);
    formData.append("category", values.category);
    formData.append("duration", values.duration);
    formData.append("modality", values.modality);
    formData.append("requirements", values.requirements);
    formData.append("inscription_start", inscription_start);
    formData.append("inscription_end", inscription_end);
    formData.append("cost", values.cost);
    formData.append("price", values.price);
    formData.append("observations", values.observations);
    await axiosInstance.patch(`/courses/update/${values.id}/`, formData, {
      headers: {
        "Content-Type": "application/json",
        Authorization: `JWT ${localStorage.getItem('access_token')}`,
        Accept: "application/json"
      },
    })
      .then(res => {
        dispatch({
          type: types.UPDATE_COURSE_SUCCESS,
          payload: res.data
        })
        if (res.status === 200 || res.status === 201) {
          notification("Taller editado", "Se editó el registro con éxito", 'success');
          dispatch(getCoursesByMyCulturalSpace())
        }
      })
  } catch (error) {
    dispatch({
      type: types.UPDATE_COURSE_FAILURE,
    })
    notification("Error", "Ha ocurrido un error. Intente nuevamente", 'error');

  }
}

export const deleteCourse = ({ taller }) => async (dispatch) => {
  try {
  const { id } = taller;
  await axiosInstance.delete(`/courses/delete/${id}/`, {
    headers: {
      "Content-Type": "application/json",
      Authorization: `JWT ${localStorage.getItem('access_token')}`,
      Accept: "application/json",
    }
  })
    .then(res => {
      dispatch({
        type: types.DELETE_COURSE_SUCCESS,
        payload: res.data.id,
      })
      if (res.status === 200) {
        notification("Taller eliminado", "Se eliminó el registro con éxito", 'success');
        dispatch(getCoursesByMyCulturalSpace())
      } 
    })

}
catch(error){
  dispatch({
    type: types.DELETE_COURSE_FAILURE
    })
  notification("Error", "Ha ocurrido un error. Intente nuevamente", 'error');
}}

export const getCoursesByMyCulturalSpace = () => async (dispatch) => {
  try{
  await axiosInstance.get('/courses/my-cultural-space/', {
    headers: {
      "Content-Type": "application/json",
      Authorization: `JWT ${localStorage.getItem('access_token')}`,
      Accept: "application/json",
    }
  })
    .then(res => {
      dispatch({
        type: types.GET_COURSES_BY_MY_CULTURAL_SPACE_SUCCESS,
        payload: res.data,
      })
    })
}
catch(error){
  dispatch({
    type: types.GET_COURSES_BY_MY_CULTURAL_SPACE_FAILURE
  })
}}


export const getCourses = () => async (dispatch) => {
  try{
    await axiosInstance.get('/courses/', {
    headers: {
      "Content-Type": "application/json",
      Authorization: `JWT ${localStorage.getItem('access_token')}`,
      Accept: "application/json",
    }
  })
    .then(res => {
      dispatch({
        type: types.GET_COURSES_SUCCESS,
        payload: res.data,
      })
    })
}catch(error){
  dispatch({
    type: types.GET_COURSES_FAILURE,
  })
}
}

export const getCoursesFormation = () => async (dispatch) => {
 try{
  await axiosInstance.get(`/courses/formation/`)
    .then(res => {
      dispatch({
        type: types.GET_COURSES_FORMATION_SUCCESS,
        payload: res.data,
      })
    }
    )
  }catch(error){
  dispatch({
    type: types.GET_COURSES_FORMATION_FAILURE,
})
}}

export const getCourseFormation = (id) => async (dispatch) => {
  try{
  await axiosInstance.get(`/courses/formation/${id}`)
    .then(res => {
      dispatch({
        type: types.GET_COURSE_FORMATION_SUCCESS,
        payload: res.data,
      })
    }
    )
}catch(error){
  dispatch({
    type: types.GET_COURSE_FORMATION_FAILURE,
})}}


export const getFormationbyCulturalSpace = (id) => async (dispatch) => {
  try {

    await axiosInstance.get(`/courses/formation/culturalspaces/${id}/`)
      .then(res => {
        dispatch({
          type: types.GET_COURSES_BY_CULTURAL_SPACE_SUCCESS,
          payload: res.data,
        })
      }
      )
  } catch (err) {
    dispatch({
      type: types.GET_COURSES_BY_CULTURAL_SPACE_FAILURE
    });
  }
};

