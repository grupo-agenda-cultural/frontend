import axiosInstance from '../services/axiosApi';
import { notification } from "../utils/alert";
import { types } from '../types/types';
import { loadUser } from './authentication';

export const addCulturalSpace = (values) => async (dispatch) => {
  try {
    const formData = new FormData();
    formData.append("image", values.image);
    formData.append("name", values.name);
    formData.append("email", values.email);
    formData.append("phone", values.phone);
    formData.append("address", values.address);
    formData.append("partido", values.partido);
    formData.append("localidad", values.localidad);
    formData.append("latitude", values.latitude);
    formData.append("longitude", values.longitude);
    formData.append("facebook", values.facebook);
    formData.append("instagram", values.instagram);

    await axiosInstance.post(`/culturalspaces/add/`, formData, {
      headers: {
        "Content-Type": "application/json",
        Authorization: `JWT ${localStorage.getItem('access_token')}`,
        Accept: "application/json",
      }
    }).then(res => {
      dispatch({
        type: types.ADD_CULTURAL_SPACE_SUCCESS,
        payload: res.data
      })
      if (res.status === 200 || res.status === 201) {
        notification("Espacio cultural agregado", "Se agregó el registro con éxito", 'success');
        dispatch(loadUser())
      }
    })
  } catch (error) {
    dispatch({
      type: types.ADD_CULTURAL_SPACE_FAILURE,
    })
    notification("Error", "Ha ocurrido un error. Intente nuevamente", 'error');
  }
}

export const updateCulturalSpace = (values) => async (dispatch) => {
  try {
    const formData = new FormData();
    if (typeof values.image === 'object')
      formData.append("image", values.image);
    formData.append("name", values.name);
    formData.append("email", values.email);
    formData.append("phone", values.phone);
    formData.append("address", values.address);
    formData.append("partido", values.partido);
    formData.append("localidad", values.localidad);
    formData.append("latitude", values.latitude);
    formData.append("longitude", values.longitude);
    formData.append("facebook", values.facebook);
    formData.append("instagram", values.instagram);

    await axiosInstance.patch(`/culturalspaces/update/${values.id}/`, formData, {

      headers: {
        "Content-Type": "application/json",
        Authorization: `JWT ${localStorage.getItem('access_token')}`,
        Accept: "application/json",
      }
    }).then(res => {
      dispatch({
        type: types.UPDATE_CULTURAL_SPACE_SUCCESS,
        payload: res.data
      })
      if (res.status === 200 || res.status === 201) {
        notification("Espacio cultural editado", "Se editó el espacio cultural con éxito", 'success');
        dispatch(loadUser())
      }
    })
  } catch (error) {
    dispatch({
      type: types.UPDATE_CULTURAL_SPACE_FAILURE,
    })

  }
}

export const getCulturalSpaces = () => async (dispatch) => {
  try {
    await axiosInstance.get(`/culturalspaces/`)
      .then(res => {
        dispatch({
          type: types.GET_CULTURAL_SPACES_SUCCESS,
          payload: res.data,
        })
      }
      )
  } catch (error) {
    dispatch({
      type: types.GET_CULTURAL_SPACES_FAILURE
    });
  }
}