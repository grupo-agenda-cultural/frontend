import axiosInstance from '../services/axiosApi'
import { notification } from "../utils/alert"
import { types } from '../types/types';


export const enableUser = ({ user }) => async (dispatch) => {
  try {
    const { id, name, last_name, email, password, phone, groups } = user;
    await axiosInstance.patch(`/users/confirm/${id}/`, {
      id: id,
      email: email,
      name: name,
      last_name: last_name,
      phone: phone,
      password: password,
      is_active: true,
      groups: groups
    }, {
      headers: {
        "Content-Type": "application/json",
        Authorization: `JWT ${localStorage.getItem('access_token')}`,
        Accept: "application/json",
      }
    })
      .then(res => {
        dispatch({
          type: types.ENABLE_USER_SUCCESS,
          payload: res.data
        })
        if (res.status === 200) {
          notification("Cuenta habilitada", "Se habilitó la cuenta del coordinador con éxito", 'success');
          dispatch(getCoordinadores())
        }
      })
  } catch (error) {
    dispatch({
      type: types.ENABLE_USER_FAILURE,
    })
    notification("Error", "Ha ocurrido un error. Intente nuevamente", 'error');

  }
}
export const disableUser = ({ user }) => async (dispatch) => {
  //consultar la api y enviar un método put para actualizar el user
  try {
    const { id, name, last_name, email, phone, groups } = user;
    await axiosInstance.patch(`/users/update/${user.id}/`, {
      id: id,
      email: email,
      name: name,
      last_name: last_name,
      phone: phone,
      // password: password,
      is_active: false,
      groups: groups
    },
      {
        headers: {
          "Content-Type": "application/json",
          Authorization: `JWT ${localStorage.getItem('access_token')}`,
          Accept: "application/json",
        }
      })
      .then(res => {
        dispatch({
          type: types.DISABLE_USER_SUCCESS,
          payload: res.data
        })
        if (res.status === 200) {
          notification("Cuenta deshabilitada", "Se deshabilitó la cuenta del coordinador con éxito", 'warning');
          dispatch(getCoordinadores())
        }
      })
  } catch (error) {
    dispatch({
      type: types.DISABLE_USER_SUCCESS
    })
    notification("Error", "Ha ocurrido un error. Intente nuevamente", 'error');
  }
}

export const updateUser = (values, coordinador) => async (dispatch) => {
  console.log(values)
  try {
    const response = await axiosInstance.patch(`/users/update/${values.id}/`, {
      id: values.id,
      email: values.email,
      name: values.name,
      last_name: values.last_name,
      phone: values.phone,
      password: coordinador.password,
      is_active: coordinador.is_active,
      groups: coordinador.groups
    },
      {
        headers: {
          "Content-Type": "application/json",
          Authorization: `JWT ${localStorage.getItem('access_token')}`,
          Accept: "application/json",
        }
      })

    if (response.status === 200) {
      dispatch({
        type: types.UPDATE_USER_SUCCESS,
        payload: response.data
      })
      notification("Cuenta editada", "Se editó la cuenta del coordinador con éxito", 'success');
      dispatch(getCoordinadores())
    }

  } catch (error) {
    dispatch({
      type: types.UPDATE_USER_FAILURE
    })
    notification("Error", "Ha ocurrido un error. Intente nuevamente", 'error');

  }
}

export const getCoordinadores = () => async (dispatch) => {
  try {
    const response = await axiosInstance.get('/users/coordinadores/', {
      headers: {
        "Content-Type": "application/json",
        Authorization: `JWT ${localStorage.getItem('access_token')}`,
        Accept: "application/json",
      }
    })
    if (response.status === 200) {
      dispatch({
        type: types.GET_COORDINADORES_SUCCESS,
        payload: response.data,
      })
    }
  } catch (error) {
    dispatch({
      type: types.GET_COORDINADORES_FAILURE
    })
  }
}

export const updateCulturalSpaceUser = (id, usuario, data) => async (dispatch) => {
  // const {id, name, last_name, email, password, phone, is_active, groups} = usuario;
  try{
    axiosInstance.put(`/users/coordinadores/update/${id}/`, {
    id: id,
    user: usuario,
    cultural_space: data
  }).then(res => {
    dispatch({
      type: types.UPDATE_COORDINADOR_CULTURAL_SPACE_SUCCESS,
      payload: res.data
    })
  });

} catch (error){
  dispatch({
    type: types.UPDATE_COORDINADOR_CULTURAL_SPACE_FAILURE,
});
}}