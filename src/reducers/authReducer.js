import { types } from '../types/types'

const initialState = {
  isAuthenticated: null,
  user: null,
  coordinador: null
};

export const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.AUTHENTICATED_SUCCESS:
        return {
            ...state,
            isAuthenticated: false
        }
    case types.LOGIN_SUCCESS:
      return {
        ...state,
        isAuthenticated: true
      }
    case types.LOAD_USER_SUCCESS:
      return {
        ...state,
        isAuthenticated: true,
        user: action.payload.user,
        coordinador: action.payload,
        loading: false,
      };
    case types.LOGIN_FAILURE:
    case types.LOAD_USER_FAILURE:
      localStorage.removeItem('access_token');
      localStorage.removeItem('refresh_token');
      return {
        ...state,
        user: null,
        loading: false,
        isAuthenticated: null,
      };
    // case SIGNUP_SUCCESS:
    //     return {
    //         ...state,
    //         isAuthenticated: false
    //     }
    // case AUTHENTICATED_FAILURE:
    //     return {
    //         ...state,
    //         isAuthenticated: false
    //     }
    case types.LOGOUT_SUCCESS:
      localStorage.removeItem('access_token');
      localStorage.removeItem('refresh_token');
      return {
        ...state,
        isAuthenticated: false,
        user: null,
        coordinador: null
      }    
      case types.AUTHENTICATED_FAILURE:
        return {
          ...state,
          isAuthenticated:false
      }
        case types.PASSWORD_RESET_SUCCESS:
        case types.PASSWORD_RESET_FAILURE:
        case types.PASSWORD_RESET_CONFIRM_SUCCESS:
        case types.PASSWORD_RESET_CONFIRM_FAILURE:
        case types.LOGOUT_FAILURE:
            return {
                ...state
            }
        
    default:
      return state
  }
};