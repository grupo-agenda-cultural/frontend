import { types } from '../types/types'

const initialState = {
    isLoading: true,
    courses: [],
    course: {},
    cultural_space: {}
}

export const courseReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.ADD_COURSE_SUCCESS:
            return {
                ...state,
                course: [...state.courses, action.payload]
            }
        case types.UPDATE_COURSE_SUCCESS:
            return {
                ...state,
                courses: state.courses.map(course =>
                    course.id === action.payload ? (course = action.payload) : course)
            }
        case types.DELETE_COURSE_SUCCESS:
            return {
                ...state,
                courses: state.courses.filter(course => course.id !== action.payload)
            }
        case types.GET_COURSES_BY_MY_CULTURAL_SPACE_SUCCESS:
            return {
                ...state,
                courses: action.payload,
            }
        case types.GET_COURSES_SUCCESS:
            return {
                ...state,
                courses: action.payload,
            }
        case types.GET_COURSES_FORMATION_SUCCESS:
            return {
                ...state,
                cultural_space: action.payload,
                courses: action.payload,
            }
        case types.GET_COURSE_FORMATION_SUCCESS:
            return {
                ...state,
                course: action.payload,
                cultural_space: action.payload
            }
        case types.GET_COURSES_BY_CULTURAL_SPACE_SUCCESS:
            return {
                ...state,
                cultural_space: action.payload,
                courses: action.payload,
                isLoading: false
            }
        case types.ADD_COURSE_FAILURE:
        case types.UPDATE_COURSE_FAILURE:
        case types.DELETE_COURSE_FAILURE:
        case types.GET_COURSES_FAILURE:
        case types.GET_COURSES_BY_MY_CULTURAL_SPACE_FAILURE:
        case types.GET_COURSES_BY_CULTURAL_SPACE_FAILURE:
        case types.GET_COURSES_FORMATION_FAILURE:
        case types.GET_COURSE_FORMATION_FAILURE:

            return {
                ...state
            }
        default:
            return state
    }
}

