import { types } from '../types/types'

const initialState = {
    subscriptions: [],
    subscription: {}
}

export const subscriptionReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.ADD_SUBSCRIPTION_SUCCESS:
            return {
                ...state,
                subscription: [...state.subscriptions, action.payload]
            }
            case types.ADD_SUBSCRIPTION_CALENDAR_SUCCESS:
                return {
                    ...state,
                    subscription: [...state.subscriptions, action.payload]
                }
        case types.ADD_SUBSCRIPTION_FAILURE:
        case types.ADD_SUBSCRIPTION_CALENDAR_FAILURE:

            return {
                ...state
            }

        default:
            return state
    }
}

