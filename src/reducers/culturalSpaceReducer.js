import { types } from '../types/types'

const initialState = {
  cultural_spaces: [],
  cultural_space: {},
}

export const culturalSpaceReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.ADD_CULTURAL_SPACE_SUCCESS:
      return {
        ...state,
        cultural_space: [...state.cultural_spaces, action.payload]
      }

    case types.UPDATE_CULTURAL_SPACE_SUCCESS:
      return {
        ...state,
        cultural_spaces: state.cultural_spaces.map(cultural_space =>
          cultural_space.id === action.payload ? (cultural_space = action.payload) : cultural_space)
      }
    case types.GET_CULTURAL_SPACES_SUCCESS:
      return {
        ...state,
        cultural_spaces: action.payload
      }
    case types.ADD_CULTURAL_SPACE_FAILURE:
    case types.UPDATE_CULTURAL_SPACE_FAILURE:
    case types.GET_CULTURAL_SPACES_FAILURE:
      return {
        ...state
      }
    default:
      return state
  }
}

