import { types } from '../types/types'

const initialState = {
    users: [],
    coordinadores: [],
    coordinador: {},
    user: {}
}

export const userReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.GET_COORDINADORES_SUCCESS:
            return {
                ...state,
                coordinadores: action.payload
            }
        case types.ENABLE_USER_SUCCESS:
            return {
                ...state,
                users: state.users.map(user =>
                    user.id === action.payload ? (user = action.payload) : user)
            }
        case types.DISABLE_USER_SUCCESS:
            return {
                ...state,
                users: state.users.map(user =>
                    user.id === action.payload ? (user = action.payload) : user)
            }
        case types.UPDATE_USER_SUCCESS:
            return {
                ...state,
                users: state.users.map(user =>
                    user.id === action.payload ? (user = action.payload) : user)
            }
        case types.UPDATE_COORDINADOR_CULTURAL_SPACE_SUCCESS:
            return {
                ...state,
                coordinadores: state.coordinadores.map(coordinador =>
                    coordinador.id === action.payload ? (coordinador = action.payload) : coordinador)
            }
        case types.UPDATE_USER_FAILURE:
        case types.ENABLE_USER_FAILURE:
        case types.DISABLE_USER_FAILURE:
        case types.UPDATE_COORDINADOR_CULTURAL_SPACE_FAILURE:
        case types.GET_COORDINADORES_FAILURE:
            return {
                ...state
            }
        default:
            return state
    }
}