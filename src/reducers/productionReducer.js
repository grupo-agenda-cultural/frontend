import { types } from '../types/types'

const initialState = {
  isLoading: true,
  productions: [],
  production: {},
  cultural_space: {}
}

export const productionReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.ADD_PRODUCTION_SUCCESS:
      return {
        ...state,
        production: [...state.productions, action.payload]
      }
    case types.UPDATE_PRODUCTION_SUCCESS:
      return {
        ...state,
        productions: state.productions.map(production =>
          production.id === action.payload ? (production = action.payload) : production)
      }

    case types.DELETE_PRODUCTION_SUCCESS:
      return {
        ...state,
        productions: state.productions.filter(production => production.id !== action.payload)
      }
    case types.GET_PRODUCTIONS_BY_MY_CULTURAL_SPACE_SUCCESS:
      return {
        ...state,
        productions: action.payload
      }
    case types.GET_PRODUCTIONS_SUCCESS:
      return {
        ...state,
        productions: action.payload
      }
    case types.GET_PRODUCTIONS_CALENDAR_SUCCESS:
      return {
        ...state,
        cultural_space: action.payload,
        productions: action.payload,
      }
    case types.GET_PRODUCTION_CALENDAR_SUCCESS:
      return {
        ...state,
        cultural_space: action.payload,
        production: action.payload,
      }
    case types.GET_PRODUCTIONS_BY_CULTURAL_SPACE_SUCCESS:
      return {
        ...state,
        cultural_space: action.payload,
        productions: action.payload,
        isLoading: false
      }
    case types.ADD_PRODUCTION_FAILURE:
    case types.UPDATE_PRODUCTION_FAILURE:
    case types.DELETE_PRODUCTION_FAILURE:
    case types.GET_PRODUCTIONS_FAILURE:
    case types.GET_PRODUCTIONS_BY_MY_CULTURAL_SPACE_FAILURE:
    case types.GET_PRODUCTIONS_BY_CULTURAL_SPACE_FAILURE:
    case types.GET_PRODUCTIONS_CALENDAR_FAILURE:
    case types.GET_PRODUCTION_CALENDAR_FAILURE:
      return {
        ...state
      }
    default:
      return state
  }
}

