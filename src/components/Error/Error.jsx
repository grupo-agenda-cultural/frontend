import React from 'react';
import { useNavigate } from "react-router-dom";
import './Error.css';
import error from '../../assets/images/error/error.jpg';

const Error = ({path}) => {
    console.log(path)
    const history = useNavigate();

    return (

        <div className="container-errores min-vh-100">
             <div className="container-msg">
                <img className="img-error" src={error} alt="error-img" />
                <div className="msg">
                    <h3 className="msg-error">Debe agregar su espacio cultural</h3>
                    <p>Tiene que completar los datos de su espacio cultural</p>
                    <div className="button">
                        <input type="button" className="btn btn-primary" value="Agregar espacio cultural" onClick={() => history(path)} />
                    </div>
                </div>
            </div> 
        </div>
    );
}

export default Error;