import React from 'react';
import { useState } from 'react';
import logo from '../../assets/images/logo/logo.png';
import { Formik, Form } from "formik";
import * as Yup from 'yup'
import '../Login/Login.css';
import { useDispatch } from 'react-redux';
import { register } from '../../actions/authentication';
import { FormControl, FormGroup, InputGroup } from 'react-bootstrap';

const SignUpForm = ({ slide }) => {

  const [shown, setShown] = useState(false);
  const switchShown = () => setShown(!shown);
  const dispatch = useDispatch();
  
  const ErrorMessagesSchema = Yup.object().shape({
    name: Yup.string().required('Campo requerido').matches(/^[a-zA-Z]+$/, {
      message: "Nombre inválido",
      excludeEmptyString: false}),
    lastname: Yup.string().required('Campo requerido').matches(/^[a-zA-Z]+$/, {
      message: "Apellido inválido",
      excludeEmptyString: false}),
    phone: Yup.string()
    .matches(/^(?:(?:00)?549?)?0?(?:11|[2368]\d)(?:(?=\d{0,2}15)\d{2})??\d{8}$/, {
      message: "Teléfono inválido",
      excludeEmptyString: false}).required('Campo requerido'),
    email: Yup.string().email('Email inválido').required('Campo requerido'),
    password: Yup.string().required("Campo requerido").min(8, "Debe ingresar una contraseña con un mínimo de 8 caracteres")
  });

  const handleSubmit = (values) => {
    dispatch(register(values));
  }

  return (
    <div className="form-container sign-up-container ">
      <div className="logo">
        <img src={logo} alt="logo" />
      </div>
      <Formik
        initialValues={{ name:'', lastname:'', phone: '', email: '', password: '' }}
          validationSchema={ErrorMessagesSchema}
          onSubmit={(values, { resetForm }) => {
            handleSubmit(values);
            resetForm();
            setTimeout(()=> slide(""), 5000)
          }}>
      {({ errors, touched, handleChange, handleBlur }) => (

      <Form className="formulario">
        <div className="heading">
          <h2>Crear cuenta</h2>
          <h6>¿Ya tenés una cuenta?</h6>
          <a href="#/" className="toggle"
            id="signIn"
            onClick={() => slide("")}
          > Iniciá sesión</a>
        </div>
        <div className="row">
          <div className="col-md-6 form-group p-2">
            <label htmlFor="exampleInputEmail1">Nombre</label>
            <FormGroup>
            <FormControl
              type="text"
              name="name"
              className="form-control"
              onChange={handleChange}
              onBlur={handleBlur}
              placeholder="Ingresá tu nombre"
              isValid={touched.name && !errors.name}
              isInvalid={touched.name && errors.name}/>
              <FormControl.Feedback type="invalid">
                      {errors.name}
                    </FormControl.Feedback>
              </FormGroup>
          </div>
          <div className="col-md-6 form-group p-2">
            <label htmlFor="exampleInputEmail1">Apellido</label>
            <FormGroup>

            <FormControl
              type="text"
              name="lastname"
              className="form-control"
              id="input-name"
              aria-describedby="lastname"
              placeholder="Ingresá tu apellido"
              onChange={handleChange}
              onBlur={handleBlur}
              isInvalid={touched.lastname && errors.lastname}
              isValid={touched.lastname && !errors.lastname} />
                      <FormControl.Feedback type="invalid">
                      {errors.lastname}
                    </FormControl.Feedback>
              </FormGroup>
          </div>
        </div>
        <div className="row">
          <div className="col-md-6 form-group  p-2">
            <label htmlFor="exampleInputEmail1">Teléfono</label>
            <FormGroup>
            <FormControl
              type="text"
              name="phone"
              className="form-control"
              id="input-name"
              aria-describedby="phone"
              placeholder="Ingresá tu teléfono"
              onChange={handleChange}
              onBlur={handleBlur}
              isInvalid={touched.phone && errors.phone}
              isValid={touched.phone && !errors.phone}
               />
<FormControl.Feedback type="invalid">
                      {errors.phone}
                    </FormControl.Feedback>
              </FormGroup>          </div>
          <div className="col-md-6 form-group p-2">
            <label htmlFor="exampleInputEmail1">Email</label>
            <FormGroup>
            <FormControl
              type="email"
              name="email"
              className="form-control"
              placeholder="Ingresá tu email" 
              onChange={handleChange}
              onBlur={handleBlur}
              isInvalid={touched.email && errors.email}
              isValid={touched.email && !errors.email}
              />
<FormControl.Feedback type="invalid">
                      {errors.email}
                    </FormControl.Feedback>
              </FormGroup>          
                      </div>
        </div>
        <div className="row">
          <div className="col-md-12 form-group p-2">
            <label htmlFor="exampleInputEmail1">Contraseña</label>
            <FormGroup >       
          <InputGroup>
                    <FormControl
                  type={shown ? 'text' : 'password'}
                  name="password"
                  className="form-control" 
                  placeholder="Ingresá tu contraseña" 
                  onChange={handleChange}
                  onBlur={handleBlur}
                  isInvalid={touched.password && errors.password}
/>
<span className="input-group-text" onClick={switchShown} id="password"> {shown ? <i className="bi bi-eye-slash-fill"></i> : <i className="bi bi-eye-fill"></i>}</span>

              <FormControl.Feedback type="invalid">
                      {errors.password}
                    </FormControl.Feedback>
                    </InputGroup>
                    
              </FormGroup>  

          
          </div>

        </div>
        
        <div className="row">
          <div className="col pt-3">
            <input type="submit" value="Crear cuenta" className="sign-btn" />
          </div>
        </div>
        <div className="text">
          <p className="copyright">&copy;2022 Agenda y Comunicación Cultural y Artística</p>
        </div>
      </Form>
      )}
    </Formik>
    </div>
  );
}

export default SignUpForm;