import React, { useEffect } from 'react';
import { useParams } from "react-router-dom";
import { useDispatch, useSelector } from 'react-redux';
import './Activity.css';
import { MapContainer, TileLayer, Marker, Popup } from 'react-leaflet'
import markerIconPng from "leaflet/dist/images/marker-icon.png"
import { Icon } from 'leaflet';
import "leaflet/dist/leaflet.css";
import { categories, languages, costs, modalities } from '../data';
import { localidades, partidos } from '../../CulturalSpace/data';
import moment from 'moment';
import 'moment/locale/es';
import {  getCourseFormation } from '../../../actions/courseAction';
import CONFIG from '../../../configuration/configuration';
import Error404 from '../../../routers/Error404';
const Activity = () => {

    const dispatch = useDispatch();
    const { courseId } = useParams();
    
    const courses = useSelector(state => state.courses.courses);
    const card = courses.find(elem => elem.course.id.toString()  === courseId.toString())
    useEffect(() => {
        dispatch(getCourseFormation(courseId))
        // eslint-disable-next-line  react-hooks/exhaustive-deps
    }, []);

    useEffect(() => {
        window.scrollTo({ top: 0, behavior: 'smooth' })
      }, [])
      
      console.log(card)


    return ( 
        
        <>
        {card !== undefined ?
        <>
        <div className="language-production">
                <h5>{languages.map(option => (option.value === card.course.language ? option.label : ""))}</h5>
                            </div>
        <div className="title-production-event">
                    <h1 className="title-production">{card.course.name}</h1>
                    </div>
            <div className="container-center">
                <div className="container-production">
                                    <img src={CONFIG.BASE_URL + card.course.image} style={{ width: "40em", maxWidth:"100%"}}  alt="produccion" />

                </div>
                        <div className="production-ticket">
                            <div className="ticket-header">
                                 <div className="title">
                                    Costo
                                </div>
                                <div className="price">
                                 {card.course.price !== 0.00 ? `$ ${card.course.price}` : costs.map(option => (
                                option.value === card.course.cost ? option.label : ''))}
                                </div> 
                            </div>
                               {/* <div className="ticket-body">
                                <p className="date"> Inscripción: {moment(card.course.inscription_start).local().format('DD/MM/YYYY')} a {moment(card.course.inscription_end).local().format('DD/MM/YYYY')}</p>
                            </div>   */}
  
                             <div className="button">
                                  <a className="float-button" target="_blank" rel="noreferrer" href={`https://wa.me/54${card.coordinador.phone}?text=Hola ${card.coordinador.name}!%0AMe%20podrías%20pasar%20más%20información%20sobre%20el%20taller%20*${card.course.name}*.%0AMi%20nombre%20es%20`}>
                                    <i className="bi bi-whatsapp"></i>
                                    <span className="text-whatsapp">Consultar sobre el taller</span>
                                </a>
                            </div> 
                            </div>
                           
                        </div>
                        <div className="body-event">
                        <div className="course-description">
                          <p className="organizer">Docente: {card.course.teacher}</p>
                         <p className="description" style={{whiteSpace: "pre-line"}}> {card.course.description}</p>
                         <strong>Contenido</strong> <br/>
                         <p className="description" style={{padding:0, whiteSpace: "pre-line"}}> {card.course.content}</p>
                         {card.course.requirements ?
                         <>
                         <strong>Requisitos</strong> <br/>
                         <p className="description" style={{padding:0}}> {card.course.requirements}</p>
</>
:''}
                          <p className="dates"><strong>Fecha de inscripción:</strong> {moment(card.course.inscription_start).local().format('DD/MM/YYYY')} a {moment(card.course.inscription_end).local().format('DD/MM/YYYY')} </p> 

                          <p className="modality"><strong>Modalidad:</strong> {modalities.map(option => option.value === card.course.modality ? option.label : "")}</p>
                          <p className="modality"><strong>Categoría:</strong> {categories.map(option => option.value === card.course.category ? option.label : "")} </p>
                          <p className="modality"><strong>Espacio cultural:</strong> {card.cultural_space.name}</p>
                         {
                             card.course.observations ?
                         <p className="modality"><strong>Observaciones</strong><br/> {card.course.observations}</p>
:''}
                        </div>


                            </div>
                            <div className="container-map">
                                <MapContainer center={[card.cultural_space.latitude, card.cultural_space.longitude]} zoom={15} scrollWheelZoom={false} style={{ height: '80vh', width: '100vw' }}>
          <TileLayer
            attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"/>
             <Marker position={[card.cultural_space.latitude, card.cultural_space.longitude]} icon={new Icon({ iconUrl: markerIconPng, iconSize: [25, 41], iconAnchor: [12, 41] })}>
             <Popup maxWidth="250" maxHeight="auto">
            <img height="150px" width="100%" src={CONFIG.BASE_URL + card.cultural_space.image} alt={card.cultural_space.image} />
            <div className="container text-center">
            <h5 className="py-2 mb-1 text-primary">{card.cultural_space.name}</h5>
            Dirección: {card.cultural_space.address}<br/>
            Partido: {partidos.map(option => option.value === card.cultural_space.partido ? option.label : '')}<br/>
            Localidad: {localidades.map(option => option.value === card.cultural_space.localidad ? option.label : '')}<br/>
            Teléfono: {card.cultural_space.phone}<br/>
            Email: {card.cultural_space.email}<br/>
            {card.cultural_space.instagram ? <a href={card.cultural_space.instagram} target="_blank" rel="noreferrer"><i className="bi bi-instagram" style={{fontSize:"24px",color:" #f54056",cursor:"pointer"}}></i></a> : '' } 
            &nbsp;
            {card.cultural_space.facebook ? <a href={card.cultural_space.facebook} target="_blank" rel="noreferrer"><i className="bi bi-facebook" style={{fontSize:"24px",color:"#3f729b",cursor:"pointer"}}></i></a> : ''}

             <br/>
            <a href={`https://maps.google.com/?q=${card.cultural_space.latitude},${card.cultural_space.longitude}`} target="_blank" rel="noreferrer"><button type="button" className="btn btn-primary" >¿Cómo llegar?</button></a>
            </div>    
            </Popup>
        </Marker>
        </MapContainer>
                            </div> 
                            </>
 : <Error404/>}
            </>
            )
}

            export default Activity;