export const categories = [
    { id: 1, value: 1, label: 'Adultos' },
    { id: 2, value: 2, label: 'Niños' },
    { id: 3, value: 3, label: 'Público en general' }
]

export const modalities = [
    { id: 1, value: 1, label: 'Presencial' },
    { id: 2, value: 2, label: 'Virtual' }
]

export const languages = [
    { id: 1, value: 1, label: 'Audiovisuales' },
    { id: 2, value: 2, label: 'Artes Visuales' },
    { id: 3, value: 3, label: 'Artesanías' },
    { id: 4, value: 4, label: 'Comunicación' },
    { id: 5, value: 5, label: 'Danza' },
    { id: 6, value: 6, label: 'Letras' },
    { id: 7, value: 7, label: 'Música' },
    { id: 8, value: 8, label: 'Teatro' }
]

export const costs = [
    { id: 1, value: 1, label: 'A la gorra' },
    { id: 2, value: 2, label: 'Con precio' },
    { id: 3, value: 3, label: 'Gratuito' },
]

export const formations = [
    { id: 1, value: 7, label: 'Congreso' }  ,
    { id: 2, value: 2, label: 'Curso' },
    { id: 3, value: 1, label: 'Diplomatura' },
    { id: 4, value: 6, label: 'Encuentro' },
    { id: 5, value: 5, label: 'Jornada' },
    { id: 6, value: 3, label: 'Seminario' },
    { id: 7, value: 4, label: 'Workshop' },
]

export const durations = [
    { id: 1, value: 1, label: 'Anual' },
    { id: 2, value: 2, label: 'Semestral' },
    { id: 3, value: 3, label: 'Mensual' }
]