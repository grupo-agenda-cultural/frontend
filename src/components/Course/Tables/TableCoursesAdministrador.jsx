import React, { useState, useEffect, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import ModalView from '../Modals/ModalView';
import ModalDelete from '../Modals/ModalDelete';
import { Button } from '../../Button/Button';
import DataTable from 'react-data-table-component';
import DataTableExtensions from 'react-data-table-component-extensions';
import 'react-data-table-component-extensions/dist/index.css';
import { formations, costs } from '../data';
import Spinner from '../../Spinner/Spinner';
import { getCourses } from '../../../actions/courseAction';
import ModalUpdate from '../Modals/ModalUpdate';

const TableCoursesAdministrador = () => {
   const dispatch = useDispatch();
   const courses = useSelector(state => state.courses.courses);

    const [tallerChoose, setTallerChoose] = useState('');
    const [pending, setPending] = useState(true);
    const [showModalView, setShowModalView] = useState(false);
    const [showModalUpdate, setShowModalUpdate] = useState(false);
    const [showModalDelete, setShowModalDelete] = useState(false);

  useEffect(() => {
    const loadCourses = () => dispatch(getCourses())
    loadCourses();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    const timeout = setTimeout(() => {
      const loadCourses = () => {
        dispatch(getCourses());
      }; 
      loadCourses();
      setPending(false);
    }, 1800);
    return () => clearTimeout(timeout);
    // eslint-disable-next-line
  }, [courses]);

    
        const setRowCourseView = (taller) => {
            setTallerChoose(taller);
            setShowModalView(true);

          }

          const setRowCourseUpdate= (taller) => {
            setTallerChoose(taller);
            setShowModalUpdate(true);
          }
          const setRowCourseDelete= (taller) => {
            setTallerChoose(taller);
            setShowModalDelete(true);

          }
          const CustomLoader = () => (
            <div className="loading">  
              <Spinner />
              <h5 className="text-center" style={{color:"rgb(26,53,64,25);"}}>Cargando...</h5>
            </div>  
          );
        
        

    const columns = useMemo(
        () => [
          {
            name: "Nombre",
            selector: row => row.name,
            sortable: true,
            wrap: true
          },
          {
            name: "Día/s",
            selector: row => row.date,
            sortable: true,
            wrap: true,
          },
          {
            name: "Horario",
            selector: row => row.hour,
            sortable: true,
            wrap: true
          },
          {
            name: "Docente",
            selector: row => row.teacher,
            sortable: true,
            wrap: true,
          },
          {
            name: "Espacio cultural",
            selector: row => row.cultural_space.name,
            sortable: true,
            wrap: true,
            width: '11em',
          },
          {
            name: "Formación",   
            selector: row => formations.map(option => (option.value === row.formation ? option.label : "")),
            sortable: true,
            wrap: true
          },
          {
            name: "Costo",
            selector: row => costs.map(option => (option.value === row.cost? option.label : "")),
            sortable: true,
            wrap: true
          },
          {
            name: "Precio",
            selector: row =>  `$${row.price}`,
            sortable: true,
            wrap: true
          },
          {
            cell: (row) => <Button onClick={() => setRowCourseView(row)} type="button" className="btn btn-secondary" icon="bi bi-eye-fill"></Button>,
            ignoreRowClick: true,
            allowOverflow: true,
            button: true,
            width: "50px"
          },
          {
            cell: (row) => <Button onClick={() => setRowCourseUpdate(row)} type="button" className="btn btn-primary" icon="bi bi-pencil-square"></Button>,
            ignoreRowClick: true,
            allowOverflow: true,
            button: true,
            width: "50px"
          },
          {
            cell: (row) => <Button onClick={() => setRowCourseDelete(row)} type="button" className="btn btn-danger" icon="bi bi-trash-fill"></Button>,
            ignoreRowClick: true,
            allowOverflow: true,
            button: true,
            width: "50px"
          }
        ],
        [],
      );
    
      const paginationComponentOptions = {
        rowsPerPageText: 'Filas por página',
        rangeSeparatorText: 'de',
        selectAllRowsItem: true,
        selectAllRowsItemText: 'Todos',
      };
    
      const tableData = {
        columns,
        data: courses
      };
      const customTheme = {
        rows: {
          style: {
              fontSize:'14px',
              padding:'10px 0',
              justifyContent:'center'
          },
      },
      headCells: {
          style: {
              justifyContent: 'left',
              fontSize:'14px',
          },
      },
      cells: {
        style: {
            justifyContent: 'left'
        },
    },
    };
    return(
        <>
        <div className="container-fluid bg-light p-2 min-vh-100">
          <div className="card shadow card-body m-2">
            <div className="row justify-content-center">
            <DataTableExtensions filterPlaceholder="Buscar cursos, diplomaturas, ..." export={false} print={false}
            {...tableData}
            >
              <DataTable
                noHeader
                title="Tabla de talleres"
                columns={columns}
                data={courses}
                customStyles={customTheme}
                center
                defaultSortField="id"
                defaultSortDesc={true}
                pagination paginationComponentOptions={paginationComponentOptions}
                progressPending={pending}
                progressComponent={<CustomLoader />}
                noDataComponent="No se encuentran registros en la base de datos"
                highlightOnHover
              />
              </DataTableExtensions>
            </div>
          </div>
        </div>
        <ModalView taller={tallerChoose} show={showModalView} onHide={()=> setShowModalView(false)}/>
        <ModalUpdate taller={tallerChoose} show={showModalUpdate} onHide={()=> setShowModalUpdate(false)} setOnHide={setShowModalUpdate} />
        <ModalDelete taller={tallerChoose} show={showModalDelete} onHide={()=> setShowModalDelete(false)} setOnHide={setShowModalDelete} />
        </>
    );
}

export default TableCoursesAdministrador;