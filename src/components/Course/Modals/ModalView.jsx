import React from 'react';
import { costs, categories, durations, formations, languages, modalities } from '../data';
import moment from 'moment';
import CONFIG from '../../../configuration/configuration';
import { Modal, Container, Row, Col, Button } from 'react-bootstrap';

const ModalView = ({ taller, show, onHide }) => {

  return (
    <Modal
      show={show}
      size='lg'
      onHide={onHide}
      backdrop="static"
      keyboard={false}
      aria-labelledby="modalViewCulturalSpace"
      centered
      scrollable
    >
      <Modal.Header closeButton style={{ background: 'gray', padding: "20px" }}>
        <Modal.Title style={{ color: 'white' }}>
          Visualizar Taller
        </Modal.Title>
      </Modal.Header>
      <Modal.Body >
        <Container fluid>
          <Row className="p-2">
            <Col md={6}>
              <h5>Taller</h5>                       
               <p>{taller.name}</p>
              <h5>Docente </h5>
              <p>{taller.teacher}</p>
              <h5>Día/s y horarios</h5>
              <p>{taller.date} - {taller.hour}</p>
            </Col>
            <Col md={6}>
              <Container fluid>
                <img width="100%" height="240px" alt={taller.name} src={CONFIG.BASE_URL + taller.image} />
              </Container>
            </Col>
            <Col md={12}>
              <h5>Descripción</h5>
              <p className="text-justify" style={{ whiteSpace: "pre-line" }}>{taller.description}</p>
              <h5>Contenido</h5>
              <p className="text-justify" style={{ whiteSpace: "pre-line" }}>{taller.content}</p>

            </Col>
          </Row>
          <Row className="p-2">
            <Col md={4} >
              <h5>Modalidad</h5>
              <p>{modalities.map(modalidad => (modalidad.value === taller.modality ? modalidad.label : ""))}</p>
            </Col>
            <Col md={4}>
              <h5>Destinatarios</h5>
              <p>{categories.map(destinatario => (destinatario.value === taller.category ? destinatario.label : ""))}</p>

            </Col>
            <Col md={4} >
              <h5>Duración</h5>
              <p>{durations.map(duracion => (duracion.value === taller.duration ? duracion.label : ""))}</p>
            </Col>
          </Row>
          <Row className="p-2">
            <Col md={4}>
              <h5>Formación</h5>
              <p>{formations.map(formacion => (formacion.value === taller.formation ? formacion.label : ""))}</p>
            </Col>
            <Col md={4}>
              <h5>Lenguaje artístico</h5>
              <p>{languages.map(lenguaje => (lenguaje.value === taller.language ? lenguaje.label : ""))}</p>
            </Col>

          </Row>
          <Row className="p-2">
            <Col md={4}>
              <h5>Costo</h5>

              {taller.price !== 0.00 ? `$ ${taller.price}` : costs.map(option => (
                option.value === taller.cost ? option.label : ''))}
            </Col>
          </Row>
          <Row className="p-2">
            <Col md={6}>
              <h5>Período de inscripción</h5>
              <p>{moment(taller.inscription_start).local().format('DD/MM/YYYY')} a {moment(taller.inscription_end).local().format('DD/MM/YYYY')}</p>
            </Col>
          </Row>
          {taller.requirements ?
            <Row className="p-2">
              <Col md={12}>
                <h5>Requisitos</h5>
                <p>{taller.requirements}</p>
              </Col>
            </Row> : ''}
          {taller.observations ?
            <Row className="p-2">
              <Col md={12}>
                <h5>Observaciones</h5>

                <p>{taller.observations}</p>
              </Col>
            </Row> : ''}
        </Container>
      </Modal.Body>
      <Modal.Footer>
        <Button type="button" variant="secondary" onClick={onHide}>Cerrar</Button>
      </Modal.Footer>
    </Modal>
  );
}

export default ModalView;
