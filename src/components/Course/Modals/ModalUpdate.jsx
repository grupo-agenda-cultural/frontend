import React from 'react';
import { useDispatch } from 'react-redux';
import { costs, categories, durations, formations, languages, modalities } from '../data';
import 'react-datepicker/dist/react-datepicker.css';
import { Formik, Field, ErrorMessage } from "formik";
import * as Yup from 'yup'
import DatePicker from "react-datepicker";
import 'react-datepicker/dist/react-datepicker.css';
import moment from 'moment';
import { updateCourse } from '../../../actions/courseAction';
import { Modal, Form, Row, Col, Container, Button, InputGroup } from 'react-bootstrap';
import CONFIG from '../../../configuration/configuration';

const ModalUpdate= ({taller, show, onHide, setOnHide}) => {

  const dispatch = useDispatch();
  
  const format = "dd/MM/yyyy";
 
  const formatDate = (date) => {
    const check = moment(date).format("DD/MM/YYYY");
    return check;
  }

  const handleSubmit = (values) => {
    dispatch(updateCourse(values, formatDate(values.inscription_start), formatDate(values.incription_end)));
  setOnHide(false);
  }

  // Validaciones
  const ErrorMessagesSchema = Yup.object().shape({
    image: Yup.mixed().required("Debe subir la imagen del taller") , 
    name: Yup.string().required('Campo requerido'),
    teacher: Yup.string().required('Campo requerido'),
    description: Yup.string().required('Campo requerido'),
    date: Yup.string().required('Campo requerido'),
    hour: Yup.string().required('Campo requerido'),
    content: Yup.string().required('Campo requerido'),
    language: Yup.string().required("Debe seleccionar una opción"),
    formation: Yup.string().required("Debe seleccionar una opción"),
    duration: Yup.string().required("Debe seleccionar una opción"),
    category: Yup.string().required("Debe seleccionar una opción"),
    modality: Yup.string().required("Debe seleccionar una opción"),
    inscription_start: Yup.mixed().required('Debe seleccionar una fecha'),
    inscription_end: Yup.mixed().required('Debe seleccionar una fecha'),
    cost: Yup.string().required("Debe seleccionar una opción"),
    price: Yup.number().typeError("Precio inválido").required('Campo requerido').min(0, "Precio inválido")
  });

  
  return (
    <Modal show={show}
    size='lg'
    onHide={onHide}
    backdrop="static"
    keyboard={false}
    aria-labelledby="modalUpdateCourse"
    centered
    scrollable
  >
    <Modal.Header closeButton style={{ background: "#0275d8", padding: "20px" }} >
      <Modal.Title style={{ color: "white" }}>
        Editar Taller
      </Modal.Title>
    </Modal.Header>
        <Formik
              initialValues={{
                id: taller.id,
                image: taller.image,
                name: taller.name,
                teacher: taller.teacher,
                description: taller.description,
                date: taller.date,
                hour: taller.hour,
                content: taller.content,
                language: taller.language,
                formation: taller.formation,
                category: taller.category,
                duration: taller.duration,
                modality: taller.modality,
                inscription_start: taller.inscription_start,
                inscription_end: taller.inscription_end,
                requirements: taller.requirements,
                cost: taller.cost,
                price: taller.price,
                observations: taller.observations
              }}
              enableReinitialize
              onSubmit={(values) => {
                handleSubmit(values);
              }}
              validationSchema={ErrorMessagesSchema}
            >
              {({ errors, touched, handleSubmit, handleChange, handleBlur,values, setFieldValue}) => (
                <Modal.Body>
            <Form onSubmit={handleSubmit}>
              <Container>
                     <Row className="m-2">
                     <Col xl={5} md={6}>
                     <Form.Group>
                     <Form.Label>Imagen del taller</Form.Label>
                     <div className="imagen">
                      <img src={CONFIG.BASE_URL + values.image} alt={values.name} style={{ width: '20em' }} />
                    </div>
                    <input type="file"  accept="image/*"  onClick={e => (e.target.value = null)} onChange={event => setFieldValue("image",event.target.files[0])}  name="image" className="form-control-file" id="production-img" />
                      {errors.image ? <div style={{ witch:"100%", marginTop: "0.2rem", fontSize: ".875em",color: "#dc3545"}}><ErrorMessage name="image"/></div> : null}
                      </Form.Group>
                     </Col>
                      </Row>
                      <Row className="m-2">
                  <Col lg={6} md={12} sm={12} className="pt-2">
                    <Form.Group>

                      <Form.Label>Nombre del taller</Form.Label>
                      <Form.Control
                        
                        type="text"
                        name="name"
                        placeholder="Ingresá el nombre del taller"
                        value={values.name}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        isInvalid={touched.name && errors.name}
                      />
                      <Form.Control.Feedback type="invalid">
                        {errors.name}
                      </Form.Control.Feedback>
                    </Form.Group>
                  </Col>
                  
                  <Col lg={6} md={12} sm={12} className="pt-2">
                    <Form.Group>

                      <Form.Label>Docente</Form.Label>
                      <Form.Control
                        
                        type="text"
                        name="teacher"
                        placeholder="Ingresá el nombre del docente"
                        value={values.teacher}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        isInvalid={touched.teacher && errors.teacher}
                      />
                      <Form.Control.Feedback type="invalid">
                        {errors.teacher}
                      </Form.Control.Feedback>
                    </Form.Group>
                  </Col>
                  
                </Row>
             
                <Row className="m-2">
                  <Col className="pt-2">
                    <Form.Group>
                      <InputGroup className="mb-3">
                        <InputGroup.Text id="description">Descripción</InputGroup.Text>
                        <Form.Control
                          style={{resize:'none'}}
                          type="textarea"
                          as="textarea"
                          name="description"
                          placeholder="Ingresá una breve descripción del taller"
                          value={values.description}
                          onChange={handleChange}
                          rows={20}
                          onBlur={handleBlur}
                          isInvalid={touched.description && errors.description}
                        />
                        <Form.Control.Feedback type="invalid">
                          {errors.description}
                        </Form.Control.Feedback>
                      </InputGroup>

                    </Form.Group>

                  </Col>
                </Row>
                 
                <Row className="m-2">
                <Col lg={6} md={12} sm={12} className="pt-2">
                    <Form.Group>

                      <Form.Label>Días de cursada</Form.Label>
                      <Form.Control
                          type="text"
                          name="date"
                          className="form-control"
                          placeholder="Ingresá los días de cursada"
                        value={values.date}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        isInvalid={touched.date && errors.date}
                      />
                      <Form.Control.Feedback type="invalid">
                        {errors.date}
                      </Form.Control.Feedback>
</Form.Group>
                  </Col>
                  <Col lg={6} md={12} sm={12} className="pt-2">
                    <Form.Group>

                      <Form.Label>Horarios de cursada</Form.Label>
                      <Form.Control
                          type="text"
                          name="hour"
                          className="form-control"
                          placeholder="Ingresá los horarios de cursada"
                        value={values.hour}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        isInvalid={touched.hour && errors.hour}
                      />
                      <Form.Control.Feedback type="invalid">
                        {errors.hour}
                      </Form.Control.Feedback>
</Form.Group>
                  </Col>
                  </Row>
                  
                  <Row className="m-2">
                  <Col className="pt-2">
                    <Form.Group>
                      <InputGroup className="mb-3">
                        <InputGroup.Text id="description">Contenido</InputGroup.Text>
                        <Form.Control
                          style={{resize:'none'}}
                          type="textarea"
                          as="textarea"
                          name="content"
                          placeholder="Ingresá el contenido del taller"
                          value={values.content}
                          onChange={handleChange}
                          rows={20}
                          onBlur={handleBlur}
                          isInvalid={touched.content && errors.content}
                        />
                        <Form.Control.Feedback type="invalid">
                          {errors.content}
                        </Form.Control.Feedback>
                      </InputGroup>

                    </Form.Group>

                  </Col>
                </Row>
                    
                <Row className="m-2">
                  <Col lg={6} className="pt-2">
                    <Form.Label>Lenguaje artístico</Form.Label>
                    <Form.Select
                      type="select"
                      as="select"
                      name="language"
                      value={values.language}
                      aria-label=".form-select-sm"
                      size="sm"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      isInvalid={errors.language && touched.language} >
                      <option hidden>Seleccioná el lenguaje artístico</option>
                      {languages.map(elem => (
                        <option key={elem.id} value={elem.value}>{elem.label}</option>
                      ))}
                    </Form.Select>
                    <Form.Control.Feedback type="invalid">
                      {errors.language}
                    </Form.Control.Feedback>
                  </Col>
                  <Col lg={6} className="pt-2">
                    <Form.Label>Formación</Form.Label>
                    <Form.Select
                            type="select"
                            as="select"
                            name="formation"
                            className="form-select form-select-sm"
                            size="sm"
                            value={values.formation}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      isInvalid={errors.formation && touched.formation} >
                            <option hidden>Seleccioná la formación</option>
                            {formations.map(elem => (
                              <option key={elem.id} value={elem.value}>{elem.label}</option>
                            ))}
                            </Form.Select>
                    <Form.Control.Feedback type="invalid">
                      {errors.formation}
                    </Form.Control.Feedback>
                  </Col>

                  </Row>
                <Row className="m-2">
                  <Col lg={6} className="pt-2">
                    <Form.Label>Destinatarios</Form.Label>
                    <Form.Select
                            
                            type="select"
                            as="select"
                            name="category"
                            className="form-select form-select-sm"
                            aria-label=".form-select-sm"
                            size="sm"
                            value={values.category}

                            onChange={handleChange}
                      onBlur={handleBlur}
                      isInvalid={errors.category && touched.category} >
                            <option hidden>Seleccioná los destinatarios</option>
                            {categories.map(elem => (
                              <option key={elem.id} value={elem.value}>{elem.label}</option>
                            ))}
                            </Form.Select>
                    <Form.Control.Feedback type="invalid">
                      {errors.category}
                    </Form.Control.Feedback>
                  </Col>
                  
                  <Col lg={6} className="pt-2">
                    <Form.Label>Duración</Form.Label>
                    <Form.Select
                            type="select"
                            as="select"
                            name="duration"
                            className="form-select form-select-sm"
                            aria-label=".form-select-sm"
                            size="sm"
                            value={values.duration}

                            onChange={handleChange}
                      onBlur={handleBlur}
                      isInvalid={errors.duration && touched.duration} >
                            <option hidden>Seleccioná la duración</option>
                            {durations.map(elem => (
                              <option key={elem.id} value={elem.value}>{elem.label}</option>
                            ))}
                            </Form.Select>
                    <Form.Control.Feedback type="invalid">
                      {errors.duration}
                    </Form.Control.Feedback>
                  </Col>
                  </Row>
 
<Row className="m-2">
                  <Col lg={6} className="pt-2">
                    <Form.Label>Modalidad</Form.Label>
                    <Row>
                    <Form.Group>

                      <Field component="div" name="modality">
                                            <Form.Check
                          inline
                          type="radio"
                          id="radioOne"
                          defaultChecked={values.modality === 1}
                          name="modality"
                          value={1}
                          label={modalities[0].label}
                          onBlur={handleBlur}
                          isInvalid={touched.modality && errors.modality} 


                        />

                        <Form.Check
                          inline
                          type="radio"
                          id="radioOne"
                          defaultChecked={values.modality === 2}
                          name="modality"
                          value={2}
                          onBlur={handleBlur}
                          label={modalities[1].label}
                          isInvalid={touched.modality && errors.modality} 

                        />
                         </Field>
                         {errors.modality && touched.modality ? <div style={{ witch:"100%", marginTop: "0.2rem", fontSize: ".875em",color: "#dc3545"}}><ErrorMessage name="modality"/></div> : null}

                        <Form.Control.Feedback type="invalid">
                         {errors.modality}

                    </Form.Control.Feedback>
                        </Form.Group>

                     
                    </Row>
                  </Col>
                  
                      </Row>
                     
            <Row className="m-2">

                  <strong><label className="pt-2">Inscripción</label></strong>
                  <Col lg={6} md={12} xs={12} className="pt-2">
                  <Form.Label>Fecha de inicio de la inscripción</Form.Label>
                  <DatePicker
                         selected={(values.inscription_start && new Date(values.inscription_start)) || null}
                          value={moment(values.inscription_start).local().format('DD/MM/YYYY')}
                          name="inscription_start" 
                          onChange={date => setFieldValue("inscription_start", date)}
                          errors={errors.inscription_start}
                          touched={touched.inscription_start}
                          locale="es" 
                          minDate={new Date()} 
                          dateFormat={format}
                          onKeyDown={(e) => {
                            e.preventDefault();
                           }} 
                          placeholderText="Seleccioná una fecha"/>


                         {errors.inscription_start && touched.inscription_start ? <div style={{ witch:"100%", marginTop: "0.2rem", fontSize: ".875em",color: "#dc3545"}}><ErrorMessage name="inscription_start"/></div> : null}
                  </Col>
                  <Col lg={6} md={12} xs={12} className="pt-2">
                  <Form.Label>Fecha de fin de la inscripción</Form.Label>
                  <DatePicker
                        selected={(values.inscription_end && new Date(values.inscription_end)) || null}
                          value={moment(values.inscription_end).local().format('DD/MM/YYYY')}
                          name="inscription_end" 
                          onChange={date => setFieldValue("inscription_end", date)}
                          errors={errors.inscription_end}
                          touched={touched.inscription_end}
                          locale="es" 
                          minDate={new Date()} 
                          dateFormat={format}
                          onKeyDown={(e) => {
                            e.preventDefault();
                           }} 
                          placeholderText="Seleccioná una fecha"/>

                         {errors.inscription_end && touched.inscription_end ? <div style={{ witch:"100%", marginTop: "0.2rem", fontSize: ".875em",color: "#dc3545"}}><ErrorMessage name="inscription_end"/></div> : null}

                  </Col>

                  </Row>
                  
                   
                  <Row className="m-2">
                  <Col className="pt-2">
                    <Form.Group>
                      <InputGroup className="mb-3">
                        <InputGroup.Text id="description">Requisitos</InputGroup.Text>
                        <Form.Control
                          style={{resize:'none'}}
                          type="textarea"
                          as="textarea"
                          name="requirements"
                          placeholder="Ingresá los requisitos del taller"
                          value={values.requirements}
                          onChange={handleChange}
                          rows={8}
                          onBlur={handleBlur}
                          isInvalid={touched.requirements && errors.requirements}
                        />
                        <Form.Control.Feedback type="invalid">
                          {errors.requirements}
                        </Form.Control.Feedback>
                      </InputGroup>

                    </Form.Group>

                  </Col>
                </Row>                

                
                      
                      <Row className="m-2">

                <Col lg={4} md={6} xs={12} className="pt-2">
                    <Form.Label>Costo</Form.Label>
                    <Form.Select
                      type="select"
                      as="select"
                      name="cost"
                      value={values.cost}
                      aria-label=".form-select-sm"
                      size="sm"
                      onChange={handleChange}
                      isInvalid={errors.cost && touched.cost} >
                      <option hidden>Seleccioná el costo</option>
                      {costs.map(elem => (
                        <option key={elem.id} value={elem.value}>{elem.label}</option>
                      ))}
                    </Form.Select>
                    <Form.Control.Feedback type="invalid">
                      {errors.cost}
                    </Form.Control.Feedback>
                  </Col>
                  {/* {values.cost.toString() === '2' ?  */}
                  <Col lg={3} md={6} xs={12} className="pt-2">
                    <Form.Group >
                      <Form.Label>Precio</Form.Label>
                      <InputGroup size="sm" className="mb-3">
                        <InputGroup.Text id="basic-addon1">$</InputGroup.Text>
                        <Form.Control
                          type="text"
                          name="price"
                          placeholder="0"
                          size="sm"
                          disabled={values.cost.toString() === '2' ? false : true}

                          value={values.cost.toString() === '2' ? values.price : values.price=0}
                          onChange={handleChange}
                          isInvalid={!!errors.price}
                        >
                        </Form.Control>
                        <Form.Control.Feedback type="invalid">
                          {errors.price}
                        </Form.Control.Feedback>
                      </InputGroup>
                    </Form.Group>
                  </Col>
                   </Row>
                   <Row className="m-2">
                  <Col className="pt-2">
                  <Form.Label>Observaciones</Form.Label>
                    <Form.Group>
                        <Form.Control
                          style={{resize:'none'}}
                          type="textarea"
                          as="textarea"
                          name="observations"
                          placeholder="Ingresá las observaciones del taller"
                          value={values.observations}
                          onChange={handleChange}
                          rows={6}
                          onBlur={handleBlur}
                        />
                      
                    </Form.Group>

                  </Col>
                </Row>                     

              <Modal.Footer>
                <Button type="submit" variant="primary">Enviar</Button>
                </Modal.Footer>
                </Container>
            </Form>
            </Modal.Body>
          )}
            </Formik>
      
    </Modal>
  );
}

export default ModalUpdate;