import React  from 'react';
import { useDispatch} from 'react-redux';
import { deleteCourse } from '../../../actions/courseAction';
import { Modal, Container, Row, Col, Button } from 'react-bootstrap';

const ModalDelete = ({taller, show, onHide, setOnHide}) => {
    const dispatch = useDispatch();

    const handleDelete = (e) => {
      e.preventDefault();
      dispatch(deleteCourse({taller}));
      setOnHide(false);
      //dispatch(getProductions)
    }

  return (
    <>
      <Modal
        show={show}
        size='md'
        onHide={onHide}
        backdrop="static"
        keyboard={false}
        aria-labelledby="modalDeleteCourse"
        centered
      >
        <Modal.Header closeButton style={{ background: '#bb2d3b', padding: "20px" }}>
          <Modal.Title style={{ color: 'white' }}>
            Eliminar taller
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Container>
            <Row>
              <Col md={12}>
                ¿Está seguro que desea eliminar el taller "{taller.name}"?
              </Col>
            </Row>
          </Container>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={handleDelete} type="button" variant="danger">Eliminar</Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}
export default ModalDelete;
