import React, { useState } from 'react';
import Card from './Card';
import Pagination from '../../../components/Pagination/pagination';
import './ListCards.css';

const ListCards = ({ cards = [] }) => {

  const coursesPerPage = 12;
  const [pageNumber, setPageNumber] = useState(0);
  const pageCount = Math.ceil(cards.length / coursesPerPage);
  const pagesVisited = pageNumber * coursesPerPage;

  const displayCards = cards
    .slice(pagesVisited, pagesVisited + coursesPerPage)
    .map((card,i) => {
      return (
        <Card key={i} card={card}/>
      )
    });

  const changePage = ({ selected }) => {
    setPageNumber(selected);
  };


  return (
    

      <div className="container-formation">

        <div className="container-formation-cards">
          {displayCards}
        </div>
        <Pagination
          pageCount={pageCount}
          changePage={changePage}
        />
      </div>

   
  )
}
export default ListCards;