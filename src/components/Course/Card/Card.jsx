import React from 'react';
import { Link } from 'react-router-dom';
import './Card.css'
import { categories, costs, languages, modalities } from '../data';
import CONFIG from '../../../configuration/configuration';
import moment from 'moment';
import 'moment/locale/es';

const Card = ({card} ) => {
    moment.locale('es');

    return (
            <div className="card-course">
                <div className="card-image">
                    <img src={CONFIG.BASE_URL + card.course.image} alt={card.course.image} style={{ height: '180px', width: '300px' }}  />
                </div>
                <div className="card-texto">
                    <span className="language" >{languages.map(option => (option.value === card.course.language ? option.label : ""))}
                    </span>
                    <h2>{card.course.name}</h2>
                    <p className="parrafo"><i className="bi bi-calendar px-2"></i>{card.course.date} </p>
                    <p className="parrafo"><i className="bi bi-clock px-2"></i>{card.course.hour}</p>
                    <p className="parrafo"><i className="bi bi-person-fill px-2"></i>{categories.map(option => option.value === card.course.category ? option.label : "")}</p>
                    <p className="parrafo"><i className="bi bi-geo-alt px-2"></i>{card.cultural_space.name}</p>

                {/* </div> */}
                <Link key={card.course.id} to={`/formacion/${card.course.id}`} style={{ textDecoration: "none" }}><button type="button" className="btn btn-primary">Ver más</button>
                    </Link></div>
                <div className="card-course-stats">
                    <div className="stat">
                        <div className="type">
                            Modalidad
                        </div>
                        <div className="value">
{modalities.map(option => option.value === card.course.modality ? option.label : "")}                        </div>

                    </div>
                    {/* <div className="stat">
                     <div className="type">
                     Categoría
                    </div>
                    <div className="value">
                     {categories.map(option => (option.value == card.category ? option.label : ""))}
                    </div>
                    
                </div> */}

                    <div className="stat">
                        <div className="type">
                            Costo                  </div>
                        <div className="value">
                             {card.course.price !== 0.00 ? `$ ${card.course.price}` : costs.map(option => (
                                option.value === card.course.cost ? option.label : ''))}

                        </div>

                    </div>
                </div>
            </div>
    );
}

export default Card;
