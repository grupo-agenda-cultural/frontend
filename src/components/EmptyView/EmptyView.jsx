import React, { useState, useEffect } from 'react';
import './EmptyView.css';
import error from '../../assets/images/error/error.jpg';
import Loading from '../Spinner/Spinner';

const EmptyView = () => {
    const [pending, setPending] = useState(true);

    useEffect(() => {
        const timeout = setTimeout(() => {
           setPending(false);
        }, 1800);
        return () => clearTimeout(timeout);
      }, []);
    
    return (
        <>
        { pending ?  <Loading/> :
        <div className="container-errores">
             {/* <div className="container-msg"> */}
                <img className="img-error" src={error} alt="error-img" />
                {/* </div> */}
                    <h3 className="msg-error text-center">No se han encontrado resultados para tu búsqueda </h3>
                    <p className="text-center">Intentá nuevamente con otras opciones</p>
                    {/* <p>Intente nuevamente con</p> */}
        </div>
        }
        </>
    );
}

export default EmptyView;