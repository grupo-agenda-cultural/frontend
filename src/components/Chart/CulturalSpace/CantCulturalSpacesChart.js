import React, {useState, useEffect} from 'react';
import axiosInstance from "../../../services/axiosApi";
import { Doughnut } from 'react-chartjs-2';

const Chart = () => {

  const [espaciosCulturales, setEspaciosCulturales] = useState([]);

  const getEspaciosCulturales = async () => {
    return axiosInstance
      .get("/culturalspaces/")
      .then((response) => response.data);
  }

  useEffect(() => {
    getEspaciosCulturales().then((espacioCultural) => {
      setEspaciosCulturales(espacioCultural);
    });
  }, [setEspaciosCulturales]);



  const cantidad = (espacio) => { 
    return espaciosCulturales.filter(espacioCultural => espacioCultural.partido === espacio).length
  }
  return(
  <div className="card shadow mb-4">
    <div className="card-header py-3 bg-primary">
      <h5 className="m-0 font-weight-bold text-white text-center">Espacios Culturales Por Municipio - Cargados en el sistema</h5>
    </div>
    <div className="card-body">
      <div className="chart-pie pt-4">
        <Doughnut data={{

          labels: ['José C Paz', 'Malvinas Argentinas',
          'San Miguel'],
        datasets: [
        {
          label: '# of Votes',
          data: [cantidad(1), cantidad(2), cantidad(3)],
        backgroundColor: [
        '#FF6384',
        '#36A2EB',
        '#5DE39C'
        ],
        borderColor: [
        '#FF6384',
        '#36A2EB',
        '#5DE39C'

        ],
        borderWidth: 1,
      },
    ],}
        }/>
      </div>
    </div>
  </div>
  );
}

export default Chart;