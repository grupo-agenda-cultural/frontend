import React from 'react';
import { Bar } from 'react-chartjs-2';

const data = {
  labels: ['José C Paz', 'Malvinas Argentinas',
      'San Miguel'
      ],
  datasets: [
    {
      label: 'Total',
      data: [9, 28, 46],
      backgroundColor: [
          '#3ADE7D',
          '#E06058',
          '#6B63E0',
          
      ],
      borderColor: [
          '#3ADE7D',
          '#E06058',
          '#6B63E0',
         
       
      ],
      borderWidth: 1,

    },

  ],

};

const options = {
  plugins: {
    datalabels: {
      display: true,
      color: "black",
      align: "end",
      anchor: "end",
      font: { size: "14" }
    },
  legend: {
    display: false
  }
}
};

const LocationChart = () => {
  return (
    <div className="card shadow mb-4">
      <div className="card-header py-3 bg-primary">
        <h5 className="m-0 font-weight-bold text-white text-center">Espacios Culturales Por Municipio - Último Relevamiento Observatorio Cultural UNGS</h5>
      </div>
      <div className="card-body">
        <div className="chart-pie pt-4">
          <Bar data={data} options={options} />
        </div>
      </div>
    </div>
  );
}

export default LocationChart;