import React, {useState, useEffect} from 'react';
import axiosInstance from "../../../services/axiosApi";
import { Pie } from 'react-chartjs-2';

const Chart = () => {

  const [espaciosCulturales, setEspaciosCulturales] = useState([]);

  const getEspaciosCulturales = async () => {
    return axiosInstance
      .get("/culturalspaces/")
      .then((response) => response.data);
  }

  useEffect(() => {
    getEspaciosCulturales().then((espacioCultural) => {
      setEspaciosCulturales(espacioCultural);
    });
  }, [setEspaciosCulturales]);



  const cantidad = () => { 
    return espaciosCulturales.length
  }

  return(
  <div className="card shadow mb-4">
    <div className="card-header py-3 bg-primary">
      <h5 className="m-0 font-weight-bold text-white text-center">Cantidad de espacios culturales / Último relevamiento del Observatorio Cultural UNGS</h5>
    </div>
    <div className="card-body">
    <p className="text-secondary text-center">Gráfico de las producciones por lenguaje artístico</p>
      <div className="chart-pie pt-4">
        <Pie data={{

          labels: ['Espacios Culturales de A.C.C.A', 'Último relevamiento'],
        datasets: [
        {
          label: '# de espacios culturales',
          data: [cantidad(), 83],
          backgroundColor: [
            'rgb(255, 99, 132)',
      'rgb(54, 162, 235)',
    ],
    hoverOffset: 2
  }]
}
        }/>
      </div>
    </div>
  </div>
  );
}

export default Chart;