import React from 'react';
import { Bar } from 'react-chartjs-2';
import 'chartjs-plugin-datalabels';

  const CategoryCourseChart = ({courses}) => {
    
    const cant = (category) => { 
       return courses.filter(course => course.category === category).length
     }

     const options = {
        plugins: {
            legend: {
                display: false,
                labels: {
                  color: 'rgb(255, 99, 132)'
              }
            }
        }
      }
      
     return(
    <div className="card shadow mb-4"> 
      <div className="card-header py-3">
        <h6 className="m-0 font-weight-bold text-primary">Por categoría</h6>                        
        </div>
        <div className="card-body">
        <p className="text-secondary text-center">Gráfico de los talleres por categoría</p>
          <div className="chart-pie pt-2">
          <Bar data={{
    labels: ['Adultos', 'Niños', 'Público en general'],    
    datasets: [
      {       
        label: 'Total',
        data: [cant(1),cant(2),cant(3)],
        backgroundColor: [
            '#FCA603',
            '#B467E0',
            '#5DE39C'
        ],
        borderColor: [
            '#FCA603',
            '#B467E0',
            '#5DE39C'
         
        ],
        borderWidth: 1,
      },
    ],
  }} options={options}/> 
            </div>
      </div>
      </div>
    
  );}
  
export default CategoryCourseChart;