import React from 'react';
import "chartjs-plugin-datalabels";
import { Bar } from 'react-chartjs-2';

  const options = {
    plugins: {
      datalabels: {
        display: true,
        color: "black",
        align: "end",
        anchor: "end",
        font: { size: "14" }
      },
    legend: {
      display: false
    }
}
  };
 
  
  const LanguageChart = ({productions}) => {

    const cantidad = (language) => { 
      return productions.filter(production => production.language === language).length
    }

    return(
    <div className="card shadow mt-2">
      <div className="card-header py-3">
        <h6 className="m-0 font-weight-bold text-primary">Por lenguaje artístico</h6>                        
        </div>
        <div className="card-body">
        
          <div className="chart-pie pt-4">
          <p className="text-secondary text-center">Gráfico de las producciones por lenguaje artístico</p>

          <Bar data={{
    labels: ['Audiovisuales', 
        'Artes Visuales',
        'Danza',
        'Ferias',
        'Festivales',
        'Letras',
        'Música',
        'Peñas',
        'Teatro'
        ],
    datasets: [
      {
        label: 'Total',
        data: 
         [cantidad(1),cantidad(2), cantidad(3), cantidad(4),
         cantidad(5),cantidad(6),cantidad(7),cantidad(8),cantidad(9)],
        backgroundColor: [
            '#3ADE7D',
            '#E06058',
            '#6B63E0',
            '#6EE0A6',
            '#4278DB',
            '#DE4D0B',
            '#E0D872',
            '#E67E7A',
            '#A287E6'
        ],
        borderColor: [
            '#3ADE7D',
            '#E06058',
            '#6B63E0',
            '#6EE0A6',
            '#4278DB',
            '#DE4D0B',
            '#E0D872',
            '#E67E7A',
            '#A287E6'
         
        ],
        borderWidth: 1,
      },
    ],
  }} options={options}/>
      </div>
      </div>
      </div>
    
  )};
  
export default LanguageChart;