import React from 'react';
import { Doughnut } from 'react-chartjs-2';

const Chart = ({productions}) => {

  
  const cantidad = (cost) => { 
    return productions.filter(production => production.cost === cost).length
  }

return(
  <div className="card shadow mb-4">
    <div className="card-header py-3">
      <h6 className="m-0 font-weight-bold text-primary">Por costo</h6>
    </div>
    
    <div className="card-body">
    <p className="text-secondary text-center">Gráfico de las producciones por costo</p>
      <div className="chart-pie pt-2">
        <Doughnut data={{

          labels: ['A la gorra', 'Con precio', 'Gratuito'],
           datasets: [
          {
          label: '# of Votes',
          data: [cantidad(1),cantidad(2),cantidad(3)],
         backgroundColor: [
         '#FF6384',
         '#36A2EB',
         '#FFCE56'
        ],
        borderColor: [
        '#FF6384',
        '#36A2EB',
        '#FFCE56'

        ],
        borderWidth: 1,
      },
    ],}
        }/>
      </div>
    </div>
  </div>

);
}

export default Chart;