import React from 'react';
import { Marker, Popup } from 'react-leaflet'
import markerIconPng from "leaflet/dist/images/marker-icon.png"
import { Icon } from 'leaflet';
import "leaflet/dist/leaflet.css";
import { localidades, partidos } from '../CulturalSpace/data';
import CONFIG from '../../configuration/configuration';
const Markers = (props) => {
    
    const { cultural_spaces } = props;
    const markers = cultural_spaces.map((cultural_space, i) => {
       return(
       <Marker key={i} position={[cultural_space.latitude, cultural_space.longitude]} icon={new Icon({ iconUrl: markerIconPng, iconSize: [25, 41], iconAnchor: [12, 41] })}>
            <Popup maxWidth="350" maxHeight="auto">
            <img height="150px" width="100%" src={CONFIG.BASE_URL + cultural_space.image} alt={cultural_space.image} />
            <div className="container text-center">
            <h5 className="py-2 mb-1 text-primary">{cultural_space.name}</h5>
            Dirección: {cultural_space.address}<br/>
            Partido: {partidos.map(option => option.value === cultural_space.partido ? option.label : '')}<br/>
            Localidad: {localidades.map(option => option.value === cultural_space.localidad ? option.label : '')}<br/>
            Teléfono: {cultural_space.phone}<br/>
            Email: {cultural_space.email}<br/>
            {cultural_space.instagram ? <a href={cultural_space.instagram} target="_blank" rel="noreferrer"><i className="bi bi-instagram" style={{fontSize:"24px",color:" #f54056"}}></i></a> : ''}
            &nbsp;
            {cultural_space.facebook ? <a href={cultural_space.facebook} target="_blank" rel="noreferrer"><i className="bi bi-facebook" style={{fontSize:"24px",color:"#3f729b"}}></i></a> : '' } <br/>
            <a href={`https://maps.google.com/?q=${cultural_space.latitude},${cultural_space.longitude}`} target="_blank" rel="noreferrer"><button type="button" className="btn btn-primary">¿Cómo llegar?</button></a>
            </div>    
            </Popup>
        </Marker>
       )
    })
    return <>{markers}</>;
}

export default Markers;