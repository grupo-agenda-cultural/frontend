import React, { useEffect } from 'react';
import { MapContainer, TileLayer } from 'react-leaflet'
import { useDispatch, useSelector } from 'react-redux';
import { getCulturalSpaces } from '../../actions/culturalSpaceAction';
import Markers from './Markers';

const Map = () => {

  const dispatch = useDispatch();
  const position = [-34.51916996951426, -58.701842687423294]
  const cultural_spaces = useSelector(state => state.cultural_spaces.cultural_spaces);

  useEffect(() => {
      dispatch(getCulturalSpaces())
      // eslint-disable-next-line 
  }, []);

  return (
    <>
      <div className="container-fluid bg-light" style={{
    position: 'relative',
    zIndex:0
  }}>

        <MapContainer center={position} zoom={12.4} scrollWheelZoom={false} style={{ height: '90vh', width: '100wh'}}>
          <TileLayer
            attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
          />
          <Markers cultural_spaces={cultural_spaces} />
        </MapContainer>
      </div>
    </>
  );
}

export default Map;