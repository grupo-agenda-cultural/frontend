import React from 'react';
import { Spinner } from 'react-bootstrap';

const Loading =   () => {     
    return (
        <div className="container-fluid">
        <div className="row justify-content-center">
        <div className="col-md-12 justify-content-center text-center">
        <Spinner animation="border" role="status" variant="primary" style={{ width: "5rem", height: "5rem" }}>
        </Spinner>
    </div></div>
    </div>
    );
}
export default Loading;