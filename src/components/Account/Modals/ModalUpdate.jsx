import React from 'react';
import { useDispatch } from 'react-redux';
import { updateUser } from "../../../actions/usersAction";
import * as Yup from 'yup'
import { Formik } from "formik";
import { Form, Modal, Button } from 'react-bootstrap';

const ModalUpdate = ({ coordinador, show, onHide }) => {

  const dispatch = useDispatch();

  const handleSubmit = (values, coordinador) => {
    dispatch(updateUser(values, coordinador));
  }

  const ErrorMessagesSchema = Yup.object().shape({
    name: Yup.string().required('Campo requerido').matches(/^[a-zA-Z]+$/, {
      message: "Nombre inválido",
      excludeEmptyString: false
    }),
    last_name: Yup.string().required('Campo requerido').matches(/^[a-zA-Z]+$/, {
      message: "Apellido inválido",
      excludeEmptyString: false
    }),
    phone: Yup.string()
      .matches(/^(?:(?:00)?549?)?0?(?:11|[2368]\d)(?:(?=\d{0,2}15)\d{2})??\d{8}$/, {
        message: "Teléfono inválido",
        excludeEmptyString: false
      }).required('Campo requerido'),
    email: Yup.string().email('Email inválido').required('Campo requerido'),
  });


  return (
    <Modal show={show}
      size='md'
      onHide={onHide}
      backdrop="static"
      keyboard={false}
      aria-labelledby="modalUpdateAccount"
      centered
    >
      <Modal.Header closeButton style={{ background: "#0275d8", padding: "20px" }} >
        <Modal.Title style={{ color: "white" }}>
          Editar coordinador
        </Modal.Title>
      </Modal.Header>
      <Formik
        enableReinitialize
        initialValues={{
          id: coordinador.id,
          name: coordinador.name,
          last_name: coordinador.last_name,
          phone: coordinador.phone,
          email: coordinador.email
        }}
        onSubmit={(values) => {
          handleSubmit(values, coordinador);
        }}
        validationSchema={ErrorMessagesSchema}
      >
        {({ errors, touched, handleSubmit, handleChange, values, isValid, isSubmitting }) => (

          <Modal.Body>
            <Form noValidate onSubmit={handleSubmit}>

              <Form.Group md={12} controlId="validationName">
                <Form.Label>Nombre</Form.Label>
                <Form.Control
                  type="text"
                  name="name"
                  placeholder="Ingresá el nombre del coordinador"
                  value={values.name}
                  onChange={handleChange}
                  isValid={touched.name && !errors.name}
                  isInvalid={!!errors.name}
                />
                <Form.Control.Feedback type="invalid">
                  {errors.name}
                </Form.Control.Feedback>
              </Form.Group>
              <Form.Group md={12} controlId="validationLastname" className="mt-2">
                <Form.Label>Apellido</Form.Label>
                <Form.Control
                  type="text"
                  name="last_name"
                  placeholder="Ingresá el apellido del coordinador"
                  value={values.last_name}
                  onChange={handleChange}
                  isValid={touched.last_name && !errors.last_name}
                  isInvalid={!!errors.last_name}
                />
                <Form.Control.Feedback type="invalid">
                  {errors.last_name}
                </Form.Control.Feedback>
              </Form.Group>
              <Form.Group md={12} controlId="validationPhone" className="mt-2">
                <Form.Label>Teléfono</Form.Label>
                <Form.Control
                  type="text"
                  name="phone"
                  placeholder="Ingresá el teléfono del coordinador"
                  value={values.phone}
                  onChange={handleChange}
                  isValid={touched.phone && !errors.phone}
                  isInvalid={!!errors.phone}
                />
                <Form.Control.Feedback type="invalid">
                  {errors.phone}
                </Form.Control.Feedback>
              </Form.Group>
              <Form.Group md={12} controlId="validationEmail" className="mt-2">
                <Form.Label>Email</Form.Label>
                <Form.Control
                  type="text"
                  name="email"
                  placeholder="Ingresá el email del coordinador"
                  value={values.email}
                  onChange={handleChange}
                  isValid={touched.email && !errors.email}
                  isInvalid={!!errors.email}
                />
                <Form.Control.Feedback type="invalid">
                  {errors.email}
                </Form.Control.Feedback>
              </Form.Group>

              <Modal.Footer>
                <Button type="submit" onClick={isValid ? onHide : ''}>Enviar</Button>
              </Modal.Footer>
            </Form>

          </Modal.Body>
        )}
      </Formik>
    </Modal>
  );
};

export default ModalUpdate;