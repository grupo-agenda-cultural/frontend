import React from 'react';
import { Modal, Container, Row, Col, Button } from 'react-bootstrap';

const ModalView = ({ coordinadorView, show, onHide }) => {


  return (
    <Modal
      show={show}
      size='lg'
      onHide={onHide}
      backdrop="static"
      keyboard={false}
      aria-labelledby="modalViewAccount"
      centered
    >
      <Modal.Header closeButton style={{ background: 'gray', padding: "20px" }}>
        <Modal.Title id="contained-modal-title-center" style={{ color: 'white' }}>
          Visualizar Coordinador
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Container>
          <Row>
            <Col md={6}>
              <h5>Nombre</h5>
              <p>{coordinadorView.user ? coordinadorView.user.name : ''}</p>
            </Col>
            <Col md={6}>
              <h5>Apellido</h5>
              <p>{coordinadorView.user ? coordinadorView.user.last_name : ''}</p>
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <h5>Email</h5>
              <p>{coordinadorView.user ? coordinadorView.user.email : ''}</p>
            </Col>
            <Col md={6}>
              <h5>Teléfono</h5>
              <p>{coordinadorView.user ? coordinadorView.user.phone : ''}</p>
            </Col>
          </Row>

          <Row>
            <Col md={12}>
              <h5>Estado de cuenta</h5>
              <p>{coordinadorView.user ? coordinadorView.user.is_active ? 'Activa' : 'Inactiva' : ''}</p>
            </Col>
          </Row>
          <Row>
            <Col md={12}>
              <h5>{coordinadorView.user ? coordinadorView.user.is_active ? 'Espacio cultural' : '' : ''}</h5>
              {coordinadorView.user ? coordinadorView.user.is_active ?
                coordinadorView.cultural_space ?
                  coordinadorView.cultural_space.name :
                  'El coordinador no creó su espacio cultural' : '' : ''
              }
            </Col>
          </Row>
        </Container>
      </Modal.Body>
      <Modal.Footer>
      <Button variant="secondary" onClick={onHide}>Cerrar</Button>
      </Modal.Footer>
    </Modal>
  );
}

export default ModalView;