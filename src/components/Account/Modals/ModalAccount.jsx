import React from 'react';
import { useDispatch } from 'react-redux';
import { Modal, Container, Row, Col, Button } from 'react-bootstrap';
import { enableUser, disableUser } from '../../../actions/usersAction';

const ModalAccount = ({ user, show, onHide, setOnHide }) => {

  const dispatch = useDispatch();

  const handleEnable = (e) => {
    e.preventDefault();
    dispatch(enableUser({ user }));
    setOnHide(false)

  }

  const handleDisable = (e) => {
    e.preventDefault();
    dispatch(disableUser({ user }));
    setOnHide(false)
  }


  return (
    <>
      {!user.is_active ?

        <Modal
          show={show}
          size='md'
          onHide={onHide}
          backdrop="static"
          keyboard={false}
          aria-labelledby="modalEnableAccount"
          centered
        >
          <Modal.Header closeButton style={{ background: '#157347', padding: "20px" }}>
            <Modal.Title style={{ color: 'white' }}>
              Habilitar cuenta
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Container>
              <Row>
                <Col md={12}>
                  ¿Está seguro que desea habilitar la cuenta de {user.name} {user.last_name}?

                </Col>
              </Row>
            </Container>
          </Modal.Body>
          <Modal.Footer>
            <Button onClick={handleEnable} type="button" variant="success">Habilitar</Button>
          </Modal.Footer>
        </Modal>
        :
        <Modal
          show={show}
          size='md'
          onHide={onHide}
          backdrop="static"
          keyboard={false}
          aria-labelledby="modalDisableAccount"
          centered
        >
          <Modal.Header closeButton style={{ backgroundColor: "#bb2d3b", padding: "20px" }}>
            <Modal.Title style={{ color: 'white' }}>
              Deshabilitar cuenta
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Container>
              <Row>
                <Col md={12}>
                  ¿Está seguro que desea deshabilitar la cuenta de {user.name} {user.last_name}?

                </Col>
              </Row>
            </Container>
          </Modal.Body>
          <Modal.Footer>
            <Button onClick={handleDisable} type="button" variant="danger">Deshabilitar</Button>
          </Modal.Footer>
        </Modal>}
    </>
  );
};

export default ModalAccount;