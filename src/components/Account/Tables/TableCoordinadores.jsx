import { React, useState, useEffect, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Button } from "../../Button/Button";
import ModalUpdate from "../Modals/ModalUpdate"
import ModalAccount from '../Modals/ModalAccount';
import ModalView from '../Modals/ModalView';
import { getCoordinadores} from '../../../actions/usersAction';
import DataTable from 'react-data-table-component';
import DataTableExtensions from 'react-data-table-component-extensions';
import 'react-data-table-component-extensions/dist/index.css';
import Spinner from "../../Spinner/Spinner";

const TableCoordinadores = () => {

  const dispatch = useDispatch();
  const users_coordinadores = useSelector(state => state.users.coordinadores);
  const [userChoose, setUserChoose] = useState('');
  const [pending, setPending] = useState(true);
  const [showModalUpdate, setShowModalUpdate] = useState(false);
  const [showModalView, setShowModalView] = useState(false);
  const [showModalAccount, setShowModalAccount] = useState(false);

  useEffect(() => {
    const loadCoordinadores = () => dispatch(getCoordinadores())
    loadCoordinadores();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  
  useEffect(() => {
    const timeout = setTimeout(() => {
      const loadCoordinadores = () => { 
        dispatch(getCoordinadores())
      }; 
      loadCoordinadores();
      setPending(false);
		}, 1800);
		return () => clearTimeout(timeout);
    // eslint-disable-next-line
	}, []);

  const setRowUserUpdate = (user) => {
    setUserChoose(user);
    setShowModalUpdate(true)
  }

  const setRowUserView = (user) => {
    setUserChoose(user);
    setShowModalView(true)
  }
  const setRowUserAccount = (user) => {
    setUserChoose(user);
    setShowModalAccount(true)
  }
  
  const CustomLoader = () => (
    <div className="loading">  
      <Spinner />
      <h5 className="text-center" style={{color:"rgb(26,53,64,25)"}}>Cargando...</h5>
    </div>  
  );

  const customTheme= {
    rows: {
      style: {
          fontSize:'14px',
          padding:'10px 0',
          justifyContent:'center'
      },
  },
  headCells: {
      style: {
          justifyContent: 'left',
          fontSize:'14px',
      },
  },
  cells: {
    style: {
        justifyContent: 'left'
    },
},
};

  const columns = useMemo(
    () => [
      {
        name: "Nombre",
        selector: row => row.user.name,
        sortable: true,
      },
      {
        name: "Apellido",
        selector: row => row.user.last_name,
        sortable: true,
      },
      {
        name: "Teléfono",
        selector: row => row.user.phone,
        sortable: true,
      },
      {
        name: "Email",
        selector: row => row.user.email,
        sortable: true,
        wrap: true,
        width: "250px"
      },
       {
         name: "Espacio cultural",
         selector: (row) => (row.user.is_active ?row.cultural_space !== '' ? row.cultural_space.name : 'El coordinador no creó su espacio cultural' : ''),
         sortable: true,
         wrap: true
      },
      {
        cell: (row) => <Button onClick={() =>  setRowUserView(row)} type="button" className="btn btn-secondary" icon="bi bi-eye-fill"></Button>,
        ignoreRowClick: true,
        allowOverflow: true,
        button: true,
        width: "48px"
      },
      {
        cell: (row) => <Button onClick={() => setRowUserUpdate(row.user)} type="button" className="btn btn-primary" icon="bi bi-pencil-square"></Button>,
        ignoreRowClick: true,
        allowOverflow: true,
        button: true,
        width: "48px"
      },
      {
        cell: (row) => !row.user.is_active ? (
          <Button onClick={() => setRowUserAccount(row.user)} type="button" className="btn btn-success btn-block"  icon="bi bi-person-check"> Habilitar</Button>
        ) : <Button onClick={() => setRowUserAccount(row.user)} type="button" className="btn btn-danger" icon="bi bi-person-dash"> Deshabilitar</Button>
        ,
        selector: (row) => row.user.is_active,
        sortable: true,
        ignoreRowClick: true,
        allowOverflow: false,
        button: true,
        width:'11em'
      },
    ],
    [],
  );

  const paginationComponentOptions = {
    rowsPerPageText: 'Filas por página',
    rangeSeparatorText: 'de',
    selectAllRowsItem: true,
    selectAllRowsItemText: 'Todos',
  };

  const tableData = {
    columns,
    data: users_coordinadores
  };


  return (
    <>
        <div className="card shadow card-body m-3">
          <div className="row justify-content-center">
          <DataTableExtensions filterPlaceholder="Buscar coordinadores"  export={false} print={false}
            {...tableData}
            >
            <DataTable
              noHeader
              title="Tabla de coordinadores"
              columns={columns}
              customStyles={customTheme}
              data={users_coordinadores}
              center
              defaultSortField="id"
              pagination paginationComponentOptions={paginationComponentOptions}
              progressPending={pending}
              progressComponent={<CustomLoader />}
              noDataComponent="No se encuentran registros de los coordinadores en la base de datos"
            />
            </DataTableExtensions>
          </div>
        </div>
      
      <ModalView coordinadorView={userChoose} show={showModalView} onHide={()=>setShowModalView(false)}/>
      <ModalUpdate coordinador={userChoose} show={showModalUpdate} onHide={()=>setShowModalUpdate(false)}/>
      <ModalAccount user={userChoose} show={showModalAccount} onHide={()=>setShowModalAccount(false)} setOnHide={setShowModalAccount}/>
    </>
  );
};

export default TableCoordinadores;