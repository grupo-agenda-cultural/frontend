import React from 'react'
import {Carousel} from 'react-bootstrap'
import './Slider.css';
import image1 from "../../assets/images/carousel/acca.png";
import image2 from "../../assets/images/carousel/carousel1.png";
import image3 from "../../assets/images/carousel/carousel2.png";
import image4 from "../../assets/images/carousel/espacios-culturales.png";

const Slider = () => {
    return (
        <>
            {/* <Carousel prevLabel='' nextLabel='' > */}
            <Carousel controls={false}>
            <Carousel.Item interval={5000}>
                    <a href="/inicio"><img
                        className="img-carousel d-block w-100"
                        src={image1}
                        alt="acca"
                    /></a>
                </Carousel.Item>
                <Carousel.Item interval={8000}>
                    <a href="/agenda"><img
                        className="img-carousel d-block w-100"
                        src={image2}
                        alt="agenda"
                    /></a>
                </Carousel.Item>
                <Carousel.Item interval={8000}>
                <a href="/formacion"><img
                        className="d-block w-100"
                        src={image3}
                        alt="formacion"
                    /></a>
                </Carousel.Item>
                 <Carousel.Item interval={8000}>
                <a href="/espacios-culturales"><img
                        className="d-block w-100"
                        src={image4}
                        alt="espacios-culturales"
                    /></a>
                </Carousel.Item>
            </Carousel>
        </>
    )
}

export default Slider;