import React from "react";

export const SubscriptionCulturalSpace = () => {
  return (
    <>
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
        <path
          fill=" #edf1f3"
          fillOpacity="1"
          d="M0,96L80,106.7C160,117,320,139,480,170.7C640,203,800,245,960,245.3C1120,245,1280,203,1360,181.3L1440,160L1440,320L1360,320C1280,320,1120,320,960,320C800,320,640,320,480,320C320,320,160,320,80,320L0,320Z"
        ></path>
      </svg>
      <div
        className="container-fluid text-center p-4"
        style={{ height: "20", backgroundColor: "rgb( 237, 241, 243 )" }}
      >
        <div className="row justify-content-center ">
          <div className="col-md-12">
            <h1 className="text-primary mb-4" style={{ fontSize: "3em" }}>
              <i className="bi bi-envelope-fill"></i> Suscribite a tu espacio cultural
            </h1>
            <p>
              Ingresá tu correo electrónico y enterate de todas las novedades que tenemos para contarte.
              ¡Sumate!
            </p>
          </div>
          {/* <div className="col-md-5">
            <div className="input-group my-4"></div>
            <select className="form-select">
              <option selected>Seleccionar partido del espacio cultural</option>
              <option value="1">Malvinas Argentinas</option>
              <option value="2">San Miguel</option>
              <option value="3">José C. Paz</option>
            </select>
            <input
              type="text"
              className="form-control"
              placeholder="Ingresa tu correo electrónico"
              aria-label="Recipient's username"
              aria-describedby="button-addon2"
            />
            <Button
              type="button"
              className="btn btn-primary"
              toggle="modal"
              target="#modalOk"
            >
              Suscribirse
            </Button> */}
          {/* </div> */}
        </div>
      </div>
    </>
  );
};
