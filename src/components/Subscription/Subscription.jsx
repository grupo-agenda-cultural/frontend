import React from 'react';

const Subscription = () => {
    return (
        <>
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320"><path fill=" #edf1f3" fillOpacity="1" d="M0,96L80,106.7C160,117,320,139,480,170.7C640,203,800,245,960,245.3C1120,245,1280,203,1360,181.3L1440,160L1440,320L1360,320C1280,320,1120,320,960,320C800,320,640,320,480,320C320,320,160,320,80,320L0,320Z"></path></svg>
        <div className="container-fluid text-center p-4" style={{height:"20", backgroundColor:"rgb( 237, 241, 243 )" }}>
      
            <div className="row justify-content-center ">
                <div className="col-md-12">
                <h1 className="text-primary mb-4" style={{fontSize:"3em"}}><i className="bi bi-envelope-fill"></i> Suscribite a la agenda</h1>
                <p>Enterate de todas las novedades que tenemos para contarte. ¡Sumate!</p>

                </div>
            <div className="col-md-5">
               <div className="input-group my-4">
                <input type="text" className="form-control" placeholder="Ingresa tu correo electrónico" aria-label="Recipient's username" aria-describedby="button-addon2"/>
                <button className="btn btn-primary" style={{backgroundColor:"rgb(13, 143, 223)"}} type="button" id="button-addon2">Suscribirse</button>
                </div> 
                </div>

            
            </div>
        </div>
        </>
    );
}

export default Subscription;
