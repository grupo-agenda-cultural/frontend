import { useEffect, useState, useMemo } from "react";
import { Button } from '../../Button/Button';
import ModalView from "../Modals/ModalView";
import ModalUpdate from "../Modals/ModalUpdate";
import DataTable from 'react-data-table-component';
import DataTableExtensions from 'react-data-table-component-extensions';
import 'react-data-table-component-extensions/dist/index.css';
import Spinner from "../../Spinner/Spinner";
import { localidades, partidos } from "../data";
import { useDispatch , useSelector} from "react-redux";
import { getCulturalSpaces } from '../../../actions/culturalSpaceAction';

const TableCulturalSpaces = () => {

  const dispatch = useDispatch();
  const cultural_spaces = useSelector(state => state.cultural_spaces.cultural_spaces);
  const [selectedCulturalSpace, setSelectedCulturalSpace] = useState('');
  const [pending, setPending] = useState(true);
  const [showModalView, setShowModalView] = useState(false);
  const [showModalUpdate, setShowModalUpdate] = useState(false);

  const selectCulturalSpaceUpdate = (espacio) => {
    setSelectedCulturalSpace(espacio);
    setShowModalUpdate(true)
  };

  const selectCulturalSpaceView = (espacio) => {
    setSelectedCulturalSpace(espacio);
    setShowModalView(true)
  };

  useEffect(() => {
    const timeout = setTimeout(() => {
      const loadProductions = () => {
        dispatch(getCulturalSpaces())
      }; 
      loadProductions();

      setPending(false);
    }, 1800);
    return () => clearTimeout(timeout);
     // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [cultural_spaces]);


  const CustomLoader = () => (
    <div className="loading">  
      <Spinner />
      <h5 className="text-center" style={{color:"rgb(26,53,64,25)"}}>Cargando...</h5>
    </div>  
  );

  const paginationComponentOptions = {
    rowsPerPageText: 'Filas por página',
    rangeSeparatorText: 'de',
    selectAllRowsItem: true,
    selectAllRowsItemText: 'Todos',
  };

  const customTheme= {
    rows: {
      style: {
          fontSize:'14px',
          padding:'10px 0',
          justifyContent:'center'
      },
  },
  headCells: {
      style: {
          justifyContent: 'left',
          fontSize:'14px',
      },
  },
  cells: {
    style: {
        justifyContent: 'left'
    },
},
};

  const columns = useMemo(
    () => [
      {
        name: "Nombre",
        selector: row => row.name,
        sortable: true,
        wrap: true
      },
      {
        name: "Dirección",
        selector: row => row.address,
        sortable: true,
        wrap: true
      },
      {
        name: "Partido",
        selector: row => partidos.map(option => option.value === row.partido ? option.label : ''),
        sortable: true,
        wrap: true,
      },
      {
        name: "Localidad",
        selector: row => localidades.map(option => option.value === row.localidad ? option.label : ''),
        sortable: true,
        wrap: true,
      },
      {
        name: "Teléfono",
        selector: row => row.phone,
        sortable: true,
      },
      {
        name: "Email",
        selector: row => row.email,
        sortable: true,
        wrap: true,
        width:"220px"
      },
      {
        selector: row => row.facebook ? <a href={row.facebook} target="_blank" rel="noreferrer"><i className="bi bi-facebook" style={{ fontSize: "24px", color: "#3f729b" }}></i></a> : '',
        width:"4em"
      },
      {
        selector: row => row.instagram ? <a href={row.instagram} target="_blank" rel="noreferrer"><i className="bi bi-instagram" style={{ fontSize: "24px", color: " #f54056" }}></i></a> : '',
        width:"4em"
      },
      {
        cell: (row) => <Button onClick={() => selectCulturalSpaceView(row)} type="button" className="btn btn-secondary" icon="bi bi-eye-fill"></Button>,
        ignoreRowClick: true,
        allowOverflow: true,
        button: true,
        width: "50px"
      },
      {
        cell: (row) => <Button onClick={() => selectCulturalSpaceUpdate(row)} type="button" className="btn btn-primary" icon="bi bi-pencil-square"></Button>,
        ignoreRowClick: true,
        allowOverflow: true,
        button: true,
        width: "50px"
      }
    ],[]);

    const tableData = {
      columns,
      data: cultural_spaces
    };

  return (
    <>
      <div className="container-fluid bg-light p-2">
        <div className="card shadow card-body m-2">
          <div className="row justify-content-center">
          <DataTableExtensions filterPlaceholder="Buscar espacios culturales" export={false} print={false}
            {...tableData}
            >
            <DataTable
              noHeader
              title="Tabla de espacios culturales"
              columns={columns}
              customStyles={customTheme}
              data={cultural_spaces}
              defaultSortField="id"
              defaultSortDesc={true}
              pagination paginationComponentOptions={paginationComponentOptions}
              progressPending={pending}
              progressComponent={<CustomLoader />}
              noDataComponent="No se encuentran registros en la base de datos"
            />
            </DataTableExtensions>
          </div>
        </div>
      </div>
      <ModalView culturalSpace={selectedCulturalSpace} show={showModalView} onHide={()=>setShowModalView(false)}/>
      <ModalUpdate culturalSpace={selectedCulturalSpace} show={showModalUpdate} onHide={()=>setShowModalUpdate(false)}/>
    </>
  );
};

export default TableCulturalSpaces;