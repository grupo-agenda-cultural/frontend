export const partidos = [
    { id: 1, value: 1, label: 'José C. Paz' },
    { id: 2, value: 2, label: 'Malvinas Argentinas' },
    { id: 3, value: 3, label: ' San Miguel' }
]

export const localidades = [
    { id: 1, value: 1, label: 'Área de Promoción El Triángulo' },
    { id: 2, value: 2, label: 'Bella Vista' },
    { id: 3, value: 3, label: 'Campo de Mayo' },
    { id: 4, value: 4, label: 'Grand Bourg' },
    { id: 5, value: 5, label: 'Ingeniero Adolfo Sourdeaux' },
    { id: 6, value: 6, label: 'Ingeniero Pablo Nogués' },
    { id: 7, value: 7, label: 'José C. Paz' },
    { id: 8, value: 8, label: 'Los Polvorines' },
    { id: 9, value: 9, label: 'Muñiz' },
    { id: 10, value: 10, label: 'San Miguel' },
    { id: 11, value: 11, label: 'Santa María' },
    { id: 12, value: 12, label: 'Tierras Altas' },
    { id: 13, value: 13, label: 'Tortuguitas' },
    { id: 14, value: 14, label: 'Villa de Mayo' },
]

export const location = [
    {
        id: 1,
        value: 1,
        label: 'José C. Paz',
        localidades: [
            { id: 7, value: 7, label: 'José C. Paz' }
        ]
    },
    {
        id: 2,
        value: 2,
        label: 'Malvinas Argentinas',
        localidades: [
            { id: 1, value: 1, label: 'Área de Promoción El Triángulo' },
            { id: 4, value: 4, label: 'Grand Bourg' },
            { id: 5, value: 5, label: 'Ingeniero Adolfo Sourdeaux' },
            { id: 6, value: 6, label: 'Ingeniero Pablo Nogués' },
            { id: 8, value: 8, label: 'Los Polvorines' },
            { id: 12, value: 12, label: 'Tierras Altas' },
            { id: 13, value: 13, label: 'Tortuguitas' },
            { id: 14, value: 14, label: 'Villa de Mayo' }
        ]
    },
    {
        id: 3,
        value: 3,
        label: 'San Miguel',
        localidades: [
            { id: 2, value: 2, label: 'Bella Vista' },
            { id: 3, value: 3, label: 'Campo de Mayo' },
            { id: 9, value: 9, label: 'Muñiz' },
            { id: 10, value: 10, label: 'San Miguel' },
            { id: 11, value: 11, label: 'Santa María' }
        ]
    }
]