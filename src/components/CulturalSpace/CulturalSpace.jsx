import React from 'react';
import CulturalSpaceAdd from './Modals/CulturalSpaceAdd';
import CulturalSpaceUpdate from './Modals/CulturalSpaceUpdate';
import { connect } from 'react-redux';

const CulturalSpace = ({usuario, coordinador}) => {

  console.log(coordinador)
  return (
    <>

    {coordinador.cultural_space === '' ?
      <CulturalSpaceAdd />
    :
    <CulturalSpaceUpdate culturalSpace={coordinador.cultural_space}/>
    } 
    </>

  );
}

const mapStateToProps = (state) => ({
  usuario: state.authentication.user,
  coordinador: state.authentication.coordinador,
  users: state.users
});

export default connect(mapStateToProps)(CulturalSpace);
