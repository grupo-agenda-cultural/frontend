import React from 'react';
import { localidades, partidos } from '../data';
import { Modal, Container, Row, Col, Button } from 'react-bootstrap';
import CONFIG from '../../../configuration/configuration';

const ModalView = ({ culturalSpace, show, onHide }) => {

  return (
    <Modal
      show={show}
      size='lg'
      onHide={onHide}
      backdrop="static"
      keyboard={false}
      aria-labelledby="modalViewCulturalSpace"
      centered
      scrollable
    >
      <Modal.Header closeButton style={{ background: 'gray', padding: "20px" }}>
        <Modal.Title style={{ color: 'white' }}>
          Visualizar Espacio cultural
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Container>
          <Row>
            <Col md={6}>
              <h5>Nombre del espacio cultural</h5>
              <p>{culturalSpace.name}</p>
              <h5>Dirección</h5>
              <p>{culturalSpace.address}</p>
              <h5>Partido</h5>
              <p>{partidos.map(option => option.value === culturalSpace.partido ? option.label : '')}</p>
              <h5>Localidad</h5>
              <p>{localidades.map(option => option.value === culturalSpace.localidad ? option.label : '')}</p>
              <h5>Teléfono</h5>
              <p>{culturalSpace.phone}</p>
              <h5>Email</h5>
              <p>{culturalSpace.email}</p>
              <h5>Redes sociales</h5>
              {culturalSpace.facebook ? <a href={culturalSpace.facebook} target="_blank" rel="noopener noreferrer"><i className="bi bi-facebook p-1" style={{ fontSize: "30px", color: "#3f729b" }}></i></a> : ''}
              {culturalSpace.instagram ? <a href={culturalSpace.instagram} target="_blank" rel="noopener noreferrer"><i className="bi bi-instagram" style={{ fontSize: "30px", color: " #f54056" }}></i></a> : ''}

            </Col>
            <Col md={6}>
              <Container>
                <img width="100%" style={{ height: "20em" }} alt={culturalSpace.name} src={CONFIG.BASE_URL + culturalSpace.image} />
              </Container>
            </Col>
          </Row>
        </Container>

      </Modal.Body>
      <Modal.Footer>
        <Button type="button" variant="secondary" onClick={onHide}>Cerrar</Button>
      </Modal.Footer>
    </Modal>

  );
}

export default ModalView;