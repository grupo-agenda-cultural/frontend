import React from 'react';
import { useDispatch } from 'react-redux';
import { location } from "../data";
import * as Yup from 'yup'
import { Formik, ErrorMessage } from "formik";
import { Container, Form, Row, Col , Button} from 'react-bootstrap';
import { updateCulturalSpace } from "../../../actions/culturalSpaceAction";
import CONFIG from '../../../configuration/configuration';

const CulturalSpaceUpdate = ({culturalSpace}) => {

  const dispatch = useDispatch();

  const handleSubmit = (values) => {
    dispatch(updateCulturalSpace(values));
  }
  
   
  const isValidUrl = (url) => {
    try {
        new URL(url);
    } catch (e) {
        return false;
    }
    return true;
};
  // Validaciones
  const ErrorMessagesSchema = Yup.object().shape({
    image: Yup.mixed().required("Debe subir la imagen del espacio cultural"), 
    name: Yup.string().required('Campo requerido'),
    email: Yup.string().email('Email inválido').required('Campo requerido'),
    phone: Yup.string()
      .matches(/^[0-9]+$/, {
        message: "Teléfono inválido",
        excludeEmptyString: false
      })
      .required('Campo requerido'),
      address: Yup.string().required('Campo requerido'),
     partido: Yup.string().required("Debe seleccionar una opción"),
     localidad: Yup.string().required("Debe seleccionar una opción"),
    latitude: Yup.string()
    .required('Campo requerido'),
    longitude: Yup.string()
      .required('Campo requerido'),
      facebook: Yup.string().when( (val, schema) => {
        if (val) {  
           if(val.length > 0) { 
             return Yup.string().test('is-url-valid', 'Debe ingresar una URL válida', (value) => isValidUrl(value));
          } else { 
             return Yup.string().notRequired().nullable();
          }
   
        } else {
             return Yup.string().notRequired().nullable();
        }
     }),
     instagram: Yup.string().when( (val, schema) => {
      if (val) {  
         if(val.length > 0) { 
           return Yup.string().test('is-url-valid', 'Debe ingresar una URL válida', (value) => isValidUrl(value));
        } else { 
           return Yup.string().notRequired().nullable();
        }
 
      } else {
           return Yup.string().notRequired().nullable();
      }
   }),
  });

  return (
    <>
      <div className="container bg-white p-5 border rounded ">
          <Formik
            enableReinitialize
            initialValues={{
              id: culturalSpace.id,
              image: culturalSpace.image,
              name: culturalSpace.name,
              email: culturalSpace.email,
              phone: culturalSpace.phone,
              address: culturalSpace.address,
              partido: culturalSpace.partido,
              localidad: culturalSpace.localidad,
              latitude: culturalSpace.latitude,
              longitude: culturalSpace.longitude,
              facebook: culturalSpace.facebook,
              instagram: culturalSpace.instagram
            }}
            onSubmit={(values) => {
              handleSubmit(values);
            }}
            validationSchema={ErrorMessagesSchema}
          >
            {({ errors, touched, handleSubmit, handleBlur, handleChange, values, setFieldValue }) => (
              <Form onSubmit={handleSubmit}>
            <Container>
              <Row>
              <Col xl={5}>
              <Form.Group>                 
               <Form.Label>Imagen del espacio cultural</Form.Label>
                  <div className="card bg-light mb-2 my-2" style={{  minWidth: "20em" ,maxWidth: "24em" }}>
                  <img src={CONFIG.BASE_URL +  values.image} style={{ width: '20em' }} alt={values.image} />

                  <input type="file" accept="image/*" onClick={e => (e.target.value = null)} onChange={event => setFieldValue("image",event.target.files[0])}  name="image" className="form-control-file" id="production-img" />
                      </div>
                      {errors.image ? <div style={{ witch:"100%", marginTop: "0.2rem", fontSize: ".875em",color: "#dc3545"}}><ErrorMessage name="image"/></div> : null}
                    
                    </Form.Group>
                    </Col> 
                    </Row>
                    <Row>
                  <Col lg={6} md={12} sm={12} className="p-2">
                    <Form.Group>

                      <Form.Label>Nombre del espacio cultural</Form.Label>
                      <Form.Control
                        type="text"
                        name="name"
                        placeholder="Ingresá el nombre del espacio cultural"
                        value={values.name}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        isInvalid={touched.name && errors.name}
                      />
                      <Form.Control.Feedback type="invalid">
                        {errors.name}
                      </Form.Control.Feedback>
                    </Form.Group>
                  </Col>
                  </Row>
                  <Row>
                  <Col lg={6} md={12} sm={12} className="p-2">
                <Form.Label>Email</Form.Label>
                <Form.Group>
                <Form.Control                  
                    type="email"
                    name="email"
                    value={values.email}
                        onBlur={handleBlur}
                        onChange={handleChange}
                    placeholder="Ingresá el email de contacto"
                    isInvalid={touched.email && errors.email}/>

                  <Form.Control.Feedback type="invalid">
                        {errors.email}
                      </Form.Control.Feedback>
                    </Form.Group>
                  </Col>
                
                  <Col lg={6} md={12} sm={12} className="p-2">
                  <Form.Label>Teléfono</Form.Label>
                  <Form.Group>
                  <Form.Control
                    name="phone"
                    className="form-control"
                    placeholder="Ingresá el teléfono" 
                    value={values.phone}
                        onBlur={handleBlur}
                        onChange={handleChange}
                    isInvalid={touched.phone && errors.phone}/>
                <Form.Control.Feedback type="invalid">
                        {errors.phone}
                      </Form.Control.Feedback>
                    </Form.Group>
                  </Col>
              </Row>
              <Row>
              <Col lg={4} md={12} sm={12} className="p-2">
                  <Form.Label>Dirección</Form.Label>
                  <Form.Group>
                  <Form.Control
                  
                      type="text"
                      name="address"
                      className="form-control"
                      id="direccion-espacio-cultural"
                      placeholder="Ingresá la dirección"
                      value={values.address}
                        onBlur={handleBlur}
                        onChange={handleChange} 
                      isInvalid={touched.address && errors.address}/>
                  <Form.Control.Feedback type="invalid">
                        {errors.address}
                      </Form.Control.Feedback>
                    </Form.Group>
                  </Col>
                  <Col md={4} className="p-2">
                  <Form.Group>
                    <Form.Label>Partido</Form.Label>
                    <Form.Select
                      type="select"
                      as="select"
                      name="partido"
                      value={values.partido}
                      aria-label=".form-select-sm"
                      onChange={handleChange}
                      isInvalid={touched.partido && errors.partido}>
                      <option selected hidden>Seleccioná el partido</option>
                      {location.map(elem => (
                        <option key={elem.id} value={elem.value}>{elem.label}</option>
                      ))}
                    </Form.Select>
                    <Form.Control.Feedback type="invalid">
                      {errors.partido}
                    </Form.Control.Feedback>
                  </Form.Group>
                </Col>
                {values.partido ?
                  <Col className="p-2">
                  <Form.Group>
                    <Form.Label>Localidad</Form.Label>
                    <Form.Select
                      type="select"
                      as="select"
                      name="localidad"
                      value={values.localidad}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      isInvalid={touched.localidad && errors.localidad}>
                      {/* <option selected hidden>Seleccioná la localidad</option> */}
                      {location[values.partido-1].localidades.map(elem => (
                        <option key={elem.id} value={elem.value}>{elem.label}</option>
                      ))}
                    </Form.Select>
                    <Form.Control.Feedback type="invalid">
                      {errors.localidad}
                    </Form.Control.Feedback>
                  </Form.Group>
                </Col>                 
                : ''}
</Row>
              <Row className="p-1">
                <p className="text-secondary">
                  (Para ingresar la latitud y longitud, ingresa a <a href="https://www.google.com.ar/maps">Google Maps</a> y seguí las instrucciones del Manual de Uso)</p>
              </Row>
              <Row>
                <Col md={6} className="p-2">
                  <Form.Group>
                    <Form.Label>Latitud</Form.Label>
                    <Form.Control
                      type="text"
                      name="latitude"
                      placeholder="Ingresá la latitud del espacio cultural"
                      value={values.latitude}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      isInvalid={touched.latitude && errors.latitude}
                    />
                    <Form.Control.Feedback type="invalid">
                      {errors.latitude}
                    </Form.Control.Feedback>
                  </Form.Group>
                </Col>
                <Col md={6} className="p-2">
                  <Form.Group>
                    <Form.Label>Longitud</Form.Label>
                    <Form.Control
                      type="text"
                      name="longitude"
                      placeholder="Ingresá la longitud del espacio cultural"
                      value={values.longitude}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      isInvalid={touched.longitude && errors.longitude}
                    />
                    <Form.Control.Feedback type="invalid">
                      {errors.longitude}
                    </Form.Control.Feedback>
                  </Form.Group>
                </Col>
              </Row>
              <Row>
              <Col lg={6} md={12} sm={12} className="p-2">
                  <Form.Group>
                    <Form.Label>Facebook</Form.Label>
                    <Form.Control
                      type="text"
                      name="facebook"
                      placeholder="Ingresá el link de Facebook"
                      value={values.facebook || ""}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      isInvalid={touched.facebook && errors.facebook}
                    />
                    <Form.Control.Feedback type="invalid">
                      {errors.facebook}
                    </Form.Control.Feedback>
                  </Form.Group>
                </Col>
                <Col lg={6} md={12} sm={12} className="p-2">
                  <Form.Group>
                    <Form.Label>Instagram</Form.Label>
                    <Form.Control
                      type="text"
                      name="instagram"
                      placeholder="Ingresá el link de Instagram"
                      value={values.instagram || ""}
                      onChange={handleChange}
                      isInvalid={touched.instagram && errors.instagram}
                    />
                    <Form.Control.Feedback type="invalid">
                      {errors.instagram}
                    </Form.Control.Feedback>
                  </Form.Group>
                </Col>
              </Row>
             
          

          <Row className="m-2 justify-content-center">
          <Col lg={6} md={12} sm={12} className="p-2">
              <div className="btn-group d-flex" role="group">
              <Button type="submit" variant="primary">Editar espacio cultural</Button>
              </div>
            </Col>
            </Row>

            </Container>

          </Form>
    
              )}
        </Formik>
        </div>
    </>
  );
};



// const mapStateToProps = (state) => ({
//   usuario: state.authentication.user,
//   users: state.users
// });
export default CulturalSpaceUpdate;
//export default connect(mapStateToProps)(CulturalSpaceAdd);
