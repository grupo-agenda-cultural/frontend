import React from "react";
import { useDispatch } from 'react-redux';
import { location } from "../data";
import { updateCulturalSpace } from "../../../actions/culturalSpaceAction";
import * as Yup from 'yup'
import { Modal, Form, Row, FormGroup, Col, FormLabel, Button, FormSelect } from "react-bootstrap";
import { Formik, ErrorMessage } from "formik";
import CONFIG from "../../../configuration/configuration";
const ModalUpdate = ({ culturalSpace, show, onHide }) => {

  const dispatch = useDispatch();

  const handleSubmit = (values) => {
    dispatch(updateCulturalSpace(values));
  }



  const isValidUrl = (url) => {
    try {
        new URL(url);
    } catch (e) {
        return false;
    }
    return true;
};
  // Validaciones
  const ErrorMessagesSchema = Yup.object().shape({
    image: Yup.mixed().required("Debe subir la imagen del espacio cultural"), 
    name: Yup.string().required('Campo requerido'),
    email: Yup.string().email('Email inválido').required('Campo requerido'),
    phone: Yup.string()
      .matches(/^[0-9]+$/, {
        message: "Teléfono inválido",
        excludeEmptyString: false
      })
      .required('Campo requerido'),
      address: Yup.string().required('Campo requerido'),
     partido: Yup.string().required("Debe seleccionar una opción"),
     localidad: Yup.string().required("Debe seleccionar una opción"),
    latitude: Yup.string()
    .required('Campo requerido'),
    longitude: Yup.string()
      .required('Campo requerido'),
      facebook: Yup.string().when( (val, schema) => {
        if (val) {  
           if(val.length > 0) { 
             return Yup.string().test('is-url-valid', 'Debe ingresar una URL válida', (value) => isValidUrl(value));
          } else { 
             return Yup.string().notRequired();
          }
   
        } else {
             return Yup.string().notRequired();
        }
     }),
     instagram: Yup.string().when( (val, schema) => {
      if (val) {  
         if(val.length > 0) { 
           return Yup.string().test('is-url-valid', 'Debe ingresar una URL válida', (value) => isValidUrl(value));
        } else { 
           return Yup.string().notRequired();
        }
 
      } else {
           return Yup.string().notRequired();
      }
   }),
  });

  return (
    <Modal show={show}
      size='xl'
      onHide={onHide}
      backdrop="static"
      keyboard={false}
      aria-labelledby="modalUpdateCulturalSpace"
      centered
      scrollable
    >
      <Modal.Header closeButton style={{ background: "#0275d8", padding: "20px" }} >
        <Modal.Title style={{ color: "white" }}>
          Editar Espacio cultural
        </Modal.Title>
      </Modal.Header>
      <Formik
        enableReinitialize
        initialValues={{
          id: culturalSpace.id,
          image: culturalSpace.image,
          name: culturalSpace.name,
          address: culturalSpace.address,
          partido: culturalSpace.partido,
          localidad: culturalSpace.localidad,
          latitude: culturalSpace.latitude,
          longitude: culturalSpace.longitude,
          phone: culturalSpace.phone,
          email: culturalSpace.email,
          facebook: culturalSpace.facebook,
          instagram: culturalSpace.instagram,
        }}
        onSubmit={(values) => {
          handleSubmit(values);
        }}
        validationSchema={ErrorMessagesSchema}
      >
        {({ errors, touched, handleSubmit, handleChange, handleBlur, values, setFieldValue, isValid, isSubmitting }) => (

          <Modal.Body>
            <Form onSubmit={handleSubmit} id="form">
              <Row className="m-2">
                <FormGroup md={10}>
                  <Form.Label>Imagen del espacio cultural</Form.Label>
                  <div className="card bg-light mb-3 my-2" style={{ width: "22em" }}>
                    <img src={CONFIG.BASE_URL + values.image} style={{ width: "10em" }} alt={values.image} />
                    <input type="file"
                      accept="image/*"
                      //onClick={e => (e.target.value = null)} 
                      onChange={event => setFieldValue("image", event.target.files[0])}
                      name="image"
                      className="form-control-file"
                      id="production-img" />
                  </div>
                  {errors.image ? <div style={{ witch: "100%", marginTop: "0.2rem", fontSize: ".875em", color: "#dc3545" }}><ErrorMessage name="image" /></div> : null}
                </FormGroup>
              </Row>
              <Row className="m-2" >
                <Col md={12} className="p-1">
                  <FormGroup>
                    <Form.Label>Nombre del espacio cultural</Form.Label>
                    <Form.Control
                      type="text"
                      name="name"
                      placeholder="Ingresá el nombre del espacio cultural"
                      value={values.name}
                      onChange={handleChange}
                      isInvalid={!!errors.name}
                    />
                    <Form.Control.Feedback type="invalid">
                      {errors.name}
                    </Form.Control.Feedback>
                  </FormGroup>
                </Col>
              </Row>
              <Row className="m-2">
                <Col md={6} className="p-1">
                  <FormGroup md={6}>
                    <Form.Label>Email</Form.Label>
                    <Form.Control
                      type="email"
                      name="email"
                      placeholder="Ingresá el email del espacio cultural"
                      value={values.email}
                      onChange={handleChange}
                      isInvalid={!!errors.email}
                    />
                    <Form.Control.Feedback type="invalid">
                      {errors.email}
                    </Form.Control.Feedback>
                  </FormGroup>
                </Col>

                <Col md={6} className="p-1">
                  <FormGroup md={6}>
                    <Form.Label>Teléfono</Form.Label>
                    <Form.Control
                      type="text"
                      name="phone"
                      placeholder="Ingresá el teléfono del espacio cultural"
                      value={values.phone}
                      onChange={handleChange}
                      isInvalid={!!errors.phone}
                    />
                    <Form.Control.Feedback type="invalid">
                      {errors.phone}
                    </Form.Control.Feedback>
                  </FormGroup>
                </Col>
              </Row>

              <Row className="m-2">
                <Col md={4} className="p-1">
                  <FormGroup>
                    <FormLabel>Dirección</FormLabel>
                    <Form.Control
                      type="text"
                      name="address"
                      placeholder="Ingresá la dirección del espacio cultural"
                      value={values.address}
                      onChange={handleChange}
                      isInvalid={!!errors.address}
                    />
                    <Form.Control.Feedback type="invalid">
                      {errors.address}
                    </Form.Control.Feedback>
                  </FormGroup>
                </Col>
                <Col md={4} className="p-1">
                  <FormGroup>
                    <FormLabel>Partido</FormLabel>
                    <Form.Select
                      type="select"
                      as="select"
                      name="partido"
                      value={values.partido}
                      aria-label=".form-select-sm"
                      onChange={handleChange}
                      isInvalid={touched.partido && errors.partido}>
                      <option selected hidden>Seleccioná el partido</option>
                      {location.map(elem => (
                        <option key={elem.id} value={elem.value}>{elem.label}</option>
                      ))}
                    </Form.Select>
                    <Form.Control.Feedback type="invalid">
                      {errors.partido}
                    </Form.Control.Feedback>
                  </FormGroup>
                </Col>
                <Col className="p-1">
                  <FormGroup>
                    <FormLabel>Localidad</FormLabel>
                    <FormSelect
                      type="select"
                      name="localidad"
                      value={values.localidad}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      aria-label=".form-select-sm"
                      isInvalid={touched.localidad && errors.localidad}>
                      {/* <option selected hidden>Seleccioná la localidad</option> */}
                      {location[values.partido - 1].localidades.map(elem => (
                        <option key={elem.id} value={elem.value}>{elem.label}</option>
                      ))}
                    </FormSelect>
                    <Form.Control.Feedback type="invalid">
                      {errors.localidad}
                    </Form.Control.Feedback>
                  </FormGroup>
                </Col>                 </Row>
              <Row className="p-1">
                <p className="text-secondary">
                  (Para editar la latitud y longitud, ingresa a <a href="https://www.google.com.ar/maps">Google Maps</a> y seguí las instrucciones del Manual de Uso)</p>
              </Row>
              <Row className="m-2">
                <Col md={6} className="p-1">
                  <FormGroup>
                    <Form.Label>Latitud</Form.Label>
                    <Form.Control
                      type="text"
                      name="latitude"
                      placeholder="Ingresá la latitud del espacio cultural"
                      value={values.latitude}
                      onChange={handleChange}
                      isInvalid={!!errors.latitude}
                    />
                    <Form.Control.Feedback type="invalid">
                      {errors.latitude}
                    </Form.Control.Feedback>
                  </FormGroup>
                </Col>
                <Col md={6} className="p-1">
                  <FormGroup>
                    <Form.Label>Longitud</Form.Label>
                    <Form.Control
                      type="text"
                      name="longitude"
                      placeholder="Ingresá la longitud del espacio cultural"
                      value={values.longitude}
                      onChange={handleChange}
                      isInvalid={!!errors.longitude}
                    />
                    <Form.Control.Feedback type="invalid">
                      {errors.longitude}
                    </Form.Control.Feedback>
                  </FormGroup>
                </Col>
              </Row>
              <Row className="m-2">
                <Col md={6} className="p-1">
                  <FormGroup>
                    <Form.Label>Facebook</Form.Label>
                    <Form.Control
                      type="text"
                      name="facebook"
                      placeholder="Ingresá el link de Facebook"
                      value={values.facebook}
                      onChange={handleChange}
                      isInvalid={!!errors.facebook}
                    />
                    <Form.Control.Feedback type="invalid">
                      {errors.facebook}
                    </Form.Control.Feedback>
                  </FormGroup>
                </Col>
                <Col md={6} className="pt-1">
                  <FormGroup>
                    <Form.Label>Instagram</Form.Label>
                    <Form.Control
                      type="text"
                      name="instagram"
                      placeholder="Ingresá el link de Instagram"
                      value={values.instagram}
                      onChange={handleChange}
                      isInvalid={!!errors.instagram}
                    />
                    <Form.Control.Feedback type="invalid">
                      {errors.instagram}
                    </Form.Control.Feedback>
                  </FormGroup>
                </Col>
              </Row>

              <Modal.Footer>
                <Button
                  type="submit"
                  variant="primary"
                  onClick={isValid ? onHide : ''}>
                  Enviar
                </Button>
              </Modal.Footer>
            </Form>
          </Modal.Body>)}
      </Formik>

    </Modal>
  );
};

export default ModalUpdate;