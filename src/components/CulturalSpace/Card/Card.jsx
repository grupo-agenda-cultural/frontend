import React from 'react';
import { useDispatch} from 'react-redux';
import { localidades, partidos } from "../data";
import { Link } from 'react-router-dom';
import { Formik, ErrorMessage } from "formik";
import * as Yup from 'yup'
import { addSubscription } from "../../../actions/subscriptionAction";
import './Card.css';
import CONFIG from '../../../configuration/configuration';
import { Button, Form, InputGroup } from 'react-bootstrap';

const Card = ({ item }) => {

  const dispatch = useDispatch();

  // Validaciones
  const ErrorMessagesSchema = Yup.object().shape({
    email: Yup.string().email('Email inválido').required('Debe ingresar su email')
  });

  const handleSubmit = (values) => {
    dispatch(addSubscription(values))
  }
  
  return (
    <>
      <div className="card-cultural-space">
        <div className="imagen-cultural-space">
          <img src={CONFIG.BASE_URL + item.image} className="img-cultural-space"
            alt="img espacio cultural"
          />
        </div>
        <div className="body-cultural-space">
            <div className="left-card">
              <h2 className="cultural-space-name"> {item.name}</h2>
              <p><b>Dirección:</b> {item.address}   <a href={`https://maps.google.com/?q=${item.latitude},${item.longitude}`} target="_blank" rel="noreferrer">¿Cómo llegar?</a></p>
              <p><b>Partido:</b> {partidos.map(option => option.value === item.partido ? option.label : "")}</p>
              <p><b>Localidad:</b> {localidades.map(option => option.value === item.localidad ? option.label : "")}</p>
              <p><b>Email:</b> {item.email}</p>
              <p><b>Teléfono:</b> {item.phone}</p>
              <div className="social-networks">
                {item.instagram ? <p><a href={item.instagram} target="_blank" rel="noreferrer"><i className="bi bi-instagram" style={{ fontSize: "25px", color: " #f54056" }}></i></a> &nbsp;</p> : ''}
                {item.facebook ? <p><a href={item.facebook} target="_blank" rel="noreferrer"><i className="bi bi-facebook" style={{ fontSize: "25px", color: "#3f729b" }}></i></a></p> : ''}
              </div>

              <Formik
            initialValues={{
              id: item.id,
              email:'',
            }}
            onSubmit={(values,{resetForm}) => {
              handleSubmit(values);
              resetForm();
            }}
            validationSchema={ErrorMessagesSchema}
          >
            {({ errors, touched, handleSubmit, handleChange, handleBlur, values, setFieldValue }) => (
              <Form>
             <div className="input-suscription">
         <Form.Group >       
          <InputGroup>
                    <Form.Control
                  type="text"
                  name="email"
                  placeholder="Ingresá tu correo electrónico"
                  value={values.email} 
                  onChange={handleChange}
                  onBlur={handleBlur}
                  isInvalid={touched.email && errors.email}/>
               <Button type="submit" onClick={handleSubmit} variant="primary">Suscribirme</Button>

</InputGroup>
              {errors.email && touched.email ? <div style={{ witch:"100%", fontSize: ".875em", color: "#dc3545"}}><ErrorMessage name="email"/></div> : null}

              </Form.Group>   
              
</div>
      </Form>
              )}
             

        </Formik>
        </div>
            <div className="right-card">
            <Link to={`/agenda/espacio-cultural/${item.id}`} style={{ textDecoration: "none" }}><button className="btn btn-primary" style={{ fontSize: "16px", padding:"14px", width:"100%"}} type="button" id="button-addon2">Ver producciones</button></Link>
            <Link to={`/formacion/espacio-cultural/${item.id}`} style={{ textDecoration: "none" }}><button className="btn btn-success" style={{ fontSize: "16px", marginTop: "20px", padding:"14px",width:"100%"}} type="button" id="button-addon2">Ver talleres</button></Link>
            </div>
        </div>
      </div> 
    </>
  );
};

export default Card;