import React from 'react';
import Select from 'react-select';
import DatePicker, { registerLocale } from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { modalities, costs, categories, languages } from './data';
import 'react-datepicker/dist/react-datepicker.css';
import es from "date-fns/locale/es"
import './FilterCalendar.css';

const FilterCalendarCulturalSpace = ({
    startDate,
    endDate,
    setDateRange,
    selectedLanguage,
    selectLanguage,
    selectedCategory,
    selectCategory,
    selectedModality,
    selectModality,
    selectedCost,
    selectCost
}) => {
    
    registerLocale('es', es);
    
    return(
      <>
        <div className="filters">
            <h1 className="name-filters">Filtrar por</h1>
            <div className="filter-item">
              <h3 className="filter-name-item">Fechas</h3>
              <DatePicker
                selectsRange={true}                
                startDate={startDate}
                endDate={endDate}
                minDate={new Date()}
                locale="es"
                dateFormat="dd/MM/yyyy"
                placeholderText="Por fechas"
                onChange={setDateRange}
                isClearable={true}
              /> </div>
            <div className="filter-item">
              <h3 className="filter-name-item">Actividades</h3>
              <Select
                options={languages}
                value={selectedLanguage}
                menuColor='red'
                placeholder="Por tipo de actividad"
                isMulti
                noOptionsMessage={() => "No hay más opciones"}
                onChange={selectLanguage}/>
            </div>
            <div className="filter-item">
              <h3 className="filter-name-item">Categorías</h3>
              <Select
                options={categories}
                value={selectedCategory}
                menuColor='red'
                placeholder="Por categoría"
                isMulti
                noOptionsMessage={() => "No hay más opciones"} 
                onChange={selectCategory}
                />
            </div>
            <div className="filter-item">
              <h3 className="filter-name-item">Costos</h3>
              <Select
                options={costs}
                value={selectedCost}
                menuColor='red'
                placeholder="Por costo"
                isMulti
                name="cost"
                noOptionsMessage={() => "No values available"}
                onChange={selectCost}
              />
            </div>
            <div className="filter-item">
              <h3 className="filter-name-item">Modalidades</h3>
              <Select
                options={modalities}
                menuColor='red'
                placeholder="Por modalidad"
                isMulti
                value={selectedModality}
                noOptionsMessage={() => "No hay más opciones"}
                onChange={selectModality}
                isSearchable={true}
                />
            </div>
           
          </div> 
          </>
    )
}

export default FilterCalendarCulturalSpace;