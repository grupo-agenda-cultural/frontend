import React, { useState } from 'react';
import { useDispatch, connect } from 'react-redux';
import {  useNavigate, Link } from 'react-router-dom';
import { login } from '../../actions/authentication';
import logo from '../../assets/images/logo/logo.png';
import './Login.css';
import { Formik, Form } from "formik";
import * as Yup from 'yup'
import { FormGroup, FormLabel, FormControl, Row, InputGroup } from 'react-bootstrap';

const LoginForm = ({ isAuthenticated, usuario, slide }) => {

  const [shown, setShown] = useState(false);
  const switchShown = () => setShown(!shown);
  const [loginError, setLoginError] = useState("");
  
  const history = useNavigate();

  const dispatch = useDispatch();

  const handleSubmit = (values) => {
    dispatch(login(values.email, values.password));
    
  }
    
  
  if (isAuthenticated && usuario != null) {
    const { groups } = usuario;
    if (groups[0].id === 1) {
      history('/coordinador/inicio');
    } else if (groups[0].id === 2) {
      history('/administrador/inicio');    
    }else {
      setLoginError("Error al iniciar sesión")
    }
  }
  const ErrorMessagesSchema = Yup.object().shape({
    email: Yup.string().email('Email inválido').required('Campo requerido'),
    password: Yup.string().required("Campo requerido").min(8, "Debe ingresar una contraseña con un mínimo de 8 caracteres")
  });


  return (
    <>
      <div className="form-container sign-in-container">
        <div className="logo">
          <img src={logo} alt="logo" />
        </div>
        <Formik
          initialValues={{ email: '', password: '' }}
          validationSchema={ErrorMessagesSchema}
          onSubmit={(values, { resetForm }) => {
            handleSubmit(values);
          }}>
          {({ errors, touched, values, handleChange, handleBlur }) => (
            <Form className="formulario">
              <div className="heading">
                <h2> Iniciar sesión</h2>
                <h6>¿No tenés una cuenta? </h6>
                <a href="#/" className="toggle" id="signUp"
                  onClick={() => slide("right-panel-active")}
                >Regístrate</a>
              </div>
              <Row>
                <div className="col-md-12 form-group mt-4 mb-3 p-2">
                <FormLabel>Email</FormLabel>
                <FormGroup>
                  <FormControl
                    type="email"
                    name="email"
                    value={values.email}
                    className="form-control"
                    id="email"
                    aria-describedby="email"
                    placeholder="Ingresá tu email"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    isInvalid={touched.email && errors.email}
              />
                  <FormControl.Feedback type="invalid">
                      {errors.email}
                    </FormControl.Feedback>
                    </FormGroup>

                {/* <FormFeedback tooltip invalid>{errors.email}</FormFeedback> */}
                </div>
                </Row>
              <Row>
                <div className="col-md-12 form-group mb-4 p-2">
                <FormLabel>Contraseña</FormLabel>
                <FormGroup >       
          <InputGroup>
                    <FormControl
                  type={shown ? 'text' : 'password'}
                  name="password"
                  className="form-control" 
                  placeholder="Ingresá tu contraseña" 
                  value={values.password}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  isInvalid={touched.password && errors.password}
/>
<span className="input-group-text" onClick={switchShown} id="password"> {shown ? <i className="bi bi-eye-slash-fill"></i> : <i className="bi bi-eye-fill"></i>}</span>

              <FormControl.Feedback type="invalid">
                      {errors.password}
                    </FormControl.Feedback>
                    </InputGroup>
                    
              </FormGroup>   
                </div>
                </Row>
              <input type="submit" value="Iniciar sesión" className="sign-btn" />
              {touched.password && touched.email && loginError && ( 
                <FormControl.Feedback>{loginError}</FormControl.Feedback>
                    )
                    } 
              <div className="text">
                <Link to="/reset-password">¿Olvidaste tu contraseña?</Link>
                <p className="copyright">&copy;2022 Agenda y Comunicación Cultural y Artística</p>
              </div>
            </Form>
          )}
        </Formik>
      </div>
    </>
  )
}

// Get Data from redux store
const mapStateToProps = (state) => ({
  isAuthenticated: state.authentication.isAuthenticated,
  usuario: state.authentication.user
});

export default connect(mapStateToProps)(LoginForm);