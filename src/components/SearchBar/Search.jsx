import React from 'react';
import './Search.css';

const SearchBar = ({ value, changeInput, placeholder }) => (
  <div className="container-search">
    <div className="container-animation"></div>

<div className="search-box">
  <input className="search-input" value={value} onChange={changeInput} type="text" name="" placeholder={placeholder}/>
    <i className="bi bi-search search-btn"></i>
    
</div>

</div>
);

export default SearchBar;