import React, { useState, useEffect, useMemo } from 'react';
import { useDispatch, useSelector, connect } from 'react-redux';
import { Button } from '../../Button/Button';
import ModalView from '../Modals/ModalView';
import ModalDelete from '../Modals/ModalDelete';
import ModalUpdate from '../Modals/ModalUpdate';
import { getProductionsByMyCulturalSpace } from '../../../actions/productionAction';
import DataTable from 'react-data-table-component';
import DataTableExtensions from 'react-data-table-component-extensions';
import 'react-data-table-component-extensions/dist/index.css';
import Spinner from "../../Spinner/Spinner";
import { costs, languages, modalities } from '../data';
import moment from 'moment';
import 'moment/locale/es';

const TableProductionsCoordinador = ({ usuario }) => {

  const dispatch = useDispatch();

  const productions = useSelector(state => state.productions.productions);
  const [productionChoose, setProductionChoose] = useState('');
  const [pending, setPending] = useState(true);
  const [showModalView, setShowModalView] = useState(false);
  const [showModalUpdate, setShowModalUpdate] = useState(false);
  const [showModalDelete, setShowModalDelete] = useState(false);

  useEffect(() => {
    dispatch(getProductionsByMyCulturalSpace())
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    const timeout = setTimeout(() => {
      const loadProductions = () => {
        dispatch(getProductionsByMyCulturalSpace());
      }; 
      loadProductions();
      setPending(false);
    }, 1800);
    
    return () => clearTimeout(timeout);
          // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);


  const setRowProductionView = (production) => {
    setProductionChoose(production);
    setShowModalView(true);
  }

  const setRowProductionUpdate = (production) => {
    setProductionChoose(production);
    setShowModalUpdate(true);
  }

  const setRowProductionDelete = (production) => {
    setProductionChoose(production);
    setShowModalDelete(true);
  }
  const CustomLoader = () => (
    <div className="loading">
      <Spinner />
      <h5 className="text-center" style={{ color: "rgb(26,53,64,25)" }}>Cargando...</h5>
    </div>
  );

  const columns = useMemo(
    () => [
      {
        name: "Nombre",
        selector: row => row.name,
        sortable: true,
        wrap: true
      },
      {
        name: "Inicio",
        selector: row => row.date_start ? moment(row.date_start).local().format('DD/MM/YYYY HH:mm') + " hs" : '',
        sortable: true,
        wrap: true,
      },
      {
        name: "Finalización",
        selector: row => row.date_end ? moment(row.date_end).local().format('DD/MM/YYYY HH:mm') + " hs" : '-',
        sortable: true,
        wrap: true
      },
      {
        name: "Organizador",
        selector: row => row.organizer_person,
        sortable: true,
        wrap: true
      }
      , {
        name: "Lenguaje",
        selector: row => languages.map(option => (option.value === row.language ? option.label : "")),
        sortable: true,
        wrap: true,
      },
      {
        name: "Modalidad",
        selector: row => modalities.map(option => (option.value === row.modality ? option.label : "")),
        sortable: true,
        wrap: true
      },
      {
        name: "Costo",
        selector: row => costs.map(option => (option.value === row.cost ? option.label : "")),
        sortable: true,
        wrap: true
      },
      {
        name: "Precio",
        selector: row => `$${row.price}`,
        sortable: true,
        wrap: true
      },
      {
        cell: (row) => <Button onClick={() => setRowProductionView(row)} type="button" className="btn btn-secondary" icon="bi bi-eye-fill"></Button>,
        ignoreRowClick: true,
        allowOverflow: true,
        button: true,
        width: "50px"
      },
      {
        cell: (row) => <Button onClick={() => setRowProductionUpdate(row)} type="button" className="btn btn-primary" icon="bi bi-pencil-square"></Button>,
        ignoreRowClick: true,
        allowOverflow: true,
        button: true,
        width: "50px"
      },
      {
        cell: (row) => <Button onClick={() => setRowProductionDelete(row)} type="button" className="btn btn-danger" icon="bi bi-trash-fill"></Button>,
        ignoreRowClick: true,
        allowOverflow: true,
        button: true,
        width: "50px"
      },
    ],
    [],
  );

  const paginationComponentOptions = {
    rowsPerPageText: 'Filas por página',
    rangeSeparatorText: 'de',
    selectAllRowsItem: true,
    selectAllRowsItemText: 'Todos',
  };

  const tableData = {
    columns,
    data: productions
  };

  const customTheme = {
    rows: {
      style: {
          fontSize:'14px',
          padding:'10px 0',
          justifyContent:'center'
      },
  },
  headCells: {
      style: {
          justifyContent: 'left',
          fontSize:'14px',
      },
  },
  cells: {
    style: {
        justifyContent: 'left'
    },
},
};

  return (
    <>
      <div className="container-fluid bg-light p-2 min-vh-100">
        <div className="card shadow card-body m-2">
          <div className="row justify-content-center">
            <DataTableExtensions filterPlaceholder="Buscar producciones" export={false} print={false}
              {...tableData}
            >
              <DataTable
                noHeader
                title="Tabla de producciones"
                columns={columns}
                customStyles={customTheme}
                data={productions}
                center
                defaultSortField="id"
                defaultSortDesc={true}
                pagination paginationComponentOptions={paginationComponentOptions}
                progressPending={pending}
                progressComponent={<CustomLoader />}
                noDataComponent="No se encuentran registros en la base de datos"
                highlightOnHover
              />
            </DataTableExtensions>
          </div>
        </div>
      </div>
      <ModalView production={productionChoose} show={showModalView} onHide={()=> setShowModalView(false)}/>
      <ModalDelete production={productionChoose} show={showModalDelete} onHide={()=> setShowModalDelete(false)} setOnHide={setShowModalDelete} />
      <ModalUpdate production={productionChoose} show={showModalUpdate} onHide={()=> setShowModalUpdate(false)} setOnHide={setShowModalUpdate} />
    </>
  );
}

const mapStateToProps = (state) => ({
  usuario: state.authentication.user.groups,
});

export default connect(mapStateToProps)(TableProductionsCoordinador);
