import React from 'react';
import { costs, modalities, categories, languages } from '../data';
import moment from 'moment';
import 'moment/locale/es';
import { Modal, Container, Row, Col, Button } from 'react-bootstrap';
import CONFIG from '../../../configuration/configuration';

const ModalView = ({ production, show, onHide }) => {

  return (
    <Modal
      show={show}
      size='lg'
      onHide={onHide}
      backdrop="static"
      keyboard={false}
      aria-labelledby="modalViewAccount"
      centered
      scrollable
    >
      <Modal.Header closeButton style={{ background: 'gray', padding: "20px" }}>
        <Modal.Title id="contained-modal-title-center" style={{ color: 'white' }}>
          Visualizar Producción
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Container fluid>
          <Row className="p-2">
            <Col md={5}>
              <h5>Producción</h5>
              <p>{production.name}</p>
              {production.date_end ?
                <>
                  <h5>Fecha de inicio</h5><p>{moment(production.date_start).local().format('DD/MM/YYYY HH:mm')} hs</p>

                  <h5>Fecha de finalización</h5> <p>{moment(production.date_end).local().format('DD/MM/YYYY HH:mm')} hs</p> </> :
                <>
                  <h5>Fecha</h5> <p>{moment(production.date_start).local().format('DD/MM/YYYY HH:mm')}  hs</p></>
              }
              <h5>Organiza</h5>
              <p>{production.organizer_person}</p>
              <h5>Lenguaje artístico </h5>
              <p>{languages.map(language => (language.value === production.language ? language.label : ""))}</p>
              <h5>Modalidad </h5>
              <p>{modalities.map(modality => (modality.value === production.modality ? modality.label : ""))}</p>
              <h5>Categoría</h5>
              <p>{categories.map(category => (category.value === production.category ? category.label : ""))}</p>
              <h5>Costo</h5>
              {production.price !== 0.00 ? `$ ${production.price}` : costs.map(option => (
                option.value === production.cost ? option.label : ''))}

            </Col>
            <Col md={7}>
              <Container>
                <img width="100%" alt={production.name} src={CONFIG.BASE_URL + production.image} />
              </Container>
              <Container>
                <h5 className="pt-2">Descripción</h5>
                <p style={{ whiteSpace: "pre-line" }}>{production.description}</p>
              </Container>

            </Col>
          </Row>

        </Container>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={onHide}>Cerrar</Button>
      </Modal.Footer>
    </Modal>
  );
}

export default ModalView;