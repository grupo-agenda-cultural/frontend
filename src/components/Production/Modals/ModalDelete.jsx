import React from 'react';
import { useDispatch } from 'react-redux';
import { deleteProduction } from '../../../actions/productionAction';
import { Modal, Container, Row, Col, Button } from 'react-bootstrap';

const ModalDelete = ({ production, show, onHide, setOnHide }) => {

  //Dispatch para ejecutar la acción
  const dispatch = useDispatch();

  const handleDelete = (e) => {
    e.preventDefault();
    dispatch(deleteProduction({ production }));
    setOnHide(false)
  }

  return (
    <>

      <Modal
        show={show}
        size='md'
        onHide={onHide}
        backdrop="static"
        keyboard={false}
        aria-labelledby="modalDeleteProduction"
        centered
      >
        <Modal.Header closeButton style={{ background: '#bb2d3b', padding: "20px" }}>
          <Modal.Title style={{ color: 'white' }}>
            Eliminar producción
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Container>
            <Row>
              <Col md={12}>
                ¿Está seguro que desea eliminar la producción "{production.name}"?
              </Col>
            </Row>
          </Container>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={handleDelete} type="button" variant="danger">Eliminar</Button>
        </Modal.Footer>
      </Modal>

    </>
  );
}
export default ModalDelete;