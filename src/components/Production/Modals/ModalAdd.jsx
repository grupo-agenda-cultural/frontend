import React from 'react';
import { useDispatch } from 'react-redux';
import { addProduction } from '../../../actions/productionAction';
import { languages, categories, costs, modalities } from '../data';
import DatePicker, { registerLocale } from "react-datepicker";
import 'react-datepicker/dist/react-datepicker.css';
import es from "date-fns/locale/es"
import moment from 'moment';
import * as Yup from 'yup'
import { connect } from 'react-redux';
import { Formik, Field, ErrorMessage} from "formik";
import { Modal, Container, Form,Button, Row, Col, InputGroup} from 'react-bootstrap';

const ModalAdd = ({coordinador, show, onHide, setOnHide}) => {

  const dispatch = useDispatch();
  // const[formEnviado, setFormEnviado] = useState(false)
  const format = "dd/MM/yyyy HH:mm"
  registerLocale('es', es);

  const formatDate = (date) => {
    const check = moment(date).format("DD/MM/YYYY HH:mm");
    return check;
  }

  const handleSubmit = (values) => {
    dispatch(addProduction(coordinador,values, formatDate(values.date_start), formatDate(values.date_end)));
    setOnHide(false);
  }


  // Validaciones
  const ErrorMessagesSchema = Yup.object().shape({
    image: Yup.mixed().required("Debe subir la imagen de la producción") , 
    name: Yup.string().required('Campo requerido'),
    organizer_person: Yup.string().required('Campo requerido'),
    description: Yup.string().required('Campo requerido'),
    date_start: Yup.mixed().required('Debe seleccionar una fecha'),
    date_end: Yup.mixed().required('Debe seleccionar una fecha'),
    language: Yup.string().required("Debe seleccionar una opción"),
    modality: Yup.string().required("Debe seleccionar una opción"),
    category: Yup.string().required("Debe seleccionar una opción"),
    cost: Yup.string().required("Debe seleccionar una opción"),
    price: Yup.number().typeError("Precio inválido")
  });


  return (
    <Modal show={show}
      size='lg'
      onHide={onHide}
      backdrop="static"
      keyboard={false}
      aria-labelledby="modalUpdateCulturalSpace"
      centered
      scrollable
    >
      <Modal.Header closeButton style={{ background: "#198754", padding: "20px" }} >
        <Modal.Title style={{ color: "white" }}>
          Agregar Producción
        </Modal.Title>
      </Modal.Header>
            <Formik
              initialValues={{
                image: null,
                name: '',
                organizer_person: '',
                description: '',
                date_start: '',
                date_end: '',
                language: '',
                modality:'',
                category: '',
                cost: '',
                price: 0
              }}
              enableReinitialize
              onSubmit={(values, { resetForm }) => {
                handleSubmit(values);
                resetForm();
                // setTimeout(() => setFormEnviado(true),2000);
              }}
              validationSchema={ErrorMessagesSchema}
            >
              {({ errors, touched, handleSubmit, handleChange, handleBlur,values,isValid, setFieldValue}) => (
                <Modal.Body>
                <Form noValidate onSubmit={handleSubmit}>
                  <Container>
                     <Row className="m-2">
                     <Col xl={6} md={6}>
                     <Form.Group>
                     <Form.Label>Imagen de la producción</Form.Label>
                     <img src={values.image ? URL.createObjectURL(values.image) : null} style={{width:"20em"}} alt={values.image ? values.image.name : null} />

                        <input type="file"  accept="image/*"  onClick={e => (e.target.value = null)} onChange={event => setFieldValue("image",event.target.files[0])}  name="image" className="form-control-file" id="production-img" />
                      {errors.image && touched.image ? <div style={{ witch:"100%", marginTop: "0.2rem", fontSize: ".875em",color: "#dc3545"}}><ErrorMessage name="image"/></div> : null}
                     </Form.Group>
                     </Col>
                      </Row>
                      <Row className="m-2">
                  <Col lg={6} md={12} sm={12} className="pt-2">
                    <Form.Group>

                      <Form.Label>Nombre de la producción</Form.Label>
                      <Form.Control
                        
                        type="text"
                        name="name"
                        placeholder="Ingresá el nombre de la producción"
                        value={values.name}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        isInvalid={touched.name && errors.name}
                      />
                      <Form.Control.Feedback type="invalid">
                        {errors.name}
                      </Form.Control.Feedback>
                    </Form.Group>
                  </Col>
                  <Col lg={6} md={12} sm={12} className="pt-2">
                    <Form.Group>

                      <Form.Label>Organizador/a de la producción</Form.Label>
                      <Form.Control
                        type="text"
                        name="organizer_person"
                        placeholder="Ingresá el organizador/a"
                        value={values.organizer_person}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        isInvalid={touched.organizer_person && errors.organizer_person}
                      />
                      
                      <Form.Control.Feedback type="invalid">
                        {errors.organizer_person}
                      </Form.Control.Feedback>
                    </Form.Group>
                  </Col>
                      </Row>
                   
                <Row className="m-2">
                  <Col className="pt-2">
                    <Form.Group>
                      <InputGroup className="mb-3">
                        <InputGroup.Text id="description">Descripción</InputGroup.Text>
                        <Form.Control
                          style={{resize:'none'}}
                          type="textarea"
                          as="textarea"
                          name="description"
                          placeholder="Ingresá una breve descripción de la producción"
                          value={values.description}
                          onChange={handleChange}
                          rows={20}
                          onBlur={handleBlur}
                          isInvalid={touched.description && errors.description}
                        />
                        <Form.Control.Feedback type="invalid">
                          {errors.description}
                        </Form.Control.Feedback>
                      </InputGroup>

                    </Form.Group>

                  </Col>
                </Row>
                     <Row className="m-2">
                     <Col lg={6} md={12} xs={12} className="pt-2">
                    <Form.Label>Día y horario de inicio de la producción</Form.Label>
                        <DatePicker 
                          selected={values.date_start}
                          value={values.date_start}
                          name="date_start" 
                          onChange={date => setFieldValue("date_start", date)}
                          errors={errors.date_start}
                          touched={touched.date_start}
                          locale="es" 
                          showTimeSelect 
                          timeIntervals={15} 
                          minDate={new Date()} 
                          dateFormat={format}
                          onKeyDown={(e) => {
                            e.preventDefault();
                           }}
                          placeholderText="Seleccioná una fecha"/>
                         {errors.date_start && touched.date_start ? <div style={{ witch:"100%", marginTop: "0.2rem", fontSize: ".875em",color: "#dc3545"}}><ErrorMessage name="date_start"/></div> : null}
                      </Col> 
                      <Col lg={6} md={12} xs={12} className="pt-2">
                    <Form.Label>Día y horario de inicio de la producción</Form.Label>
                        <DatePicker
                          // value={startDate} 
                          selected={values.date_end}
                          value={values.date_end}
                          name="date_end" 
                          onChange={date => setFieldValue("date_end", date)}
                          errors={errors.date_end}
                          touched={touched.date_end}
                          locale="es" 
                          showTimeSelect 
                          timeIntervals={15} 
                          minDate={new Date()} 
                          dateFormat={format}
                          onKeyDown={(e) => {
                            e.preventDefault();
                           }} 
                          placeholderText="Seleccioná una fecha"/>
                         {errors.date_end && touched.date_end ? <div style={{ witch:"100%", marginTop: "0.2rem", fontSize: ".875em",color: "#dc3545"}}><ErrorMessage name="date_end"/></div> : null}
                      </Col> 
                      </Row>
              
                    
                      <Row className="m-2">
                  <Col lg={6} className="pt-2">
                    <Form.Label>Lenguaje artístico</Form.Label>
                    <Form.Select
                      type="select"
                      as="select"
                      name="language"
                      value={values.language}
                      aria-label=".form-select-sm"
                      size="sm"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      isInvalid={errors.language && touched.language} >
                      <option hidden>Seleccioná el lenguaje artístico</option>
                      {languages.map(elem => (
                        <option key={elem.id} value={elem.value}>{elem.label}</option>
                      ))}
                    </Form.Select>
                    <Form.Control.Feedback type="invalid">
                      {errors.language}
                    </Form.Control.Feedback>
                  </Col>

                  <Col lg={6} className="pt-2">
                    <Form.Label>Modalidad</Form.Label>
                    <Row>
                    <Form.Group>

                      <Field component="div" name="modality">
                                            <Form.Check
                          inline
                          type="radio"
                          id="radioOne"
                          defaultChecked={values.modality === 1}
                          name="modality"
                          value={1}
                          label={modalities[0].label}
                          onBlur={handleBlur}
                          isInvalid={touched.modality && errors.modality} 


                        />

                        <Form.Check
                          inline
                          type="radio"
                          id="radioOne"
                          defaultChecked={values.modality === 2}
                          name="modality"
                          value={2}
                          onBlur={handleBlur}
                          label={modalities[1].label}
                          isInvalid={touched.modality && errors.modality} 

                        />
                         </Field>
                         {errors.modality && touched.modality ? <div style={{ witch:"100%", marginTop: "0.2rem", fontSize: ".875em",color: "#dc3545"}}><ErrorMessage name="modality"/></div> : null}

                        <Form.Control.Feedback type="invalid">
                         {errors.modality}

                    </Form.Control.Feedback>
                        </Form.Group>

                     
                    </Row>
                  </Col>
                      </Row>
                    
                      <Row className="m-2">
                  <Col lg={5} md={6} xs={12} className="pt-2">
                    <Form.Label>Categoría</Form.Label>
                    <Form.Select
                      type="select"
                      as="select"
                      name="category"
                      value={values.category}
                      aria-label=".form-select-sm"
                      size="sm"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      isInvalid={errors.category && touched.category} >
                      <option hidden>Seleccioná la categoría</option>
                      {categories.map(elem => (
                        <option key={elem.id} value={elem.value}>{elem.label}</option>
                      ))}
                    </Form.Select>
                    <Form.Control.Feedback type="invalid">
                      {errors.category}
                    </Form.Control.Feedback>
                  </Col>

                  <Col lg={4} md={6} xs={12} className="pt-2">
                    <Form.Label>Costo</Form.Label>
                    <Form.Select
                      type="select"
                      as="select"
                      name="cost"
                      value={values.cost}
                      aria-label=".form-select-sm"
                      size="sm"
                      onChange={handleChange}
                      isInvalid={errors.cost && touched.cost} >
                      <option hidden>Seleccioná el costo</option>
                      {costs.map(elem => (
                        <option key={elem.id} value={elem.value}>{elem.label}</option>
                      ))}
                    </Form.Select>
                    <Form.Control.Feedback type="invalid">
                      {errors.cost}
                    </Form.Control.Feedback>
                  </Col>
                   {values.cost.toString() === '2' ? 
                  <Col lg={3} md={6} xs={12} className="pt-2">
                    <Form.Group >
                      <Form.Label>Precio</Form.Label>
                      <InputGroup  size="sm" className="mb-3">
                        <InputGroup.Text id="basic-addon1">$</InputGroup.Text>
                        <Form.Control
                          type="text"
                          name="price"
                          placeholder="0"
                          size="sm"
                          value={values.price}
                          onChange={handleChange}
                          isInvalid={!!errors.price}
                        >
                        </Form.Control>
                        <Form.Control.Feedback type="invalid">
                          {errors.price}
                        </Form.Control.Feedback>
                      </InputGroup>


                    </Form.Group>
                  </Col>
                   :''}
                   </Row>
                 <Modal.Footer>
                    {/* <button type="button" className="btn btn-secondary" onClick={resetForm} data-bs-dismiss="modal" aria-label="Close">Cancelar</button> */}
                    {/* <input type="submit" value="Enviar" data-bs-dismiss={formEnviado ? "modal" : ''} className="btn btn-success" /> */}
                <Button type="submit" variant="success">Enviar</Button>
                
                    </Modal.Footer>
                  </Container>
                </Form>
                </Modal.Body>
              )}
            </Formik>
      </Modal>
      );
}

// Get Data from redux store
const mapStateToProps = (state) => ({
  // Check Authentictaion
  isAuthenticated: state.authentication.isAuthenticated,
  coordinador: state.authentication.coordinador,
});

export default connect(mapStateToProps)(ModalAdd) ;