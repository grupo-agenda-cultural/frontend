import React from 'react';
import { useDispatch } from 'react-redux';
import { updateProduction } from '../../../actions/productionAction';
import DatePicker, { registerLocale } from "react-datepicker";
import 'react-datepicker/dist/react-datepicker.css';
import moment from 'moment';
import * as Yup from 'yup'
import { languages, categories, costs, modalities } from '../data';
import { Formik, Field, ErrorMessage } from "formik";
import es from "date-fns/locale/es"
import CONFIG from '../../../configuration/configuration';
import { Button, Modal, Container, Row, Col, Form, FormGroup, FormLabel, InputGroup, } from 'react-bootstrap';

const ModalUpdate = ({ production, show, onHide, setOnHide }) => {

  const dispatch = useDispatch();
  const format = "dd/MM/yyyy HH:mm"
  registerLocale('es', es);


  const handleSubmit = (values) => {
    dispatch(updateProduction(values, formatDate(values.date_start), formatDate(values.date_end)));
    // dispatch(getProductions())
    setOnHide(false)
  }

  // Validaciones
  const ErrorMessagesSchema = Yup.object().shape({
    image: Yup.mixed().required("Debe subir la imagen de la producción"),
    name: Yup.string().required('Campo requerido'),
    organizer_person: Yup.string().required('Campo requerido'),
    description: Yup.string().required('Campo requerido'),
    date_start: Yup.mixed().required('Debe seleccionar una fecha'),
    date_end: Yup.mixed().required('Debe seleccionar una fecha'),
    language: Yup.string().required("Debe seleccionar una opción"),
    modality: Yup.string().required("Debe seleccionar una opción"),
    category: Yup.string().required("Debe seleccionar una opción"),
    cost: Yup.string().required("Debe seleccionar una opción"),
    price: Yup.number().typeError("Precio inválido")
  });

  const formatDate = (date) => {
    const check = moment(date).format("DD/MM/YYYY HH:mm");
    return check;
  }

  return (
    <Modal show={show}
      size='lg'
      onHide={onHide}
      backdrop="static"
      keyboard={false}
      aria-labelledby="modalUpdateCulturalSpace"
      centered
      scrollable
    >
      <Modal.Header closeButton style={{ background: "#0275d8", padding: "20px" }} >
        <Modal.Title style={{ color: "white" }}>
          Editar Producción
        </Modal.Title>
      </Modal.Header>
      <Formik
        enableReinitialize
        initialValues={{
          id: production.id,
          image: production.image,
          name: production.name,
          organizer_person: production.organizer_person,
          description: production.description,
          date_start: production.date_start,
          date_end: production.date_end,
          language: production.language,
          modality: production.modality,
          category: production.category,
          cost: production.cost,
          price: production.price
        }}
        onSubmit={(values) => {
          handleSubmit(values);
        }}
        validationSchema={ErrorMessagesSchema}
      >
        {({ errors, touched, handleSubmit, handleChange, values, setFieldValue, isValid, isSubmitting }) => (
          <Modal.Body>
            <Form onSubmit={handleSubmit}>
              <Container>
                <Row className="m-2">
                  <Form.Group>                 
                    <Form.Label>Imagen de la producción</Form.Label>
                    <div className="imagen">
                      <img src={CONFIG.BASE_URL + values.image} alt={values.name} style={{ width: '20em' }} />
                    </div>
                    <input type="file" accept="image/*"  onClick={e => (e.target.value = null)} onChange={event => setFieldValue("image", event.target.files[0])} name="image" className="form-control-file" id="production-img" />
                  </Form.Group>
                  {errors.image ? <div style={{ witch: "100%", marginTop: "0.2rem", fontSize: ".875em", color: "#dc3545" }}>
                    <ErrorMessage name="image" /></div> : null}
                </Row>

                <Row className="m-2">
                  <Col lg={6} md={12} sm={12} className="pt-2">
                    <Form.Group>

                      <Form.Label>Nombre de la producción</Form.Label>
                      <Form.Control
                        type="text"
                        name="name"
                        placeholder="Ingresá el nombre del espacio cultural"
                        value={values.name}
                        onChange={handleChange}
                        isInvalid={!!errors.name}
                      />
                      <Form.Control.Feedback type="invalid">
                        {errors.name}
                      </Form.Control.Feedback>
                    </Form.Group>
                  </Col>
                  <Col lg={6} md={12} sm={12} className="pt-2">
                    <FormGroup>

                      <FormLabel>Organizador/a de la producción</FormLabel>
                      <Form.Control
                        type="text"
                        name="organizer_person"
                        placeholder="Ingresá el organizador/a"
                        value={values.organizer_person}
                        onChange={handleChange}
                        isInvalid={!!errors.organizer_person}
                      />
                      <Form.Control.Feedback type="invalid">
                        {errors.organizer_person}
                      </Form.Control.Feedback>
                    </FormGroup>
                  </Col>
                </Row>

                <Row className="m-2">
                  <Col className="pt-2">
                    <FormGroup>
                      <InputGroup className="mb-3">
                        <InputGroup.Text id="description">Descripción</InputGroup.Text>
                        <Form.Control
                          style={{ resize: 'none' }}
                          type="textarea"
                          as="textarea"
                          name="description"
                          placeholder="Ingresá una breve descripción de la producción"
                          value={values.description}
                          onChange={handleChange}
                          rows={20}
                          // isInvalid={!!errors.description}
                          isInvalid={!!errors.description}
                        />
                        <Form.Control.Feedback type="invalid">
                          {errors.description}
                        </Form.Control.Feedback>
                      </InputGroup>

                    </FormGroup>

                  </Col>
                </Row>
                <Row className="m-2">
                  <Col lg={6} md={12} xs={12} className="pt-2">
                    <FormLabel>Día y horario de inicio de la producción</FormLabel>
                    <DatePicker
                      selected={(values.date_start && new Date(values.date_start)) || null}
                      value={moment(values.date_start).local().format('DD/MM/YYYY HH:mm')}
                      name="date_start"
                      onChange={date => setFieldValue("date_start", date)}
                      errors={errors.date_start}
                      touched={touched.date_start}
                      // isValid={touched.date_start && !errors.date_start}
                      locale="es"
                      showTimeSelect
                      timeIntervals={15}
                      dateFormat={format}
                      minDate={new Date()}
                      onKeyDown={(e) => {
                        e.preventDefault();
                      }}
                      placeholderText="Seleccioná una fecha" />
                    {errors.date_start && touched.date_start ? <div style={{ witch: "100%", marginTop: "0.2rem", fontSize: ".875em", color: "#dc3545" }}><ErrorMessage name="date_start" /></div> : null}
                  </Col>
                  <Col lg={6} md={12} xs={12} className="pt-2">
                    <Form.Label>Día y horario de fin de la producción</Form.Label>
                    <DatePicker
                      selected={(values.date_end && new Date(values.date_end)) || null}
                      value={moment(values.date_end).local().format('DD/MM/YYYY HH:mm')}
                      name="date_end"
                      onChange={date => setFieldValue("date_end", date)}
                      errors={errors.date_end}
                      touched={touched.date_end}
                      locale="es"
                      showTimeSelect
                      timeIntervals={15}
                      minDate={new Date()}
                      dateFormat={format}
                      onKeyDown={(e) => {
                        e.preventDefault();
                      }}
                      placeholderText="Seleccioná una fecha" />
                    {errors.date_end && touched.date_end ? <div style={{ witch: "100%", marginTop: "0.2rem", fontSize: ".875em", color: "#dc3545" }}><ErrorMessage name="date_end" /></div> : null}
                  </Col>
                </Row>



                <Row className="m-2">
                  <Col lg={6} className="pt-2">
                    <Form.Label>Lenguaje artístico</Form.Label>
                    <Form.Select
                      type="select"
                      as="select"
                      name="language"
                      value={values.language}
                      aria-label=".form-select-sm"
                      size="sm"
                      onChange={handleChange}
                      isInvalid={!!errors.language} >
                      <option hidden>Seleccioná el lenguaje artístico</option>
                      {languages.map(elem => (
                        <option key={elem.id} value={elem.value}>{elem.label}</option>
                      ))}
                    </Form.Select>
                    <Form.Control.Feedback type="invalid">
                      {errors.language}
                    </Form.Control.Feedback>
                  </Col>

                  <Col lg={6} className="pt-2">
                    <Form.Label>Modalidad</Form.Label>
                    <Row>
                      <Form.Group>

                        <Field component="div" name="modality">
                          <Form.Check
                            inline
                            type="radio"
                            id="radioOne"
                            defaultChecked={values.modality === 1}
                            name="modality"
                            value={1}
                            label={modalities[0].label}
                            isInvalid={!!errors.modality}

                          />

                          <Form.Check
                            inline
                            type="radio"
                            id="radioOne"
                            defaultChecked={values.modality === 2}
                            name="modality"
                            value={2}
                            label={modalities[1].label}
                            isInvalid={!!errors.modality}

                          />
                        </Field>
                        <Form.Control.Feedback type="invalid">
                          {errors.modality}

                        </Form.Control.Feedback>
                      </Form.Group>



                    </Row>
                  </Col>

                </Row>

                <Row className="m-2">
                  <Col lg={5} md={6} xs={12} className="pt-2">
                    <FormLabel>Categoría</FormLabel>
                    <Form.Select
                      type="select"
                      as="select"
                      name="category"
                      value={values.category}
                      aria-label=".form-select-sm"
                      size="sm"
                      onChange={handleChange}
                      isInvalid={!!errors.category} >
                      <option hidden>Seleccioná la categoría</option>
                      {categories.map(elem => (
                        <option key={elem.id} value={elem.value}>{elem.label}</option>
                      ))}
                    </Form.Select>
                    <Form.Control.Feedback type="invalid">
                      {errors.category}
                    </Form.Control.Feedback>
                  </Col>
                  <Col lg={4} md={6} xs={12} className="pt-2">
                    <FormLabel>Costo</FormLabel>
                    <Form.Select
                      type="select"
                      as="select"
                      name="cost"
                      value={values.cost}
                      aria-label=".form-select-sm"
                      size="sm"
                      onChange={handleChange}
                      isInvalid={!!errors.cost} >
                      <option hidden>Seleccioná el costo</option>
                      {costs.map(elem => (
                        <option key={elem.id} value={elem.value}>{elem.label}</option>
                      ))}
                    </Form.Select>
                    <Form.Control.Feedback type="invalid">
                      {errors.cost}
                    </Form.Control.Feedback>
                  </Col>
                  {values.cost.toString() === '2' ?
                    <Col lg={3} md={6} xs={12} className="pt-2">
                      <FormGroup >
                        <FormLabel>Precio</FormLabel>
                        <InputGroup size="sm" className="mb-3">
                          <InputGroup.Text id="basic-addon1">$</InputGroup.Text>
                          <Form.Control
                            type="text"
                            name="price"

                            placeholder="0"
                            value={values.price}
                            onChange={handleChange}
                            isInvalid={!!errors.price}
                          >
                          </Form.Control>
                          <Form.Control.Feedback type="invalid">
                            {errors.price}
                          </Form.Control.Feedback>
                        </InputGroup>


                      </FormGroup>
                    </Col>
                    : ''}
                </Row>
                <Modal.Footer>
                  <Button type="submit" variant="primary" >Enviar</Button>
                </Modal.Footer>
              </Container>
            </Form>
          </Modal.Body>
        )
        }
      </Formik >
    </Modal >
  );
}


export default ModalUpdate;