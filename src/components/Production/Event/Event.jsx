import React, { useEffect } from 'react';
import { useParams } from "react-router-dom";
import { useDispatch, useSelector } from 'react-redux';
import './Event.css';
import { MapContainer, TileLayer, Marker, Popup } from 'react-leaflet'
import markerIconPng from "leaflet/dist/images/marker-icon.png"
import { Icon } from 'leaflet';
import "leaflet/dist/leaflet.css";
import { categories, languages, costs, modalities } from '../data';
import { localidades, partidos } from '../../CulturalSpace/data';
import moment from 'moment';
import 'moment/locale/es';
import CONFIG from '../../../configuration/configuration';
import { getProductionCalendar, getProductionsCalendar } from '../../../actions/productionAction';
import Error404 from '../../../routers/Error404';

const Event = () => {

    const dispatch = useDispatch();
    const { productionId } = useParams();

    const productions = useSelector(state => state.productions.productions)
    const card = productions.find(elem => elem.production.id.toString()  === productionId.toString())

    useEffect(() => {
        dispatch(getProductionsCalendar());
        dispatch(getProductionCalendar(productionId));
        // eslint-disable-next-line
    }, []);

    useEffect(() => {
        window.scrollTo({ top: 0, behavior: 'smooth' })
    }, [])


    return (
        <>
            {card !== undefined ?
                <>
                    <div className="language-production">
                        <h5>{languages.map(option => (option.value === card.production.language ? option.label : ""))}</h5>
                    </div>
                    <div className="title-production-event">
                        <h1 className="title-production">{card.production.name}</h1>
                    </div>
                    <div className="container-center">
                        <div className="container-production">
                            <img src={CONFIG.BASE_URL + card.production.image} style={{ width: "40em", maxWidth: "100%" }} alt="produccion" />
                        </div>
                        <div className="production-ticket">
                            <div className="ticket-header">
                                <div className="title">
                                    Costo
                                </div>
                                <div className="price">
                                    {card.production.price !== 0.00 ? `$ ${card.production.price}` : costs.map(option => (
                                        option.value === card.production.cost ? option.label : ''))}
                                </div>
                            </div>
                            <div className="ticket-body">
                                <p className="date"><i className="bi bi-calendar px-2"></i>{moment(card.production.date_start).local().format('DD/MM/YYYY')} {moment(card.production.date_end).local().format('DD/MM/YYYY') === moment(card.production.date_start).local().format('DD/MM/YYYY') ? '' : 'a ' + moment(card.production.date_end).local().format('DD/MM/YYYY')}</p><br />
                                <p className="date"><i className="bi bi-clock px-2"></i>{moment(card.production.date_start).local().format('HH:mm')} hs a {card.production.date_end ? moment(card.production.date_end).local().format('HH:mm') + ' hs' : ''} </p>
                            </div>

                            <div className="button">
                                <a className="float-button" target="_blank" rel="noreferrer" href={`https://wa.me/54${card.coordinador.phone}?text=Hola ${card.coordinador.name}!%0AMe%20podrías%20pasar%20más%20información%20sobre%20la%20producción%20*${card.production.name}*.%0AMi%20nombre%20es%20`}>
                                    <i className="bi bi-whatsapp"></i>
                                    <span className="text-whatsapp">Consultar sobre la producción</span>
                                </a>
                            </div>
                        </div>

                    </div>
                    <div className="body-event">
                        <div className="production-description">
                            <p className="organizer">Organiza: {card.production.organizer_person}</p>
                            <p className="description" style={{ whiteSpace: "pre-line" }}> {card.production.description}</p>
                            {card.production.date_end ?
                                <>
                                    <p className="dates"><strong>Fecha de inicio:</strong> {moment(card.production.date_start).local().format('DD/MM/YYYY HH:mm')} hs</p>

                                    <p className="dates"><strong>Fecha de finalización:</strong> {moment(card.production.date_end).local().format('DD/MM/YYYY HH:mm')} hs</p></> :
                                <p className="dates"><strong>Fecha:</strong> {moment(card.production.date_start).local().format('DD/MM/YYYY HH:mm')} hs</p>
                            }
                            <p className="modality"><strong>Modalidad:</strong> {modalities.map(option => option.value === card.production.modality ? option.label : "")}</p>
                            <p className="modality"><strong>Categoría:</strong> {categories.map(option => option.value === card.production.category ? option.label : "")} </p>
                            <p className="modality"><strong>Espacio cultural:</strong> {card.cultural_space.name}</p>
                        </div>

                    </div>
                    <div className="container-map">
                        <MapContainer center={[card.cultural_space.latitude, card.cultural_space.longitude]} zoom={15} scrollWheelZoom={false} style={{ height: '80vh', width: '100vw' }}>
                            <TileLayer
                                attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png" />
                            <Marker position={[card.cultural_space.latitude, card.cultural_space.longitude]} icon={new Icon({ iconUrl: markerIconPng, iconSize: [25, 41], iconAnchor: [12, 41] })}>
                                <Popup maxWidth="250" maxHeight="auto">
                                    <img height="150px" width="100%" src={CONFIG.BASE_URL + card.cultural_space.image} alt={card.cultural_space.image} />
                                    <div className="container text-center">
                                        <h5 className="py-2 mb-1 text-primary">{card.cultural_space.name}</h5>
                                        Dirección: {card.cultural_space.address}<br />
                                        Partido: {partidos.map(option => option.value === card.cultural_space.partido ? option.label : '')}<br />
                                        Localidad: {localidades.map(option => option.value === card.cultural_space.localidad ? option.label : '')}<br />
                                        Teléfono: {card.cultural_space.phone}<br />
                                        Email: {card.cultural_space.email}<br />
                                        {card.cultural_space.instagram ? <a href={card.cultural_space.instagram} target="_blank" rel="noreferrer"><i className="bi bi-instagram" style={{ fontSize: "24px", color: " #f54056", cursor: "pointer" }}></i></a> : ''}
                                        &nbsp;
                                        {card.cultural_space.facebook ? <a href={card.cultural_space.facebook} target="_blank" rel="noreferrer"><i className="bi bi-facebook" style={{ fontSize: "24px", color: "#3f729b", cursor: "pointer" }}></i></a> : ''}
                                        <br />
                                        <a href={`https://maps.google.com/?q=${card.cultural_space.latitude},${card.cultural_space.longitude}`} target="_blank" rel="noreferrer"><button type="button" className="btn btn-primary" >Cómo llegar</button></a>
                                    </div>
                                </Popup>
                            </Marker>
                        </MapContainer>
                    </div>
                </>
                : <Error404 />}

        </>
    )
}

export default Event;