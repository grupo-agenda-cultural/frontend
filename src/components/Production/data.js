export const categories = [
    { id: 1, value: 1, label: 'Adultos'},
    { id: 2, value: 2, label: 'Niños' },
    { id: 3, value: 3, label: 'Público en general' }
  ]

 export const costs = [
    { id: 1, value: 1, label: 'A la gorra' },
    { id: 2, value: 2, label: 'Con precio' },
    { id: 3, value: 3, label: 'Gratuito' }
  ]

  export const languages = [
    { id: 1, value: 1, label: 'Artes Visuales' },
    { id: 2, value: 2, label: 'Audiovisuales' },
    { id: 3, value: 3, label: 'Danza' },
    { id: 4, value: 4, label: 'Ferias' },
    { id: 5, value: 5, label: 'Festival' },
    { id: 6, value: 6, label: 'Letras' },
    { id: 7, value: 7, label: 'Música' },
    { id: 8, value: 8, label: 'Peña' },
    { id: 9, value: 9, label: 'Teatro' }    
  ]

  export const modalities = [
    { id: 1, value: 1, label: 'Presencial' },
    { id: 2, value: 2, label: 'Virtual' }
]
