import React, { useState } from 'react';
import Pagination from '../../../components/Pagination/pagination';
import Card from './Card';
import './ListCards.css';

const ListCards = ({ cards = [] }) => {

  const productionsPerPage = 12; 
  const [pageNumber, setPageNumber] = useState(0);
  const pageCount = Math.ceil(cards.length / productionsPerPage);
  const pagesVisited = pageNumber * productionsPerPage;

  const changePage = ({ selected }) => {
    setPageNumber(selected);
  };
  
  const displayCards = cards
    .slice(pagesVisited, pagesVisited + productionsPerPage)
    .map((card,index) => {
      return (
        <Card key={index} card={card} />
      )
    });

  return (
      <div className="container-agenda">
          <div className="container-cards-agenda">
            {displayCards}  
            
        </div>
        <Pagination
          pageCount={pageCount}
          changePage={changePage}
        />
      </div>
  )
}
export default ListCards;
