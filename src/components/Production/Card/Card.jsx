import React from 'react';
import { Link } from 'react-router-dom';
import './Card.css'
import { categories, costs, languages, modalities } from '../data';
import CONFIG from '../../../configuration/configuration';
import moment from 'moment';
import 'moment/locale/es';


const Card = ({ card }) => {
    moment.locale('es');
    return (
           
            <div className="card-production">
                <div className="card-image">
                    <img src={CONFIG.BASE_URL + card.production.image} alt="production-img" style={{ height: '180px', width: '100%' }}  />
                </div>
                <div className="card-texto justify-content-center">
                    <span className="language">{languages.map(option => (option.value === card.production.language ? option.label : ""))}
                    </span>
                    <h2>{card.production.name}</h2>
                    {/* <div className="card-body"> */}
                    <p className="parrafo-production"><i className="bi bi-calendar px-2"></i>{moment(card.production.date_start).local().format('DD/MM/YYYY')} {moment(card.production.date_end).local().format('DD/MM/YYYY') === moment(card.production.date_start).local().format('DD/MM/YYYY') ? '' : 'a ' + moment(card.production.date_end).local().format('DD/MM/YYYY')}</p>
                    
                    <p className="parrafo-production"><i className="bi bi-clock px-2"></i>{moment(card.production.date_start).local().format('HH:mm')} hs a {card.production.date_end ? moment(card.production.date_end).local().format('HH:mm') + ' hs' : ''} </p> 
                    
                    {/* {card.production.date_end ? <p className="parrafo">{moment(card.production.date_end).local().format('DD/MM/YYYY HH:mm')} hs</p> : ''} */}
                    <p className="parrafo-production"><i className="bi bi-person-fill px-2"></i>{categories.map(option => option.value === card.production.category ? option.label : "")}</p>
                    {/* <p className="parrafo"><i className="bi bi-geo-alt px-2"></i>{partidos.map(option => option.value === card.cultural_space.partido ? option.label : "")}</p> */}

                <Link key={card.production.id} to={`/agenda/produccion/${card.production.id}`} style={{ textDecoration: "none" }}><button type="button" className="btn btn-primary">Ver más</button>
                    </Link></div>
                <div className="card-stats">
                    <div className="stat">
                        <div className="type">
                            Modalidad
                        </div>
                        <div className="value">
{modalities.map(option => option.value === card.production.modality ? option.label : "")}                        </div>

                    </div>
                    <div className="stat">
                        <div className="type">
                            Costo                  </div>
                        <div className="value">
                             {card.production.price !== 0.00 ? `$ ${card.production.price}` : costs.map(option => (
                                option.value === card.production.cost ? option.label : ''))}

                        </div>

                    </div>
                </div>
            </div>
    );
}

export default Card;
