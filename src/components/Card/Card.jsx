import React from "react";
import { motion} from 'framer-motion/dist/framer-motion';
import { cardAnimation } from "../../utils/animation";
import "./Card.css";


const Card = ({ image, title, animateCustom }) => {
  return (
    <motion.div
      className="card-acca"
      variants={cardAnimation}
      initial="hidden"
      // animate="show"
      animate={animateCustom}
      transition={{ stiffness: 5000 }}
    >
      <div className="icon-image">{image}</div>
      <h4 className="card-acca-title">{title}</h4>
    </motion.div>
  );
}

export default Card;