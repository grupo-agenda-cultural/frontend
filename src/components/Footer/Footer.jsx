import React from 'react';
import './Footer.css';

const Footer = () => {
    return(
        <div className="footer">
            <p className="copyright">&copy; 2022 Agenda y Comunicación Cultural y Artística</p>        
        </div>
    );
}

export default Footer;