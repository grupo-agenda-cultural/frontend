import React,{ useState } from 'react';
import { Link } from 'react-router-dom';
import './Navbar.css';
import { logout } from '../../../actions/authentication';
import {  Navigate } from "react-router-dom";
import { connect } from 'react-redux';
import { useDispatch } from 'react-redux';
import logo from '../../../assets/images/logo/logo.png';
import Swal from 'sweetalert2';
const Navbar = ({usuario}) => {

    const [click, setClick] = useState(false);
    // const [dropdown, setDropdown] = useState(false);

    const dispatch = useDispatch();

  
    const handleLogout = e => {

Swal.fire({
  title: 'Cerrar sesión',
  text: "¿Estás seguro de que quieres cerrar la sesión?",
  icon: 'question',
  showCancelButton: true,
  cancelButtonColor: '#B5B2B2',
  confirmButtonColor: '#3085d6',
  cancelButtonText: 'Cancelar',
  confirmButtonText: 'Sí, cerrar mi sesión'
}).then((result) => {
  if (result.isConfirmed) {
    dispatch( logout() );   
     return <Navigate to="/login" />;
  }
})
  }

    const handleClick = () => setClick(!click);
    const closeMobileMenu = () => setClick(false);
  

    // const onMouseEnter = () => {
    //   if (window.innerWidth < 1150) {
    //     setDropdown(true);
    //   } else {
    //     setDropdown(false);
    //   }
    // };
  
    // const onMouseLeave = () => {
    //   if (window.innerWidth < 1150) {
    //     setDropdown(false);
    //   } else {
    //     setDropdown(false);
    //   }
    // };

    
    return (
        <>
          <nav className='navbar-coordinador'>
          <Link to="/coordinador/inicio" className="nav-logo"><img src={logo} width="200px" alt="logo"/></Link>
            <div className='menu-icon' onClick={handleClick}>
              <i className={click ? "bi bi-x-circle" : "bi bi-list"} />
            </div>
            <ul className={click ? 'nav-menu-coordinador active' : 'nav-menu-coordinador'}>
              <li className='nav-coordinador-item'>
                <Link to='/coordinador/inicio' className='nav-links' onClick={closeMobileMenu}>
                  Inicio
                </Link>
              </li>
              <li
                className='nav-coordinador-item'
                // onMouseEnter={onMouseEnter}
                // onMouseLeave={onMouseLeave}
              >
                <Link
                  to='/coordinador/espacio-cultural'
                  className='nav-links'
                  onClick={closeMobileMenu}
                >
                  Espacio cultural
                </Link>
              </li>
              
              <li className='nav-coordinador-item'>
                <Link
                 to='/coordinador/producciones' 
                  className='nav-links'
                  onClick={closeMobileMenu}
                >
                  Producciones
                </Link>
              </li>

              <li className='nav-coordinador-item'>
                <Link
                  to='/coordinador/formacion'
                  className='nav-links'
                  onClick={closeMobileMenu}
                >
                  Formación
                </Link>
              </li>
              <li className='nav-coordinador-item'>
                <Link
                  to='/coordinador/reportes'
                  className='nav-links'
                  onClick={closeMobileMenu}
                >
                  Reportes
                </Link>
              </li>
              <li className='nav-coordinador-item'>
              {/*}
                <Link
                 to="/logout"
                  className='nav-links'
                  onClick={closeMobileMenu}
                > */}
                <div className="nav-links">
                  <button type="button" onClick={handleLogout} className="btn btn-primary">Cerrar sesión</button>
                {/* </Link> */}
                </div>
              </li>
            </ul>
          </nav>
        </>
      );
}

// Get Data from redux store
const mapStateToProps = (state) => ({
    // Check Authentictaion
    isAuthenticated: state.authentication.isAuthenticated,
    usuario: state.authentication.user
  });

export default connect(mapStateToProps,{logout})(Navbar) ;