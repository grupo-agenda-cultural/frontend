import React,{useState} from 'react';
import { Link } from 'react-router-dom';
import './Navbar.css';
import logo from '../../../assets/images/logo/logo.png';

const Navbar = () => {

    const [click, setClick] = useState(false);
    // const [dropdown, setDropdown] = useState(false);
    

    const handleClick = () => setClick(!click);
    const closeMobileMenu = () => setClick(false);
  

    // const onMouseEnter = () => {
    //   if (window.innerWidth < 1150) {
    //     setDropdown(false);
    //   } else {
    //     setDropdown(true);
    //   }
    // };
  
    // const onMouseLeave = () => {
    //   if (window.innerWidth < 1150) {
    //     setDropdown(false);
    //   } else {
    //     setDropdown(false);
    //   }
    // };

    
    return (
        <>
          <nav className='nav navbar-rounded shadow-sm p-3 mb-3 rounded'>
          <Link to="/" className="nav-logo"><img src={logo} width="200px" alt="logo"/></Link>
            <div className='menu-icon' onClick={handleClick}>
              <i className={click ? "bi bi-x-circle" : "bi bi-list"} style={{color: "#000"}}/>
            </div>
            <ul className={click ? 'navbar-menu active' : 'navbar-menu'}>
              <li className='navbar-item'>
                <Link to='/inicio' className='navbar-links' onClick={closeMobileMenu}>
                  Inicio
                </Link>
              </li>
              <li
                className='navbar-item'
                // onMouseEnter={onMouseEnter}
                // onMouseLeave={onMouseLeave}
              >
                <Link
                  to='/agenda'
                  className='navbar-links'
                  onClick={closeMobileMenu}
                >
                  Agenda
                </Link>
              </li>
              <li className='navbar-item'>
                <Link
                  to='/formacion'
                  className='navbar-links'
                  onClick={closeMobileMenu}
                >
                  Formación
                </Link>
              </li>
              <li className='navbar-item'>
                <Link
                  to='/espacios-culturales'
                  className='navbar-links'
                  onClick={closeMobileMenu}
                >
                  Espacios culturales
                </Link>
              </li>
              <li className='navbar-item'>
                <Link
                  to='/mapa'
                  className='navbar-links'
                  onClick={closeMobileMenu}
                >
                  Mapa
                </Link>
              </li>
            </ul>
          </nav>
        </>
      );
}

export default Navbar;