export const Button = ({onClick, type, className, toggle, target, dismiss, arialabel, children, icon}) =>
 (
    <button
      onClick={onClick}
      type={type}
      className={className}
      data-bs-toggle={toggle}
      data-bs-target={target}
      data-bs-dismiss={dismiss}
      aria-label={arialabel}
    >
      <i className={icon}></i>
      {children}
    </button>
  );
