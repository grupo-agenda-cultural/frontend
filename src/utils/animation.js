export const cardAnimation = {
    hidden: {
      scale: 0,
      opacity: 0,
    },
    show: {
      scale: 1,
      opacity: 1,
    },
  };

  export const reveal = {
    hidden: { x: -700 },
    show: { x: 0 },
  };

  export const topContainerAnimation = {
    hidden: {
      opacity: 0,
    },
    show: {
      opacity: 1,
    },
  };

  export const videoAnimation = {
    hidden: { x: -800, opacity: 0 },
    show: { x: 0, opacity: 1 },
  };
  export const reasonsAnimation = {
    hidden: { x: 800, opacity: 0 },
    show: { x: 0, opacity: 1 },
  };
  