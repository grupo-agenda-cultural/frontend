import Swal from 'sweetalert2';

export const notification = ( title, message, type ) =>{

    Swal.fire({
       title: title,
       text: message,
       icon: type,
       //timer: 1500,
       confirmButtonText: 'Aceptar'
       //showConfirmButton: false 
    });
}

export const notificationConfirmPassword = ( title, message, type ) =>{

    Swal.fire({
       title: title,
       text: message,
       icon: type,
       //timer: 1500,
       confirmButtonText: 'Aceptar'
       //showConfirmButton: false 
    }).then((result) => {
        if (result.value) {
          window.location.href = `/login`
        }
      }); 
}


export const notificationSubscription = ( title, type ) =>{

    Swal.fire({
       title: title,
       icon: type,
       position: 'center',
       timer: 1500,
       showConfirmButton: false 
    });
}

export const notificationRegister = ( title, message, type ) =>{
    Swal.fire({
        title: title,
        text: message,
        icon: type,
        timer: 1500,
        confirmButtonText: 'Aceptar',
        showConfirmButton: false 
    })
}


export const notificationError = ( title, message, type ) =>{

    Swal.fire({
       title: title,
       text: message,
       icon: type,
       //timer: 1500,
       confirmButtonText: 'Aceptar'
       //showConfirmButton: false 
    });
}

export const notificationTimer = ( title ) => {
     
    let timerInterval;
    
    Swal.fire({
    title: title,
    html: 'Cargando',
    timer: 1500,
    timerProgressBar: true,
    didOpen: () => {
        Swal.showLoading()
        timerInterval = setInterval(() => {
            const content = Swal.getHtmlContainer()
            if (content) {
                const b = content.querySelector('b')
                if (b) {
                    b.textContent = Swal.getTimerLeft()
                }
            }
        }, 100)
    },
    willClose: () => {
        clearInterval(timerInterval)
    }
    }).then((result) => {
        /* Read more about handling dismissals below */
        if (result.dismiss === Swal.DismissReason.timer) {
            //console.log('I was closed by the timer')
        }
    })
}
