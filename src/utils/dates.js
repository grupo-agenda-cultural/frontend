import moment from 'moment';

export const formatDate = (date) => {
    const check = moment(date).format("DD/MM/YYYY HH:mm");
    return check;
}