import React from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import ProtectedRoute from "./routers/ProtectedRoute";
import { AdminLayout } from "./layouts/AdminLayout";
import { CoordinadorLayout } from "./layouts/CoordinadorLayout";
import { UserLayout } from "./layouts/UserLayout";

import Administrador from "./pages/Administrador/Homepage/Administrador";

import LoginSignUp from "./pages/LoginSignUp/LoginSignUp";
import ResetPassword from "./pages/LoginSignUp/ResetPassword/ResetPassword";
import ResetPasswordConfirm from "./pages/LoginSignUp/ResetPassword/ResetPasswordConfirm";
import Homepage from "./pages/User/Homepage/Homepage";
import Calendar from "./pages/User/Calendar/Calendar";
import CalendarCulturalSpace from "./pages/User/Calendar/CalendarCulturalSpace";
import FormationCulturalSpace from "./pages/User/Formation/FormationCulturalSpace";
import CalendarEvent from "./pages/User/Calendar/CalendarEvent";
import Formation from "./pages/User/Formation/Formation";
import CulturalSpaces from "./pages/User/CulturalSpaces/CulturalSpaces";
import MapCulturalSpaces from "./pages/User/Map/MapCulturalSpaces";

import Coordinador from "./pages/Coordinador/Homepage/Coordinador";
import CulturalSpaceCoordinador from "./pages/Coordinador/CulturalSpace/CulturalSpaceCoordinador";
import ProductionsCoordinador from './pages/Coordinador/Productions/Productions';
import CoursesCoordinador from './pages/Coordinador/Formation/Courses';

import ProductionsAdministrador from "./pages/Administrador/Productions/Productions";

import Coordinadores from "./pages/Administrador/Accounts/Coordinadores";
import CulturalSpacesAdministrador from "./pages/Administrador/CulturalSpaces/CulturalSpacesAdministrador";
import CoursesAdministrador from "./pages/Administrador/Formation/Courses";
import ChartsAdministrador from "./pages/Administrador/Charts/Charts";
import ChartsCoordinador from "./pages/Coordinador/Charts/ChartsCoordinador";
import Activity from "./components/Course/Activity/Activity";
import Error404 from "./routers/Error404";

function App() {
  return (
    <>
      <Router>
        <div className="App">
          <Routes>
            <Route path="/login" element={<LoginSignUp />} />
            <Route path="/reset-password" element={<ResetPassword />} />
            <Route exact path='/password/reset/:uid/:token' element={<ResetPasswordConfirm />} />
            <Route element={<UserLayout />}>
              <Route exact path="/inicio" element={<Homepage />} />
              <Route path="/agenda" element={<Calendar />} />
              <Route path="/agenda/produccion/:productionId" element={<CalendarEvent />} />
              <Route path="/agenda/espacio-cultural/:culturalSpaceId/" element={<CalendarCulturalSpace />} />
              <Route path="/espacios-culturales" element={<CulturalSpaces />} />
              <Route path="/formacion" element={<Formation />} />
              <Route path="/formacion/:courseId" element={<Activity />} />
              <Route path="/formacion/espacio-cultural/:culturalSpaceId/" element={<FormationCulturalSpace/>} />
              <Route path="/mapa" element={<MapCulturalSpaces />} />
            </Route>

            <Route element={<CoordinadorLayout />}>
              <Route element={<ProtectedRoute isAllowed={1} />}>
                <Route path="/coordinador/inicio" exact element={<Coordinador />} />
                <Route path="/coordinador/producciones" exact element={<ProductionsCoordinador />} />
                <Route path="/coordinador/espacio-cultural" exact element={<CulturalSpaceCoordinador />} />
                <Route path="/coordinador/formacion" exact element={<CoursesCoordinador />} />
                <Route path="/coordinador/reportes" exact element={<ChartsCoordinador />} />
              </Route>
            </Route>
            <Route element={<ProtectedRoute isAllowed={2} />}>
              <Route element={<AdminLayout />}>
                <Route path="/administrador/inicio" exact element={<Administrador />} />
                <Route path="/administrador/coordinadores" exact element={<Coordinadores />} />
                <Route path="/administrador/reportes" exact element={<ChartsAdministrador />} />
                <Route path="/administrador/espacios-culturales" element={<CulturalSpacesAdministrador />} />
                <Route path="/administrador/producciones" element={<ProductionsAdministrador />} />
                <Route path="/administrador/formacion" element={<CoursesAdministrador />} />
               

              </Route>
            </Route>
            <Route path="*" element={<Error404/>} />


          </Routes>
        </div>
      </Router>
    </>
  );
}

export default App;