import axios from 'axios';
import CONFIG from '../configuration/configuration';

const axiosInstance = axios.create({
    
    baseURL: CONFIG.BASE_URL,
    timeout: 5000,
    'Authorization': `JWT ${localStorage.getItem('access_token')}`,
    'Content-Type': 'application/json',
    'accept': 'application/json'    
});

export default axiosInstance;