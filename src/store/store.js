import { createStore, combineReducers, applyMiddleware } from "redux";
import thunk from 'redux-thunk'
import {composeWithDevTools} from 'redux-devtools-extension'
import { courseReducer } from "../reducers/courseReducer";
import { subscriptionReducer } from "../reducers/subscriptionReducer";
import { productionReducer } from "../reducers/productionReducer";
import { userReducer } from "../reducers/userReducer";
import {authReducer} from "../reducers/authReducer";
import {culturalSpaceReducer} from "../reducers/culturalSpaceReducer";

const rootReducer = combineReducers ({
    courses: courseReducer,
    productions : productionReducer,
    subscriptions : subscriptionReducer,
    users: userReducer,
    cultural_spaces: culturalSpaceReducer,
    authentication: authReducer
});

// convert object to string and store in localStorage
function saveToLocalStorage(state) {
    try {
      const serialisedState = JSON.stringify(state);
      localStorage.setItem("persistantState", serialisedState);
    } catch (e) {
      console.warn(e);
    }
  }
  
  // load string from localStarage and convert into an Object
  // invalid output must be undefined
  function loadFromLocalStorage() {
    try {
      const serialisedState = localStorage.getItem('persistantState');
      if (serialisedState === null) return undefined;
      return JSON.parse(serialisedState);
    } catch (e) {
      console.warn(e);
      return undefined;
    }
  }

const store = createStore(rootReducer,loadFromLocalStorage(), composeWithDevTools( applyMiddleware(thunk) ));

store.subscribe(() => saveToLocalStorage(store.getState()));

export default store;