import { render, screen } from '@testing-library/react';
import { Tarjeta } from '../components/Tarjeta'

const item = {
    id: 1,
    logo: "",
    name: "Centro cultural UNGS",
    address: "Julio roca 579",
    partido: "San Miguel",
    location: "San Miguel",
    phone: "44514575",
    email: "ccungs@campus.ungs.edu.ar",
    facebook: "-",
    instagram: "-",
}

test('renders learn react link', () => {
    render(<Tarjeta item={item} />);
    const nombreDelEspacio = screen.getByText(/Centro cultural UNGS/i);
    expect(nombreDelEspacio).toBeInTheDocument();
});

test('renders learn react link', () => {
    render(<Tarjeta item={item} />);
    const direccionDelEspacio = screen.getByText(/Julio roca 579/i);
    expect(direccionDelEspacio).toBeInTheDocument();
});

test('renders learn react link', () => {
    render(<Tarjeta item={item} />);
    const emailDelEspacio = screen.getByText(/ccungs@campus.ungs.edu.ar/i);
    expect(emailDelEspacio).toBeInTheDocument();
});








